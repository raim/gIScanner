\NeedsTeXFormat{LaTeX2e}
%\documentclass[10pt,twocolumn]{article}
\documentclass[11pt]{article}
\usepackage[a4paper, margin=2cm]{geometry}
\usepackage{subfigure}
\usepackage{url}
%\usepackage{href}
\usepackage{graphicx}

\topmargin 0.0cm
%\oddsidemargin 0.5cm
%\evensidemargin 0.5cm
%\textwidth 17.35cm 
\textheight 24cm

\parskip 1.5 ex
\parindent 0 cm


\title{\texttt{cmchainer}: scanning genomes for group I introns}
\author{Rainer Machn\'e and Peter Stadler}

\begin{document}
\maketitle
\tableofcontents

\begin{abstract}\normalsize \parskip 1 ex

  Since the Rfam database does not feature several well-known group I
  introns (gI), we reasoned that either the Rfam covariance model (CM)
  does not cover the full diversity of bacterial group I
  introns or the presence of open reading frames in these introns
  hampers sensitivity of \texttt{infernal}-based CM scans.
  
  We therefore split the Rfam seed alignment (RF00028) as well as 14
  alignments of group I subtypes in the GISSD database into 27
  overlapping blocks spanning a directed acyclic graph (DAG) along the
  5'$\rightarrow$3' direction of the conserved intron core structure,
  yielding 379 distinct covariance models (sub-CMs). These sub-CMs
  were used to scan bacterial and phage genomes and reconstruct group
  I intron candidates by chaining adjacent hits in the correct DAG
  order.

  This document describes the construction of sub-CMs (section
  \ref{subcm}) and their usage in scanning genomes (section
  \ref{chainer}). Section \ref{bcereus} explains example results.

\end{abstract}

% evaluation of the strategy on known gI introns from
%the GISSD database (document gissd\_test.pdf)
\clearpage

\section{Sub-CM Construction}
\label{subcm}

\paragraph{In short,} 
we generated a coherent gI core structure annotation (Fig.
\ref{fig:annot}) of 15 distinct gI intron alignments (the Rfam RF00028
seed alignment, representative of the gI core structure, and 14 intron
subtypes from the GISSD database) by a partially manual process and
used this annotation to cut each alignment into overlapping blocks at
defined cut sites (Table \ref{tab:cuts}). The exact ordering of
overlapping blocks is defined by the Rfam consensus structure and
spans a directed acyclic graph (Fig. \ref{fig:dag}) that will later be
used in chaining of genomic hits. Figures \ref{fig:rfam}-\ref{fig:ie3}
depict all 15 gI introns, their annotation by core structure elements
1-7, the cut sites (red triangles) and alignment blocks, colored from
5' (violet) to 3' (red) and these colors will be used in reports of
chained sequences from genomic scans.

\paragraph{In detail,} 
we first built and calibrated covariance models (CM) using 
\texttt{Infernal 1.1} \cite{Nawrocki2013} (\texttt{cmbuild} and
\texttt{cmcalibrate}) from the 15 different full alignments with
secondary structure annotation: the Rfam seed alignment of 12 gI
sequences (RFAM00028), where the consensus structure represents only
the conserved gI intron core (Fig. \ref{fig:annot} \& \ref{fig:rfam}),
and 14 intron subtype alignments available at the GISSD database
(\url{http://www.rna.whu.edu.cn/gissd/}) \cite{Zhou2008}, which
contain additional stems and vary in the number of bulges and stem
lengths of the core structure (Fig. \ref{fig:ia1}-\ref{fig:ie3}).

The 15 gI alignments were coherently annotated according to the known
and well-defined core structure of group I introns by the following
semi-automatic procedure:\\All conserved stems were enumerated and
annotated in the Rfam consensus structure (CS) as obtained by
\texttt{cmemit} (Fig. \ref{fig:annot}). The historical exception
\cite{Burke1987} of distinguishing between P4 and P5 stems (rather
then using P4a for the latter) was skipped.  Stem P7 (\#6 in our
annotation) forms a pseudo-knot with P3, together defining the
catalytic center of the introns. P7 is the only feature highly
conserved on primary structure (sequence) level. Only one of two stems
in a pseudo-knot conformation can be handled by CM and in all 15
downloaded alignments the base pairs of stem P3 are annotated, most
likely with the annotators' motivation that the sequence conservation
of P7 will serve as a high scoring feature in CM scans in
itself. Thus, the stem P7 was manually located and annotated in the
Rfam CS.\\
The Rfam CS annotation was then mapped to all 14 GISSD
subtype CS (\texttt{cmemit}), where secondary structure alignment by
\texttt{RNAforester} \cite{Hochsmann2003} (\textit{via} R script
\texttt{mapStructureAnnotation.R}) served as a guide for manual
annotation of each subtype CS. Additional stems of the individual
subtypes were retained as basepairs in WUSL notation ($<..>$). Next,
\texttt{mapStructureAnnotation.R} was used again to align the original
secondary structure annotations of each alignment with the previously
emitted and now annotated CS. The annotations were then mapped back to
the alignments automatically by copying annotation between aligned
stems and expanding to annotated secondary structure basepairs that
were lost in the emitted CS, and copying the annotations for the P7
pseudoknot stem. By this procedure, we obtained a coherent and
comprehensive annotation of the core gI structure and all additional
stems in all 15 original alignments (Rfam RF00028 seed alignment, 14
GISSD subtype alignments) as shown in Figures
\ref{fig:rfam}-\ref{fig:ie3}.

The consensus structure of the conserved gI core was then divided into
27 overlapping block types by cutting the primary structure in the
loops or at the bases of annotated stems \ref{tab:cuts}. \textit{Via}
their common annotation, all the 15 alignments were split into these
27 blocks types (\texttt{splitAlignment.R}). Since four gI subtypes
(IB2, IB3, IB4, IC2) lack the P2 stems and the P2 stem defines the
cut-sites of six blocks (Table \ref{tab:cuts}), we obtained
$15*27-4*6=381$ individual alignment blocks. Each of the 27 original
block types has 11-15 distinct implementations stemming from the 15
distinct Rfam and GISSD alignments. Figure \ref{fig:dag} shows the
directed acyclic graph (DAG) that is spanned by the overlaps (edges)
between the 27 alignment block types (nodes), where the
5'$\rightarrow$3' offset between blocks sets the direction of
edges. Cut sites are indicated as red triangles in Figures
\ref{fig:rfam}-\ref{fig:ie3}. The alignment block types are colored
from 5' (violet) to 3' (red) and these colors are used in reports from
genomic scans (where individual hits are chained to propose full
intron candidates).

Covariance models (sub-CMs) were calibrated for each of the $381$
alignment blocks with secondary structure annotation
(\texttt{cmcalibrate}). For further $2$ alignment blocks no CM could
be calibrated due to lack of information content in the P1 stem of the
IC3 subtype, resulting in a total of $379$ sub-CMs.

\section{Genome scans and sub-CM chaining}
\label{chainer}


Genomes can then be scanned with each of the sub-CMs (\texttt{cmsearch})
and hits are collected and further processed using a simple \textit{ad hoc}
`chaining' strategy implemented in the R script
\texttt{chainCMResults.R}:

\begin{enumerate}
\item Optionally (option --inc), only hits that pass the \texttt{cmsearch}
  significance criterium are further processed. This depends on the assumed
  size of the scanned genome in megabases (\texttt{cmsearch} option
  -Z=$<$float$>$) and is indicated by the entry `!' in column `inc' in
  tab-delimited \texttt{cmsearch} result file.
\item Two adjacent sub-CM hits are chained if they are on the same
  strand, in a minimal distance to each other (option -maxd=$<$int$>$)
  and in the correct 5'$\rightarrow$3' order, i.e., a path along
  directed edges in the ordering DAG (Fig. \ref{fig:dag}) exists
  between the two blocks, independent of the gI subtype they were
  derived from. A wrong order is accepted if the two hits stem from
  overlapping blocks in the original Rfam alignment, i.e., the two
  blocks share an edge in the ordering DAG (Fig. \ref{fig:dag}).
\item A completed chain is accepted if it contains a minimal number of
  chained hits (option -minl=$<$int$>$) and at least two hits from
  non-overlapping alignment blocks (option -ovlf), i.e. two blocks that do
  not share an edge in the ordering DAG (Fig. \ref{fig:dag}).
\end{enumerate}

%Additionally, the script will also report hits from the 15 full-length gI
%intron CM which did not lead to a full chain by sub-CMS. These are usually
%false positives (TODO: test/quantify).
%
Besides reporting all gI candidate chains in a tab-delimited results
file, the script \texttt{chainCMResults.R} also plots the invidual
chained hits of all reported gI candidates for visual inspection in a
`genome browser' view (see Fig. \ref{fig:bcereus} and section
\ref{bcereus}); including, if provided: genome annotations from a gff
file or tab-delimited hit files from other annotation tools
(\texttt{blast}, \texttt{hmmer}, \texttt{cmsearch}, ...). Note, that
the chaining strategy itself could later take into account hits from
different sources, e.g., \texttt{hmmer} or \texttt{blast} hits for the
intron-encoded or the exon open reading frames, or local
\texttt{RNAfold} candidates for the highly variable and
splice-site-defining loops P1 and P10.


\begin{figure}
  \begin{minipage}{.3\textwidth}\centering
    \subfigure{
      \label{tab:annot}
      \begin{tabular}{lc}
        official & herein\\\hline
        stem P1 & 1\\
        stem P2 & 2\\
        stem P3 & 3\\
        stem P4 & 4\\
        stem P5 & 4\\
        stem P6 & 5\\
        stem P7 & 6\\
        stem P8 & 7\\
        stem P9 & 8
      \end{tabular}
    }
  \end{minipage}
  \begin{minipage}{.7\textwidth}\centering
    %\includegraphics[width=\textwidth]{../parameters/rf00028_consensus.fa_PK_ss_cut.ps}
    \includegraphics[width=\textwidth]{../parameters/rf00028_consensus.fa_PK_ss.ps}
  \end{minipage}
  \caption[Group I Intron core structure annotation]{{\bf Group I
      Intron core structure annotation.} Left: mapping from the
    official gI core structure annotation to the annotation used in
    this work in alignment annotation and splitting. P3 forms the
    basis of a multiloop including P4, P5, P6 and the 5' stem of
    P7. The common notation of P4 and P5 is a historical exception and
    here subsumed with index 4. Right: consensus structure of the CM
    constructed from the Rfam RF00028 seed alignment which is used as
    a representative of the conserved intron core in this work, and
    showing the P7 stem of the P3-P7 pseudoknot TODO: indicate P3.}
  \label{fig:annot}
\end{figure}

\begin{table}  
  \begin{minipage}{.5\textwidth}\small\centering
    \begin{tabular}{cc|l|l}
      \bf left & \bf right & \bf contains \\\hline
      L     & P$_1$ & P$_1$ & *\\
      P$_1$ & L$_2$ & P$_1$; L$_2$ &*,**\\
      P$_1$ & P$_2$ & P$_1$; P$_2$ &**\\
      R$_1$ & P$_2$ & R$_1$; P$_2$ &**\\
      P$_2$ & L$_3$ & P$_2$; L$_3$ &**\\
      R$_2$ & P$_3$ & R$_2$; P$_3$; P$_4$; P$_5$; L$_6$ &**\\
      R$_2$ & L$_4$ & R$_2$; L$_3$; L$_4$ &**\\
      P$_3$ & L$_4$ & P$_3$; P$_4$; P$_5$; L$_6$ \\
      L$_3$ & L$_4$ & L$_3$; L$_4$ \\
      L$_3$ & L$_6$ & L$_3$; P$_4$; P$_5$; L$_6$ \\
      R$_3$ & P$_4$ & R$_3$; P$_4$; P$_5$; L$_6$ \\
      P$_4$ & L$_5$ & P$_4$; L$_5$ \\
      R$_4$ & P$_5$ & R$_4$; P$_5$ \\
      R$_4$ & L$_7$ & R$_3$; R$_4$; P$_5$; L$_6$; L$_7$ 
    \end{tabular}
  \end{minipage}
  \begin{minipage}{.5\textwidth}\small\centering
    \begin{tabular}{cc|l}
      \bf left & \bf right & \bf contains \\\hline
      P$_5$ & L$_6$ & P$_5$; L$_6$ \\
      R$_5$ & P$_6$ & R$_3$; R$_5$; P$_6$; P$_7$ \\
      R$_5$ & L$_7$ & R$_3$; R$_5$; L$_6$; L$_7$ \\
      P$_6$ & L$_7$ & R$_3$; P$_6$; P$_7$ \\
      L$_6$ & L$_8$ & R$_3$; P$_6$; P$_7$; L$_8$ \\
      L$_6$ & R$_3$ & R$_3$; L$_6$ \\
      R$_6$ & P$_7$ & R$_6$; P$_7$ \\
      P$_7$ & L$_8$ & R$_6$; P$_7$; L$_8$ \\
      P$_7$ & P$_8$ & R$_6$; P$_7$; P$_8$ \\
      R$_7$ & P$_8$ & R$_6$; R$_7$; P$_8$ \\
      R$_7$ & R$_6$ & R$_6$; R$_7$ \\
      R$_6$ & P$_8$ & R$_6$; P$_8$ \\
      P$_8$ & R     & P$_8$ 
    \end{tabular}
  \end{minipage}
  \caption[Alignments blocks]{{\bf Alignments blocks.} Alignments were
    cut around the main stem-loops of the intron core structure as
    shown in Figure \ref{fig:annot}. The left and right cut sites are
    indicated, where P$_i$ indicates that both halfs are included,
    L$_i$: only the left (5') half and R$_i$: only the right (3') half
    of the stem $i$ is included. R and L without index indicate the
    left (5') and right (3') ends of the intron. (*) for these
    alignment blocks no CM could be calibrated due to lack of
    information content in the P1 stem. (**) these alignment blocks
    are not available for gI subtypes IB2, IB3, IB4 and IC2 since they
    lack stem P2.}
  \label{tab:cuts}
\end{table}
\begin{figure}
  \includegraphics[width=.8\linewidth]{../parameters/substk/RF00028_seed.stockholm_annotated_subcms_order.ps}
  \caption[]{DAG of sub-CM overlaps}
  \label{fig:dag}
\end{figure}

\bibliographystyle{abbrv}
%% for new refs: reset to global tata.bib, run latex/bibtex,
%% and use `bibexport -o alignments.bib alignments.aux`
%\bibliography{/home/raim/ref/tata}
\bibliography{alignments}
\clearpage

\section{The Bacillus\_FT9 genome: example results}
\label{bcereus}

The scaffolds 1-6 of the Bacillus\_FT9 genome were scanned with
\texttt{cmsearch} for all 379 sub-CMs, assuming circular chromosomes (by
copying the first 5000 nt to the end of each sequence) and using local
alignment (\texttt{cmsearch} default) and a search space of 1 Mb genome size
for E-value calculation (\texttt{cmsearch} option -Z 1).

A `chain', i.e., a gI intron candidate was reported by the script
\texttt{chainCMResults.R} when at least two sub-CM hits (option
--minl=2) that passed the \texttt{cmsearch} significance criteria
(option --inc) were detected on the same strand and in the correct
5'$\rightarrow$3' order, with maximally 5000 bp distance between two
neighboring hits (option --maxd=5000) and with at least two hits where
the originating alignment blocks do not overlap in the Rfam consensus
core structure (option --ovlf). Details on the chaining strategy are
given in section \ref{chainer}. Additionally, the script reported
\texttt{cmsearch} hits based on any of the 15 full-length gI intron
CMs where no full sub-CM chain could be constructed. All 6 resulting
hits are listed in the file \texttt{results.tab}, accompanied by plots
for visual inspection (Fig. \ref{fig:bcereus} \&
\ref{fig:bcereus_full}).

Figure \ref{fig:bcereus} shows the sub-CM hit profile of the four
reported chains and Figure \ref{fig:bcereus_full} shows two additional hits
only reported from the 15 full CM scans. The latter two hits based on
full CM scans only look rather poorly and may reflect other RNA
structural elements with similarity to the 3' end of gI introns or a
fragmented and unfunctional relic of a gI intron, as can be seen from
isolated and non-significant sub-CM hits from the 3' end of the core
structure (indicated by their color code, Fig. \ref{fig:dag} and
\ref{fig:rfam}-\ref{fig:ie3}). Chain 3 on scaffold 1
(Fig. \ref{chain3}) on the other hand barely passed the chaining
criteria, has no full CM hit and reflects only some 5' gI
structures. Figures \ref{chain1}, \ref{chain2} and \ref{chain4} are
well-covered by ordered subCM hits and are most likely true gI intron
hits. Non-significant sub-CM hits in the correct order suggest that
both candidates \ref{chain1} \& \ref{chain4} may be longer on the 5'
end then reported in the file \texttt{results.tab}.

\begin{figure}[h!]
  \begin{minipage}{.5\textwidth}
      \includegraphics[height=.9\linewidth,angle=-90]{figures/Bacillus_FT9_|_circularized:_5000_nt_Scaffold_1_gI3_cmfull_1.eps}\\
      \subfigure[full hit 1, scaffold 1]{
        \label{full1}
        \texttt{\tiny Bacillus\_FT9\_|\_circularized:\_5000\_nt\_Scaffold\_1\_gI3\_cmfull\_1.png}
      }
  \end{minipage}
  \begin{minipage}{.5\textwidth}
      \includegraphics[height=.9\linewidth,angle=-90]{figures/Bacillus_FT9_|_circularized:_5000_nt_Scaffold_1_gI4_cmfull_2.eps}\\
    \subfigure[full hit 2, scaffold 1]{
      \label{full2}
      \texttt{\tiny Bacillus\_FT9\_|\_circularized:\_5000\_nt\_Scaffold\_1\_gI4\_cmfull\_2.png}
    }
  \end{minipage}
  \caption[Bacillus\_FT9 genome: additional full CM hits]{{\bf gI
      intron scan in the Bacillus\_FT9 genome: additional full CM
      hits.} As Fig. \ref{fig:bcereus}, but the two additional hits
    from full CM scans that had no sub-CM chain.}
  \label{fig:bcereus_full}.
\end{figure}



\begin{figure}[h!]
  \begin{minipage}{.5\textwidth}
    \includegraphics[height=\linewidth,angle=-90]{figures/Bacillus_FT9_|_circularized:_1724_nt_Scaffold_3_gI1_cmchain_1.eps}\\
    \subfigure[chain 1, scaffold 3]{
      \label{chain1}
      \texttt{\tiny Bacillus\_FT9\_|\_circularized:\_1724\_nt\_Scaffold\_3\_gI1\_cmchain\_1.png}
    }\\
    \includegraphics[height=\linewidth,angle=-90]{figures/Bacillus_FT9_|_circularized:_1724_nt_Scaffold_3_gI2_cmchain_2.eps}\\
    \subfigure[chain 2, scaffold 3]{
      \label{chain2}
      \texttt{\tiny Bacillus\_FT9\_|\_circularized:\_1724\_nt\_Scaffold\_3\_gI2\_cmchain\_2.png}
    }
  \end{minipage}
  \begin{minipage}{.5\textwidth}
    \includegraphics[height=\linewidth,angle=-90]{figures/Bacillus_FT9_|_circularized:_5000_nt_Scaffold_1_gI1_cmchain_3.eps}\\
    \subfigure[chain 3, scaffold 1]{
      \label{chain3}
      \texttt{\tiny Bacillus\_FT9\_|\_circularized:\_5000\_nt\_Scaffold\_1\_gI1\_cmchain\_3.png}
    }\\
    \includegraphics[height=\linewidth,angle=-90]{figures/Bacillus_FT9_|_circularized:_5000_nt_Scaffold_1_gI2_cmchain_4.eps}\\
    \subfigure[chain 4, scaffold 1]{
      \label{chain4}
      \texttt{\tiny Bacillus\_FT9\_|\_circularized:\_5000\_nt\_Scaffold\_1\_gI2\_cmchain\_4.png}
    }
  \end{minipage}
  \caption[Bacillus\_FT9 genome: sub-CM chains]{{\bf gI intron scan in
      the Bacillus\_FT9 genome: sub-CM chains.} The x-axis is the
    genomic coordinate on the indicated chromosome (scaffold). The
    y-axis gives the number of hits for each of the 27 different
    alignment block types listed in Table \ref{tab:cuts} and
    color-coded as in Figures \ref{fig:dag} and
    \ref{fig:rfam}-\ref{fig:ie3}. Each alignment block is implemented
    by 11-15 different alignments from the Rfam consensus structure and
    14 gI intron subtypes. The \underline{upper row} in each panel
    (subCM '!')  shows sub-CM hits that were used to construct
    candidate chains, the \underline{middle row} (subCM '?') shows
    additional sub-CM hits that did not pass the \texttt{cmsearch}
    significance criteria, and which were not used in the chainer but
    can provide hints towards the exact 5' and 3' borders of the
    candidate. The \underline{bottom row} in each panel (fullCM) shows
    hits from the 15 full CMs. Dotted vertical lines denote the
    borders of the candidate as given in the results file
    \texttt{results.tab}.}
  \label{fig:bcereus}.
\end{figure}


\clearpage

\section{Secondary structures, annotation and sub-CM blocks of the 15 gI alignments}
\begin{figure}
  \includegraphics[width=.5\linewidth]{../parameters/rf00028_consensus.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/rf00028_consensus.fa_PK_ss.ps}\\ \includegraphics[width=.5\linewidth]{../parameters/rfam/RF00028_seed.stockholm_annotation.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/rfam/RF00028_seed.stockholm_annotation.fa__ss.ps}\\ \includegraphics[height=\linewidth,angle=-90]{../parameters/substk/RF00028_seed.stockholm_annotated_subcms_cuts.eps}
  \caption[]{RFAM Seed}
  \label{fig:rfam}
\end{figure}
\begin{figure}
  \includegraphics[width=.5\linewidth]{../parameters/IA1_consensus.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/IA1_consensus.fa_PK_ss.ps}\\
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IA1_annotation.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IA1_annotation.fa_PK_ss.ps}\\
  \includegraphics[height=\linewidth,angle=-90]{../parameters/substk/IA1_orig_annotated_subcms_cuts.eps}
  \caption[]{GISSD subtype IA1}
  \label{fig:ia1}
\end{figure}
\begin{figure}
  \includegraphics[width=.5\linewidth]{../parameters/IA2_consensus.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/IA2_consensus.fa_PK_ss.ps}\\
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IA2_annotation.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IA2_annotation.fa_PK_ss.ps}\\
  \includegraphics[height=\linewidth,angle=-90]{../parameters/substk/IA2_orig_annotated_subcms_cuts.eps}
  \caption[]{GISSD subtype IA2}
\end{figure}
\begin{figure}
  \includegraphics[width=.5\linewidth]{../parameters/IA3_consensus.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/IA3_consensus.fa_PK_ss.ps}\\
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IA3_annotation.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IA3_annotation.fa_PK_ss.ps}\\
  \includegraphics[height=\linewidth,angle=-90]{../parameters/substk/IA3_orig_annotated_subcms_cuts.eps}
  \caption[]{GISSD subtype IA3}
\end{figure}
\begin{figure}
  \includegraphics[width=.5\linewidth]{../parameters/IB1_consensus.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/IB1_consensus.fa_PK_ss.ps}\\
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IB1_annotation.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IB1_annotation.fa_PK_ss.ps}\\
  \includegraphics[height=\linewidth,angle=-90]{../parameters/substk/IB1_orig_annotated_subcms_cuts.eps}
  \caption[]{GISSD subtype IB1}
\end{figure}
\begin{figure}
  \includegraphics[width=.5\linewidth]{../parameters/IB2_consensus.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/IB2_consensus.fa_PK_ss.ps}\\
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IB2_annotation.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IB2_annotation.fa_PK_ss.ps}\\
  \includegraphics[height=\linewidth,angle=-90]{../parameters/substk/IB2_orig_annotated_subcms_cuts.eps}
  \caption[]{GISSD subtype IB2}
\end{figure}
\begin{figure}
  \includegraphics[width=.5\linewidth]{../parameters/IB3_consensus.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/IB3_consensus.fa_PK_ss.ps}\\
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IB3_annotation.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IB3_annotation.fa_PK_ss.ps}\\
  \includegraphics[height=\linewidth,angle=-90]{../parameters/substk/IB3_orig_annotated_subcms_cuts.eps}
  \caption[]{GISSD subtype IB3}
\end{figure}
\begin{figure}
  \includegraphics[width=.5\linewidth]{../parameters/IB4_consensus.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/IB4_consensus.fa_PK_ss.ps}\\
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IB4_annotation.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IB4_annotation.fa_PK_ss.ps}\\
  \includegraphics[height=\linewidth,angle=-90]{../parameters/substk/IB4_orig_annotated_subcms_cuts.eps}
  \caption[]{GISSD subtype IB4}
\end{figure}
\begin{figure}
  \includegraphics[width=.5\linewidth]{../parameters/IC1_consensus.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/IC1_consensus.fa_PK_ss.ps}\\
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IC1_annotation.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IC1_annotation.fa_PK_ss.ps}\\
  \includegraphics[height=\linewidth,angle=-90]{../parameters/substk/IC1_orig_annotated_subcms_cuts.eps}
  \caption[]{GISSD subtype IC1}
\end{figure}
\begin{figure}
  \includegraphics[width=.5\linewidth]{../parameters/IC2_consensus.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/IC2_consensus.fa_PK_ss.ps}\\
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IC2_annotation.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IC2_annotation.fa_PK_ss.ps}\\
  \includegraphics[height=\linewidth,angle=-90]{../parameters/substk/IC2_orig_annotated_subcms_cuts.eps}
  \caption[]{GISSD subtype IC2}
\end{figure}
\begin{figure}
  \includegraphics[width=.5\linewidth]{../parameters/IC3_consensus.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/IC3_consensus.fa_PK_ss.ps}\\
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IC3_annotation.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IC3_annotation.fa_PK_ss.ps}\\
  \includegraphics[height=\linewidth,angle=-90]{../parameters/substk/IC3_orig_annotated_subcms_cuts.eps}
  \caption[]{GISSD subtype IC3}
\end{figure}
\begin{figure}
  \includegraphics[width=.5\linewidth]{../parameters/ID_consensus.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/ID_consensus.fa_PK_ss.ps}\\
  \includegraphics[width=.5\linewidth]{../parameters/gissd/ID_annotation.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/gissd/ID_annotation.fa_PK_ss.ps}\\
  \includegraphics[height=\linewidth,angle=-90]{../parameters/substk/ID_orig_annotated_subcms_cuts.eps}
  \caption[]{GISSD subtype ID}
\end{figure}
\begin{figure}
  \includegraphics[width=.5\linewidth]{../parameters/IE1_consensus.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/IE1_consensus.fa_PK_ss.ps}\\
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IE1_annotation.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IE1_annotation.fa_PK_ss.ps}\\
  \includegraphics[height=\linewidth,angle=-90]{../parameters/substk/IE1_orig_annotated_subcms_cuts.eps}
  \caption[]{GISSD subtype IE1}
\end{figure}
\begin{figure}
   \includegraphics[width=.5\linewidth]{../parameters/IE2_consensus.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/IE2_consensus.fa_PK_ss.ps}\\
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IE2_annotation.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IE2_annotation.fa_PK_ss.ps}\\
  \includegraphics[height=\linewidth,angle=-90]{../parameters/substk/IE2_orig_annotated_subcms_cuts.eps}
  \caption[]{GISSD subtype IE2}
\end{figure}
\begin{figure}
  \includegraphics[width=.5\linewidth]{../parameters/IE3_consensus.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/IE3_consensus.fa_PK_ss.ps}\\
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IE3_annotation.fa_ss.ps}
  \includegraphics[width=.5\linewidth]{../parameters/gissd/IE3_annotation.fa_PK_ss.ps}\\
  \includegraphics[height=\linewidth,angle=-90]{../parameters/substk/IE3_orig_annotated_subcms_cuts.eps}
  \caption[]{GISSD subtype IE3}
  \label{fig:ie3}
\end{figure}


\end{document}
