
## 20180805
added all annotated stockholm alignments `*.stk/*.sto` and sub-alignment
ordering info `*dat` to git, 
from gissd and substk in processedData/  to gissd and substk in parameters;
re-built and -calibrated to parameters/subcms and parameters/fullcms;
copied latest RF00028.cm from rfam (wget) to parameters/fullcms
