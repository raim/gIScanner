# The Group I Self-Splicing Intron Scanner


Tools to (a) split RNA secondary structure models of complex RNA
molecules/families, potentially with coding inserts, (b) search each
sub-model in genomes by *infernal*, and (c) chain results into hit models
by *cmchainer*.

**Created for group I introns, but adaptable to other projects.**
Please see doc/alignments.tex for details.

Additionally, the repo currently contains some interesting utilities
to handle NCBI taxonomy, 16S rRNA-based phylogenies, and life-style
information.


## Installation, Quick Start & Usage

* Download:

```
git clone git@gitlab.com:raim/gIScanner.git
```
* Install *cmchainer* R tools:

```
R CMD build gIScanner
sudo R CMD INSTALL cmchainer_0.0.1.tar.gz
```
* Install [*infernal*](http://eddylab.org/infernal/)
* Scan a genome database (fasta file `allcontigs.fa`) in bash:

```{bash}
srcpath=~/programs/gIScanner      # path to this git
## index with infernal's easl miniapp esl-sfetch
esl-sfetch --index allcontigs.fa
## loop through all 379 calibrated gI sub-models
## NOTE: this loop should be parallelized!
for cm in `grep -l cmcalibrate $srcpath/parameters/subcms/*cm`; do
    results=subcms/allcontigs_$(basename ${cm%.*})
    cmsearch --cpu 8 -Z 1 --tblout ${results}.tab $cm allcontigs.fa
done
## call chainCMResults.R to chain results into gI models
$srcpath/scripts/chainCMResults.R --dir ./  --cmd=subcms --fend=_r112 --gdf=$srcpath/parameters/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --out=./ --fig=./figures  --chs=./chains --inc --exp --store --maxr=0 --maxd=2500 --minl=3 --ovlf --plot --dof
```

* Detailed procedure, including parallelization on an SGE cluster,
  in `scripts/<filename.sh>` to:
    - **create ordered sub-models**:
      `alignments_2012.sh`, cross-annotate different alignments of a
      RNA family, and split into defined sub-alignments
    - **calibrate covariance models**: `recalibrate_r112.sh`, calibrate existing
      gI sub-alignments with infernal's *cmbuild* and *cmcalibrate*
    - **scan genomes**: `bacteria_2018.sh`,  scan genomes with *cmsearch*, and
      chain results with *cmchainer*
* Outdated (2012) but reaching further (see archives below), and to be partially
integrated above:
    - `scripts/doAll.sh` for the full pipeline above and
      further analyses of gI content vs. taxonomy and life-style information

## git Contents

* `R/`:
an R package for all R utilities used herein, including the *cmchainer*,
tools to process alignments and secondary structure annotation, and
NCBI taxonomy and 16S rRNA phylogeny information
* `scripts/`:
Various scripts merely documenting manually executed processing steps,
and *bash* and * Rscript* command-line utilities, used in these processing
steps. The latter may be useful in adaptation to new projects.
* `parameters/`:
    - `*consensus.fa` - consensus structure annotation of cmemit
       CM consensus structures from subgroup CM; manual annotation was
       aided by RNAforester 
    - `gissd/` - original annotated alignments from the gissd database,
       with two bug fixes
    - `*.patch`: two fixes for *gissd* alignments and sequences
    - `substk/` - annotated alignments and ordering info of sub-models, as
       generated in `scripts/alignments_2012.sh`
    - `subcms/` - TODO: calibrated covariance models of sub-alignments
    - `fullcms/` - TODO: calibrated covariance models of full alignments
       from *gissd* and rfam *RF00028*
* `doc/`:
documentation of the procedure; TODO: add figures required for tex

## Additional files

`scripts/doAll.sh` 'did it all'.

The following archives document original, intermediate and result
files for gI scans, homing endonuclase scans, and analysis of taxonomy
and life-style data generated.

### Additional files in `processedData.tar.gz`

The main intermediate files generated in `doAll.sh` (and now in
`scripts/alignments_2012.sh`) and required for running the scans and
further analyses.

This archive is available at
https://www.tbi.univie.ac.at/~raim/data/introns/processedData.tar.gz
or from raim@tbi.univie.ac.at.


* `rfam/`: 
    - `RF00028_seed.stockholm.stk` - original alignment
    - `RF00028_v2/_v11.cm` - original CM for old and new infernal
    - `RF00028_seed_RF00028_v11[|mid|max].stk` - cmsearch -A alignments
       of rfam in rfam  sequences with no and --mid or --max options
    - `RF00028_seed_RF00028_v11_ali*.stk` - cmalignments of original
       sequences to their CM
    - `gissd*` - cmsearch rfam in gissd sequences
    - `I*.stk` - cmalign gissd subgroups to rfam
    - `*annotation.fa` - consensus structure of original alignments, annotated
       by mapping from parameters/rf00028_consensus.fa annotation file
       via RNAforester
    - `*.ps` - figures of annotated RNA consensus and annotation structures
       via RNAplot
    - `*annotated.stk` - annotated versions of alignments
* `gissd/`: 
    - `gissd.fasta` - all sequences of gissd introns
    - `I*.sto` - original alignments
    - `I*.cm` - CM of original alignments, TODO: old/new infernal
    - `I*.stk` - cmalignments of original sequences to their CM
    - `*annotation.fa` - consensus structure of original alignments,
      annotated by 
      mapping from parameters/I*annotation.fa via RNAforester
    - `*ps` - figures of annotated RNA consensus and annotation
      structures via RNAplot
    - `I*_annotated.stk` - annotated versions of cmalignments
    - `I*_orig_annotated.stk` - annotated versions of original alignments
* `substk/`: 
    - `*.stk` - cuts of original alignments via scripts/splitAlignment.R
    - `*.eps/png` - plots of cut ranges vs. consensus structure
    - `*.dat` - ordering information of the sub-models
* `subcms4/`: 
    - `*.cm` - built and partially calibrated CM of substk/*stk for
       infernal r1.1rc1
* `pfam/`:
    - `*.hmm` - PFAM hidden markov models for homing endonucleases
    - `*tab/*dat` - results of hmmer searches in sequences from
      `rfam` and `gissd`
* `lifestyle/`: [TODO: rm an instead add to below]
various lifestyle informations from publications and the Patric database,
processed in `scripts/doAll.sh`.


### Additional files generated by `scripts/doAll.sh`

The following genome and taxonomy files were generated for genome
scans and further analysis of gI scan results.

TODO: store all these in a big archive somewhere!

* `subcms3/`: 
    - `*.cm` - built and partially calibrated CM of `substk/*stk` for
      infernal r1.0.2
* `genomes/`: 
    - `bacteria/` - gff/ and fasta/ contain sequences from all full NCBI 
      bacterial genomes (2012), allbacteria.fasta comprises all fasta files in
      fasta dir; taxonomy.dat is the index file with a sequences<->species
      mapping
    - `phages/` - as above, but for bacterial phages
* `taxonomy/` -
16S rRNA phylogeny and NCBI taxonomy information and mappings



## Quick Note - 20180503

I have quickly packed the main data required for running the scans
at https://www.tbi.univie.ac.at/~raim/data/introns/processeData.tar.gz
Data sources are documented simply by their URL used in scripts/download.sh    
(all from ca. Sept 2012 - Feb 2013), and processing done in doAll.sh.

All bacterial and phage genome data used back then are not
included in above archive, incl. the ncbi taxonomy stuff. So all
higher level analyses in doAll.sh won't work,
but everything should be there to reproduce steps in scripts/rebecca.sh,
scripts/mia.sh and scripts/mitochondria.sh, which all apply the
basic pipeline to new projects.

So from this archive and the scripts at https://gitlab.com/raim/gIScanner 
the whole pipeline could be easily established. There may be usage of some 
scripts from the former parent CVS repository.

Regeneration of infernal CM and hmmer models may be required for new
versions of these tools. If there are novel alignments or CM models
available, they can be integrated but should be split into the same
sub-models as the GISSD and Rfam alignments.

