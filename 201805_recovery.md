# Taxonomy / Phylogeny Mapping

Es gab da diverse funktionen 16S rRNA phylogenie (aus SILVA datenbank)
irgendwie auf die NCBI taxonomy zu mappen; wozu weiss ich gar nicht mehr,
aber es kommen newick baeume fuer alle analysierten spezies raus.

Im prinzip scheint es so ein mapping der analysierten spezies, also
deren genbank accession numbers, ueber die NCBI taxonomy auf
den 16S rRNA baum der SILVA datenbank, zu sein.

Das wuerde vermutlich dann mit phylogenien der introns und HEGs
zu vergleichen sein.

fuer phispy wurden die taxonomy mappings auch verwendet.


vllt ist das noch nuetzlich?

* fetchFastaFromID.pl holt sich genbank annotation UND fasta von ncbi
ueber den NCBI taxon ID, und schreibt das neue taxon dann offenbar auch
in eine lokale liste der taxons, aus denen wie oben dann newick baeume
generiert werden

* tax2newick.R konvertiert glaub ich die NCBI
taxonomy zu einem newick format
* cmpTaxa.R mapped irgendwie 16S rRNA phylogeny auf NCBI taxonomy,
output sind wohl newick format trees

* in utils.R, jetzt R/taxonomy.R, gibt's entsprechende Funktionen
um NCBI taxonomy und newick Baeume zu parsen, darin zu wandern
(getAncestor sucht zB den gemeinsamen Ancestor mehrerer Spezies).


# Detailanalyse

Weiters gibt es scripts, um dann fuer die candidates des grossen
scans jeweils die sequenzen aus den original fastas zu ziehen, mit dem
ziel diese nochmal genauer zu analysieren, zB wurden die vollen CMs aller
gI Modelle nochmal mit extrem relaxierten cutoffs auf die
candidate chains losgelassen, um enden nochmal besser zu definieren.
und per transeq -frame 6 dann Pfam/hmmer searches fuer alle bekannten
HEG Pfams gemacht, in allen 6 moeglichen frames.

* fetchSeq.R - nimmt hit files (results.tab, aber auch blast/infernal/hmmer
sollten meoglich sein; alles mit tstart und tend spalten),
und zieht die sequenzen (plus einstellbarer randerweiterung) aus den
original genome fastas


# Lifestyle u Ergebnisanalyse

hatte auch angefangen auch so infos aus "lifestyle" datenbanken
zu integrieren, wollte wohl das vorkommen von introns mit solchen
daten korrelieren.

* collectSpeciesHits.R sammelt offenbar alle intron hits fuer
eine spezies und versucht das mit lifestyle infos aus einer PATRIC
datenbank zu vergleichen; scheint aber unfertig fuer interaktive
verwendung

* collectChains.R sammelt neben results.tab noch divese
andere Ergebnisse aus der Detailanalyse und integriert die
alle; ich glaube die intronschau figures sind eigentlich daher
und nicht aus dem chainCMResults.R


leider liefen die allerletzten analysen in wien, und die zwischenergebnisse
gibts nicht mehr, funktioniert aber alles noch.

das letzte TODO war dann:
## TODO:
## * extract pfam and intron hits in correct strand and start to align!
## * phylogeny, use 16S rRNA phylogeny, map to taxon ids via acc and GbAccList
## * map phages to bacteria host via ??
## * 3 PHYLOGENIES: introns, HEGs, 16S rRNA
## * PHAGES vs. BACTERIA: compare phylogeny with hosts

stattdessen hatte ich dann erstmal die phages gescannt, und hatte dort
# CHAINS                 274 candidates in 152 of 869 sequences


das letzte war dann wohl ein versuch speziell in cyanobakterien zu suchen,
aber auch unvollendet.

