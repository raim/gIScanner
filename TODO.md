
## 201810

* proper alignments, define bacterial gI (and re-scan)
* plot gI content on 16S tree
* pairwise distance -> HGT events
* use PFAM2GO for pfam analysis

BUGS:
* overlap analysis with 200/500 extension : covers are wrong!
  resolve this, similar problem for yeast; search separately and merge?
 -> only count direct neighbors?
 -> do this in segmentAnnotate, run 3x for upstream, cover and downstream
 region, and skip those present in cover from upstream and downstream,
 replace "details" by distances

## 201808:

* chainer: 
    - HEGS: record max distance and left/right subCM in columns
    - clean-up overlapping, if one is contained in the other/with close ends
* set up a defined example to be used in `doc/alignments.tex`; add figures
  required to generate `doc/alignments.pdf`
* R package: try to resolve overlaps with R project genomeBrowser and
  R package segmenTools, and perhaps move R package to separate git!
* [more cleanly] add source data, constructed alignments, and calibrated 
  covariance models to git and/or separate data repo
* R package: make independent of system calls to bash commands cat and grep

* move rfam, gissd, substk, to parent dir and/or
  move subcms and fullcms to models/

* genomes: index all genomes in fasta collections to plot results with
genome browser

## 201809

* results: map annotation to alignments of gI consensus models;
check original literature on subgroups for alignments
* use chain hit patterns to classify into subgroups
* pfam enrichment in phages, after cleaning for redundant sequences
