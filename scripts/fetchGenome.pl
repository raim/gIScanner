#!/usr/bin/perl -w
#
# derived from NCBI's eutils_example.pl by Oleg Khovayko


use strict;
use Getopt::Long;
use File::Basename;
use LWP::Simple;


my $outfile = "";
my $db     = "Nucleotide";
my $query  = "";
my $format = "GenBank";

# get command line parameters
usage() unless GetOptions("db=s"  => \$db,
			  "qu=s"  => \$query,
			  "f=s"   => \$format,
			  "o=s"   => \$outfile,
			  "h"     => \&usage);

sub usage
{
    printf STDERR "\n\n Retrieves GenBank file for passed \n";
    printf STDERR " NCBI Nucleotide ID using EUtils\n\n";
    
    printf STDERR "\tUsage: @{[basename($0)]}\n\n";

    printf STDERR "-db=s\twhich database should be queried, current: $db\n";
    printf STDERR "-qu=s\tsearch term, current: $query\n";
    printf STDERR "-f=s\tformat for results, current: $format\n";
    printf STDERR "-o=s\toutfile name, current: $outfile\n\n";
    exit;
}


# original comments from eutils_example.pl :
# ---------------------------------------------------------------------------
# Define library for the 'get' function used in the next section.
# $utils contains route for the utilities.
# $db, $query, and $format may be supplied by the user when prompted; 
# if not answered, default values, will be assigned as shown below.
# ---------------------------------------------------------------------------
# $esearch contains the PATH & parameters for the ESearch call
# $esearch_result containts the result of the ESearch call
# the results are displayed �nd parsed into variables 
# $Count, $QueryKey, and $WebEnv for later use and then displayed.

my $utils = "http://www.ncbi.nlm.nih.gov/entrez/eutils";
my $esearch = "$utils/esearch.fcgi?" .
              "db=$db&retmax=1&usehistory=y&term=";

my $esearch_result = get($esearch . $query);

#print "\nESEARCH RESULT: $esearch_result\n";

$esearch_result =~ 
  m|<Count>(\d+)</Count>.*<QueryKey>(\d+)</QueryKey>.*<WebEnv>(\S+)</WebEnv>|s;

my $Count    = $1;
my $QueryKey = $2;
my $WebEnv   = $3;

print "RESULTS\n";
print "Count = $Count; QueryKey = $QueryKey; WebEnv = $WebEnv\n";

# ---------------------------------------------------------------------------
# this area defines a loop which will display $retmax citation results from 
# Efetch each time the the Enter Key is pressed, after a prompt.


open(OUTF, ">$outfile") or die "can't open result file $outfile\n";

my $retstart;
my $retmax=3;

my $efetch = "$utils/efetch.fcgi?" .
    "rettype=$format&retmode=text&" .
    "db=$db&query_key=$QueryKey&WebEnv=$WebEnv";

#print "\nEF_QUERY=$efetch\n";     

my $efetch_result = get($efetch);

print OUTF $efetch_result."\n";

close(OUTF);
