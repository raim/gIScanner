#!/bin/bash

touch gissd/IDs.dat

for i in `find gissd/ -name "I*.fasta"`
do
    type=`echo $i | sed 's/_seq.fasta//;s/gissd\///'`
    for j in `grep "^>" $i |sed 's/^>//g'`
    do
	printf "%b\t%b\n"  $j $type 
    done
done
