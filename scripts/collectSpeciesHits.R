#!/usr/bin/Rscript

## read a species ID <-> sequence (NCBI IDs) mapping and a hitfile
## (infernal,hmmer,blast) and collect hits per species

library(segmenTools) # for clusterCluster and plotOverlaps

## get directory of utils
library(cmchainer)


## 1) map hits to species IDs

### NCBI species mapping
#specFile <- "genomes/bacteria/Bacteria_nolastcol.ids"
#seqCol <- 2
#namCol <- 6
#taxCol <- 1
#species <- read.table(file=specFile, sep="\t", header=FALSE,quote="")
#species <- species[,c(seqCol,namCol,taxCol)]

## PATRIC DB species information and mapping
lifeFile <- "lifestyle/patric_metadata.txt"
life <- read.table(lifeFile,sep="\t", header=TRUE,quote="",comment.char="")
life <- as.matrix(life)
life[,"NCBI.Taxon.Id"] <- gsub(" ","",life[,"NCBI.Taxon.Id"])

## all analyzed sequences and their taxonomy association
specFile <- "genomes/bacteria/taxonomy.dat"
species <- read.table(specFile,sep="\t", header=FALSE,quote="",comment.char="")
colnames(species) <- c("gi","acc","seqname","species","taxon")

## species name hash
specNames <- species[,"species"]
names(specNames) <- species[,"taxon"]

## infernal/chainer hits
hitFile <- "genomes/bacteria/allbacteria/results/results.tab"
hits <- parseHomHits(files=hitFile,type="cmchain")


## map species IDs to hits
## TODO: use taxonomy to find closest!
hitSpec <- matrix(NA, nrow=nrow(hits), ncol=3)
colnames(hitSpec) <- c("seqname","species","taxon")
for ( i in 1:nrow(hits) ) {

  ## get gi and acc ID from header: gi|315121750|ref|NC_014774.1|
  seqId <- unlist(strsplit(as.character(hits[i,"target"]),"\\|"))
  gi <- rs <- ""
  if ( length(seqId)>1 ) {
    idx <- which(seqId=="gi")
    if ( length(idx)>0 )
      gi <- seqId[idx+1]
    idx <- which(seqId=="ref")
    if ( length(idx)>0 ) {
      rs <- seqId[idx+1]
      rs <- sub("\\.[1-9]+$","",rs)
    }
  }
  idxgi <- which(species[,"gi"]==gi)
  idxrs <- which(species[,"acc"]==rs)
  if ( idxgi != idxrs )
    cat(paste("WARNING. Hit", i, "sequence IDs differ:", gi, rs,"\n"))
  hitSpec[i,"seqname"] <- as.character(species[idxgi,"seqname"])
  hitSpec[i,"species"] <- as.character(species[idxgi,"species"])
  hitSpec[i,"taxon"] <- as.character(species[idxgi,"taxon"])
}

hits <- cbind(hits,hitSpec)

## intron-containing species
allTax <- unique(species[,"taxon"])
hasIntron <- allTax %in% unique(hits[,"taxon"])
perSpecies <- table(hits[,"taxon"])

allLife <- matrix(NA, ncol=ncol(life), nrow=length(allTax))
rownames(allLife) <- allTax
colnames(allLife) <- colnames(life)
cnt <- 0
for ( i in 1:nrow(allLife) ) {
  tax <- allTax[i]
  name <- unique(species[species[,"taxon"]==tax,"species"])
  idx <- which(life[,"NCBI.Taxon.Id"]==tax)
  if ( length(idx)==0 ) {
    cnt <- cnt+1
    cat(paste("ERROR:", i, name, "NOT FOUND IN PATRIC.\n"))
    next
  }
  if ( length(idx)>1 ) {
    cat(paste("ERROR:", i, name, " - ", length(idx),
              "lifestyle infos found, taking most informative entry.\n"))
    na <- rowSums( life[idx,] == "",na.rm=T) + rowSums(is.na(life[idx,]))
    idx <- idx[which(na==min(na))]
    if ( length(idx)>1 ) 
      cat(paste("\tcouldn't decide, taking first!\n"))
    idx <- idx[1]
  }
  allLife[i,] <- life[idx,]
}
allLife[is.na(allLife)] <- "NA"
allLife[allLife==""] <- "NA"

## fix
gram <- allLife[,"Gram.Stain"]
gram[is.na(gram)] <- "NA"
gram[gram==""] <- "NA"
gram[gram=="_"] <- "-"
gram[gram=="Negative"] <- "-"
gram[gram=="Positive"] <- "+"
allLife[,"Gram.Stain"] <- gram

halo <- allLife[,"Salinity"]
halo[is.na(halo)] <- "NA"
halo[halo==""] <- "NA"
halo[halo=="ModerateHalophilic"] <- "Moderate halophilic"
halo[halo=="NonHalophilic"] <- "Non-halophilic"
allLife[,"Salinity"] <- halo

### ANALYZE ENRICHMENTS
out.path <- "strainProperties"
dir.create(out.path)


## compare
table(allLife[,"Oxygen.Requirement"], hasIntron)
table(allLife[,"Temperature.Range"], hasIntron)
table(allLife[,"Salinity"], hasIntron)
table(allLife[,"Gram.Stain"], hasIntron)

## GENOME LENGTH
## NOTE: longer genomes of intron containing species!
len <- as.numeric(allLife[,"Genome.Length"])/1e6
file.name <- file.path(out.path, "genomeLength")
plotdev(file.name, width=6, height=3)
par(mai=c(.5,1,.1,.1),mfcol=c(1,2))
boxplot(len ~ hasIntron,border=2:1, ylab="genome length, Mbp")
plot(density(na.omit(len[hasIntron])),main="")
lines(density(na.omit(len[!hasIntron])),col=2)
legend("topright",legend=c("with gI", "without gI"), col=1:2, lty=1,bty="n")
dev.off()

t.test(len[hasIntron], len[!hasIntron])
wilcox.test(len[hasIntron], len[!hasIntron])

## GC CONTENT
## NOTE: lower GC content of intron containing species!
gc <- as.numeric(allLife[,"GC.Content"])

file.name <- file.path(out.path, "genomeGC")
plotdev(file.name, width=6, height=3)
par(mai=c(.5,1,.1,.1),mfcol=c(1,2))
boxplot(gc ~ hasIntron,border=2:1, ylab="GC content")
plot(density(na.omit(gc[hasIntron])),main="")
lines(density(na.omit(gc[!hasIntron])),col=2)
legend("topright",legend=c("with gI", "without gI"), col=1:2, lty=1,bty="n")
dev.off()
## test
t.test(gc[hasIntron], gc[!hasIntron])
wilcox.test(gc[hasIntron], gc[!hasIntron])


## SELECTED PROPERTIES
props <- allLife[,c("Oxygen.Requirement","Temperature.Range","Salinity","Gram.Stain","Disease","Habitat","Motility")]

props[,"Disease"] <- ifelse(props[,"Disease"]=="NA","no","yes")

enrichments <- rep(list(NA), ncol(props))
names(enrichments) <- colnames(props)
testcnt <- 0


for ( prop in colnames(props) ) {
  cls <- props[,prop]
  ## fisher's exact, cumul.hypergeo. test
  tab <- segmenTools::clusterCluster(cl2=hasIntron, cl1=cls)
  file.name <- file.path(out.path, prop)
  plotdev(file.name, width=3, height=length(unique(cls))+1)
  par(mai=c(1,1.5,.1,.1))
  plotOverlaps(tab,xlab="strain has intron",ylab=prop)
  dev.off()
  enrichments[[prop]] <- tab
  testcnt <- testcnt + length(unique(cls)) + length(unique(hasIntron))
  
}


## S.aureus
saureus <- grep("Staphylococcus aureus",allLife[,"Organism.Name"])
sintron <- hasIntron[saureus]
