## REBECCA'S GENOMES
## retrieved manually from rebecca kirsch's folders
## and re-added original NCBI fasta headers
echo "GENERATING BLAST DB"
formatdb -i  genomes/rebecca/rebecca_blastableheaders.fasta -p F -o T
## search gII introns
blastn -query genomes/rebecca/gII_20130204_intronsonly.fasta -db genomes/rebecca/rebecca_blastableheaders.fasta -outfmt "7 qseqid sacc pident length qstart qend sstart send  evalue bitscore" > genomes/rebecca/rebecca_vs_gII_20130204_intronsonly.dat
 blastn -query genomes/rebecca/gII_20130204_intronsonly.fasta -db genomes/rebecca/rebecca_blastableheaders.fasta   > genomes/rebecca/rebecca_vs_gII_20130204_intronsonly.out

## generate index for infernal
echo "INDEXING FOR INFERNAL"
/home/mescalin/choener/tmp/infernal/infernal-1.1rc1/easel/miniapps/esl-sfetch --index genomes/rebecca/rebecca.fasta
/home/mescalin/choener/tmp/infernal/infernal-1.1rc1/easel/miniapps/esl-sfetch --index genomes/rebecca/rebecca_blastableheaders.fasta

## subCM search
export WORKDIR=~/work/introns/phylogeny/
resdir=$WORKDIR/genomes/rebecca/cmsearch
seq=$WORKDIR/genomes/rebecca/rebecca_blastableheaders.fasta
log=$WORKDIR/genomes/rebecca/qsubmit.log
Z=1
touch $log
for cm in `grep -l cmcalibrate subcms4/*cm`; do
    res=$resdir"/allbacteria_"$(basename ${cm%.*})
    echo "ompi_cmsearch -Z" $Z " -o" $res.out "-A" $res.stk "--tblout" $res.tab $cm $seq >> $log
    qsub scripts/ompi_cmsearch -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq >> $log
        #cmsearch -A $res.stk --tblout $res.tab $cm $seq > $res.out
done
#mv cmsearch.* $resdir/log
## full CM search
export WORKDIR=~/work/introns/phylogeny/
resdir=$WORKDIR/genomes/rebecca/cmfull
seq=$WORKDIR/genomes/rebecca/rebecca_blastableheaders.fasta
log=$WORKDIR/genomes/rebecca/qsubmit.log
Z=1
for cm in `grep -l cmcalibrate rfam/RF00028_v11.cm  gissd/I*_v11.cm`; do
    res=$resdir"/allbacteria_"$(basename ${cm%.*})
    echo "ompi_cmsearch -Z" $Z "-o" $res.out "-A" $res.stk "--tblout" $res.tab $cm $seq >> $log
    qsub scripts/ompi_cmsearch -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq >> $log
        #cmsearch -A $res.stk --tblout $res.tab $cm $seq > $res.out
done
#mv cmsearch.* $resdir/log

## construct hit candidates
scripts/chainCMResults.R --dir genomes/rebecca  --cmdir=cmsearch  --cmfull=cmfull --fend=_v11 --gdf=/homes/biertank/raim/work/introns/phylogeny/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --out=genomes/rebecca/results --inc --store --maxr=0 --maxd=5000 --minl=2 --ovlf=TRUE

