
### SETUP GENOMES
## at LPZG

genbro=~/programs/genomeBrowser
srcpath=~/programs/gIScanner
datpath=/scr/doofsan/raim/data/introns
sampath=/usr/local/bin # samtools
eslpath=~/bin # easel miniapps - indexing fasta for infernal
infpath=/usr/local/infernal # infernal executables
export INFERNALDIR=$infpath

cd $datpath

## ALL PROKARYOTIC GENOMES
## unpack genomes: generates folder "seeds" - delete later to save space
tar zxvf $datpath/bacGenomes.tgz -C $datpath

## circularize genomes
cat `find $datpath -name contigs` > $datpath/tmp.fa
ulimit -Sv 500000000
$srcpath/scripts/circularizeSeq.R --seq $datpath/tmp.fa --ext 5000 > $datpath/allcontigs.fa

\rm tmp.fa seeds/ -rf

## generate infernal index
$eslpath/esl-sfetch --index $datpath/allcontigs.fa

## generate genomeBrowser index
#$genbro/src/fastaindex.R $datpath/allcontigs.fa > $datpath/allcontigs.idx
$sampath/samtools faidx $datpath/allcontigs.fa
cut -f1-2 $datpath/allcontigs.fa.fai > $datpath/allcontigs.idx

## genome IDs
grep ">" $datpath/allcontigs.fa > $datpath/allcontigs.ids


## SGE CLUSTER AT LEIPZIG
srcpath=~/programs/gIScanner
datpath=/scr/doofsan/raim/data/introns
infpath=/usr/local/infernal # infernal executables
export INFERNALDIR=$infpath

## subCM search
seq=$datpath/allcontigs.fa
resdir=$datpath/results/subcms
logdir=$resdir/log
mkdir -p $logdir
Z=1


## TODO: use cpu times from phages to do short/long jobs separately
## current in cputimes_2018.sh

## loop through calibrated models (in steps!)
## NOTE: to be used interactively and repeatedly with changed qsub settings
qstat -r |grep "jobname"  | sed 's/.*jobname: \+//g' > running.txt
for cm in `grep -l cmcalibrate $srcpath/parameters/subcms/*cm |head -n 299 |tail -n 21`; do
    cmf=$(basename ${cm%.*})
    res=${resdir}/allcontigs_${cmf}
    job=scan_$cmf
    if [ -s $res.tab ]; then
	echo "results exist: $cmf"
    elif grep --quiet $cmf running.txt; then
	echo "job registered: $cmf"
    else
	echo "do scan_$cmf"
	echo "${INFERNALDIR}/bin/cmsearch --cpu 32 -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq" | qsub -q normal.q -pe para 32 -N $job -o $logdir -e $logdir -l s_rt=02:00:00 -l h_rt=02:30:00 -l h_vmem=50G
    fi
done
## NOTE: only some jobs finished in Lpzg, collecting finished jobs,
## and do rest at HHU (20180815) and Lpzg in parallel
hhupath=/gpfs/project/machne/data/introns
\rm missing_20180815.txt
for cm in `grep -l cmcalibrate $srcpath/parameters/subcms/*cm`; do
    cmf=$(basename ${cm%.*})
    res=${resdir}/allcontigs_${cmf}
    job=scan_$cmf
    if [ -s $res.tab ]; then
	echo "results exist: $cmf"
    elif grep --quiet $cmf running.txt; then
	echo "job registered: $cmf"
    else
	echo "do scan_$cmf"
	echo $cm |sed  "s|$srcpath/parameters|$hhupath|" >> missing_20180815.txt
	#echo "${INFERNALDIR}/bin/cmsearch --cpu 32 -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq" | qsub -q normal.q -pe para 32 -N $job -o $logdir -e $logdir -l s_rt=02:00:00 -l h_rt=02:30:00 -l h_vmem=50G
    fi
done
## CLEAN UP - REMOVE EMPTY!
## note: including empty .stk files for which results exist!
\rm `find results/subcms/  -maxdepth 1 -type f -name "allcontigs*" -exec wc -c {} + | sort -nr |grep "^ \+0 "|grep -v .stk | sed 's/ \+0 //'`

## RUN MISSING IN REVERSE IN LPZG
## (HHU HPC (below) keeps re-starting finished/killed jobs ....)
infpath=/usr/local/infernal # infernal executables
export INFERNALDIR=$infpath
srcpath=~/programs/gIScanner
hhupath=/gpfs/project/machne/data/introns
datpath=/scr/doofsan/raim/data/introns
seq=$datpath/allcontigs.fa
resdir=$datpath/results/subcms
logdir=$resdir/log2
mkdir -p $logdir
Z=1
qstat -r |grep "jobname"  | sed 's/.*jobname: \+//g' > $datpath/running.txt
for cm in `tac $datpath/missing_20180815.txt|head -n 120|tail -11`; do
    cm=`echo $cm|sed "s|$hhupath|$srcpath/parameters|"`
    cmf=$(basename ${cm%.*})
    res=${resdir}/allcontigs_${cmf}
    job=scan_$cmf
    if [ -s $res.tab ]; then
	echo "results exist: $cmf"
    elif grep --quiet $cmf $datpath/running.txt; then
	echo "job registered: $cmf"
    else
	echo "do scan_$cmf"
	echo "${INFERNALDIR}/bin/cmsearch --cpu 32 -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq" | qsub -q normal.q -pe para 32 -N $job -o $logdir -e $logdir -l s_rt=72:00:00 -l h_rt=72:30:00 -l h_vmem=71G
    fi
done


## CURRENT MISSING IN LPZG
datpath=/scr/doofsan/raim/data/introns
resdir=$datpath/results/subcms
\rm $datpath/missing_lpzg.txt
qstat -r |grep "jobname"  | sed 's/.*jobname: \+//g' > $datpath/running.txt
for cm in `grep -l cmcalibrate $srcpath/parameters/subcms/*cm`; do
    cmf=$(basename ${cm%.*})
    res=${resdir}/allcontigs_${cmf}
    job=scan_$cmf
    if [ -s $res.tab ]; then
	echo "results exist: $cmf"
    elif grep --quiet $cmf running.txt; then
	echo "job registered: $cmf"
    else
	echo "do scan_$cmf"
	echo $cmf  >> $datpath/missing_lpzg.txt
    fi
done

## time of a job used for phages
for job in `qstat | cut -f 2 -d ' '|grep -v "\-\-"`; do
    grep `qstat -j $job|grep job_name |sed 's/.*scan_//g'` $datpath/allcms_phages_times.txt
done | sort -k2 -n
## model length  
for job in `qstat | cut -f 2 -d ' '|grep -v "\-\-"`; do
    grep `qstat -j $job|grep job_name |sed 's/.*scan_//g'` $datpath/allcms_clength.txt
done 
 

## CURRENT MISSING AT HHU HPC
## NOTE: includes running!
datpath=/gpfs/project/machne/data/introns
resdir=$datpath/results/subcms
\rm $datpath/missing_hhu.txt
for cm in `grep -l cmcalibrate $datpath/subcms/*cm`; do
    cmf=$(basename ${cm%.*})
    res=${resdir}/allcontigs_${cmf}
    job=scan_$cmf
    if [ -s $res.tab ] && grep -v --quiet "^DONT$" $res.tab; then
	echo "results exist: $cmf"
    else
	echo "do scan_$cmf"
	echo $cmf  >> $datpath/missing_hhu.txt
    fi
done

## FINAL RUN - LONG JOBS
## collected on intron/hhu exon, and rsync'ed sorted missing_LONG.txt

infpath=/usr/local/infernal # infernal executables
export INFERNALDIR=$infpath
srcpath=~/programs/gIScanner
hhupath=/gpfs/project/machne/data/introns
datpath=/scr/doofsan/raim/data/introns
seq=$datpath/allcontigs.fa
resdir=$datpath/results/subcms
logdir=$resdir/loglong
mkdir -p $logdir
Z=1
qstat -r |grep "jobname"  | sed 's/.*jobname: \+//g' > $datpath/running.txt
for cmf in `cat $datpath/missing_LONG.txt`; do
    cm=$srcpath/parameters/subcms/${cmf}.cm
    res=${resdir}/allcontigs_${cmf}
    job=scan_$cmf
    if [ -s $res.tab ]; then
	echo "results exist: $cmf"
    elif grep --quiet $cmf $datpath/running.txt; then
	echo "job registered: $cmf"
    else
	echo "do scan_$cmf"
	echo "${INFERNALDIR}/bin/cmsearch --cpu 32 -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq" | qsub -q normal.q -pe para 32 -N $job -o $logdir -e $logdir 
    fi
done
#echo "/usr/lib64/openmpi/bin/mpirun -np 20 ${INFERNALDIR}/bin/cmsearch --mpi -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq" | qsub -pe mpirun 20-30 -N $job -o $logdir -e $logdir -l h_vmem=90G

## TODO: long jobs (IA1/RFAM P7/P8) separetely with many CPUs and
## high memory; using MPI at Lpzg

##  COLLECT RESULTS:
## use qacct on job IDs in log to get used memory/time etc.
## currently in cputimes_2018.sh


## ALLCONTIGS AT HHU

## ssh -Y machne@hpc.rz.uni-duesseldorf.de
## rsync -avz raim@doof.bioinf.uni-leipzig.de:/scr/doofsan/raim/data/introns/allcontigs.fa /gpfs/project/machne/data/introns/
## rsync -avz raim@doof.bioinf.uni-leipzig.de:/scr/doofsan/raim/data/introns/allcontigs.fa.ssi /gpfs/project/machne/data/introns/
## rsync -avz raim@doof.bioinf.uni-leipzig.de:/homes/biertank/raim/programs/gIScanner/parameters/subcms /gpfs/project/machne/data/introns/
## rsync -avz raim@doof.bioinf.uni-leipzig.de:/scr/doofsan/raim/data/introns/missing_20180815.txt /gpfs/project/machne/data/introns/

srcpath=/home/machne/programs/gIScanner
datpath=/gpfs/project/machne/data/introns
resdir=$datpath/results/subcms
mem=85gb
ncpus=20
walltime=72:00:00
logdir=$resdir/log10
mkdir -p $logdir
\cp $srcpath/scripts/bacteria_2018_hhu.sh allcontigs_script.sh
sed -i "s/ZETT/$Z/g;s/NCPU/$ncpus/g;s/GENOME/allcontigs/g;" allcontigs_script.sh
num=`wc -l allcms.txt|sed 's/ allcms.txt//'`
qsub -J 1-${num} -l select=1:ncpus=${ncpus}:mem=${mem} -l walltime=$walltime -N scan_gImodels -A Coilseq -r y -e $logdir -o $logdir allcontigs_script.sh

## NOTE: about 7-8 done this way, but cancelled; retry missing via MPI
\rm `find results/subcms/  -maxdepth 1 -type f -name "allcontigs*" -exec wc -c {} + | sort -nr |grep "^ \+0 "|grep -v .stk | sed 's/ \+0 //'`


### RUN MISSING at HHU/Hilbert via MPI
## NOTE: first results via testmpi copied to results/subcms and results/submcs/logmpi manually!
srcpath=/home/machne/programs/gIScanner
datpath=/gpfs/project/machne/data/introns
Z=1
resdir=$datpath/results/subcms
logdir=$resdir/logmpi
\cp $srcpath/scripts/bacteria_2018_mpi.sh allcontigs_mpi.sh
sed -i "s/ZETT/$Z/;s/allcms.txt/missing_20180815.txt/" allcontigs_mpi.sh

qsub -J 1-`wc -l missing_20180815.txt|sed 's/ missing.*//'` -l walltime=72:00:00 -l select=1:ncpus=100:mpiprocs=100:mem=85gb -N scan_gImodels -A Coilseq -r y -e $logdir -o $logdir allcontigs_mpi.sh
# qstat -f 1474763[].hpc-batch14

## TODO: several IA1 jobs killed because they would break walltime

## LONG JOBS at Hilbert HHU HPC
## NOTE: 20180827 - stopped missing_20180815.txt job since they
## are finished!
## qdel 1474763[].hpc-batch14

srcpath=/home/machne/programs/gIScanner
datpath=/gpfs/project/machne/data/introns
Z=1
resdir=$datpath/results/subcms
logdir=$resdir/longcms2
mkdir -p $logdir
\cp $srcpath/scripts/bacteria_2018_mpi.sh longcms_mpi.sh
sed -i "s/ZETT/$Z/;s/allcms.txt/missing_longcms.txt/" longcms_mpi.sh

cat $datpath/missing_LONG.txt | sed "s|^|$datpath/subcms/|;s/$/.cm/" > missing_longcms.txt

qsub -J 1-`wc -l missing_longcms.txt|sed 's/ missing.*//'` -l walltime=72:00:00 -l select=1:ncpus=200:mpiprocs=200:mem=85gb -N scan_gImodels -A Coilseq -r y -e $logdir -o $logdir longcms_mpi.sh
# qstat -f 1535724[].hpc-batch14

## redo 1: IA1_orig_annotated_R5-P6 (after removing DONT in files),
## but finished in Lpzg
## redo 2: RF00028_seed.stockholm_annotated_R6-P7_r112 : memory exceeded
## redo 3: RF00028_seed.stockholm_annotated_P7-P8_r112 : memory exceeded
## ...

#2: =>> PBS: job killed: mem 94716392kb exceeded limit 89128960kb
#3: =>> PBS: job killed: mem 94195260kb exceeded limit 89128960kb
#4: =>> PBS: job killed: mem 92912580kb exceeded limit 89128960kb
#5: =>> PBS: job killed: mem 167287816kb exceeded limit 89128960kb
#6: =>> PBS: job killed: mem 161948924kb exceeded limit 89128960kb

logdir=$resdir/longcms2
mkdir -p $logdir
qsub -J 2-4 -l walltime=72:00:00 -l select=1:ncpus=200:mpiprocs=200:mem=100gb -N scan_gImodels -A Coilseq -r y -e $logdir -o $logdir longcms_mpi.sh
# qstat -f 1564423[].hpc-batch14
# 2: =>> PBS: job killed: mem 105323732kb exceeded limit 104857600kb
# 3-4: qdel
logdir=$resdir/longcms3
mkdir -p $logdir
qsub -J 2-3 -l walltime=72:00:00 -l select=1:ncpus=200:mpiprocs=200:mem=200gb -N scan_gImodels -A Coilseq -r y -e $logdir -o $logdir longcms_mpi.sh
# qstat -f 1564629[].hpc-batch14

## new script, fixed logging
\cp $srcpath/scripts/bacteria_2018_mpi.sh longcms_mpi2.sh
sed -i "s/ZETT/$Z/;s/allcms.txt/missing_longcms.txt/" longcms_mpi2.sh
logdir=$resdir/longcms4
mkdir -p $logdir
qsub -J 4-7 -l walltime=72:00:00 -l select=1:ncpus=200:mpiprocs=200:mem=300gb -N scan_gImodels -A Coilseq -r y -e $logdir -o $logdir longcms_mpi2.sh
# qstat -f 1607207[].hpc-batch14
# -> only #4 done; rest exceeded memory

## more RAM required for IA1
logdir=$resdir/longcms5
mkdir -p $logdir
qsub -J 5-7 -l walltime=72:00:00 -l select=1:ncpus=200:mpiprocs=200:mem=500gb -N scan_gImodels -A Coilseq -r y -e $logdir -o $logdir longcms_mpi2.sh
# qstat -f 1669703[].hpc-batch14


### fullCM search
## at LPZG

seq=$datpath/allcontigs.fa
resdir=$datpath/results/fullcms
logdir=$resdir/log
mkdir -p $logdir
Z=1
qstat -r |grep "jobname"  | sed 's/.*jobname: \+//g' > running.txt
for cm in `grep -l cmcalibrate $srcpath/parameters/fullcms/*cm`; do
    cmf=$(basename ${cm%.*})
    res=${resdir}/allcontigs_${cmf}
    if [ -s $res.tab ]; then
	echo "$cmf exists"
    elif grep --quiet $cmf running.txt; then
	echo "$cmf registered"
    else
	echo "$cmf CALCULATE"
	echo "${INFERNALDIR}/bin/cmsearch --cpu 32 -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq" | qsub -q normal.q -pe para 32 -r y -N $cmf -o $logdir -e $logdir
    fi
done


### COLLECT & CHAIN
## currently in bactera_2018_collect.sh


