# phispydirs.sh
#!/bin/sh
##
## phispydirs.sh
## 
## Made by Rainer Machne
## Login   <raim@intron.tbi.univie.ac.at>
## 
## Started on  Wed Feb 27 14:36:36 2013 Rainer Machne
## Last update Wed Feb 27 14:36:36 2013 Rainer Machne
##

## takes a file with a list of directories in the first column, and for
## each directory, look for .gbk files and generate phispy data directories

d2s=$1
gbd=$2

O=$IFS ## spaces in cut string
IFS=$(echo -en "\n\b")
for spec in `cat $d2s`; do
    dir=`echo $spec|cut -f1`
    #tax=`echo $spec|cut -f2`
    if [ ! -d "$gbd/$dir" ]; then
	echo "WARNING" $dir "does not exist"
    fi
    gbk=`ls $gbd/$dir/*.gbk`
    echo $dir #$tax
    for gbk in `ls $gbd/$dir/*.gbk`; do
	
	seed=`echo $gbk|sed 's/\.gbk$/_seed/g'`
	phage=`echo $seed|sed 's/_seed/_phage/g'`
	if [[ -d "$seed/Features" && -d "$phage" ]]; then
	    echo "$seed $phage EXIST"
	else 
	    echo "GENERATING $seed and $phage"
	    mkdir -p $seed $phage
	    python $PHISPY/genbank_to_seed.py $gbk $seed
	fi
    done
done
IFS=$O




# End of file
