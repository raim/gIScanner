#!/bin/bash

# A wrapper for ompi_cmalign use e.g., as
# scripts/cmalignall.sh rfam/RF00028_v11.cm gissd/I*fasta gissd/gissd.fasta

## TODO: generalize, rm gissd/rfam specific code!

ompi_cmalign="/scr/trieb/raim/programs/tataProject/introns/scripts/ompi_cmalign"

args=("$@")
ln=${#args[@]}
echo $@ $ln
for (( i=1;i<$ln;i++)); do
    f=${args[${i}]}
    j=`echo $f|sed 's/gissd\//rfam\/giisd_/g;s/.fasta/_RF00028_v11.stk/g'`
    f=${args[${i}]}
    cm=$(basename ${1%.*})
    o=$(basename ${f%.*})"_"$cm".stk"
    cmargs="-o $o $1 $f"
    echo $cmargs
    qsub $ompi_cmalign $cmargs
done
