#!/usr/bin/perl -w

## read a list of sequence IDs (acc&gi), the taxonomy-name mapping file
## and print-out while parsing the huge ncbi taxonomy-sequence mapping
# perl scripts/seq2spec.pl genomes/bacteria/allbacteria_seqIDs.dat
## TODO: make more flexibile wrt sequence IDs


use strict;
use Getopt::Long;
use File::Spec qw(catpath);
use File::Basename;


my $volume = "";
my $taxdir = "taxonomy/";
my $nameFile = "names.dmp";
my $specFile = "gi_taxid_nucl.dmp";

usage() unless GetOptions("tx=s"=> \$taxdir,
			  "h"   => \&usage);

sub usage
{    
    printf STDERR "\n";
    printf STDERR "Reads  a list of sequence IDs (gi&refseq) and \n";
    printf STDERR "NCBI taxonomy-name mapping files (names.dmp & \n";
    printf STDERR "gi_taxid_nucl.dmp) and writes a tab-delimited\n";
    printf STDERR "sequences <-> species mapping (gi refseq species taxonid)\n";
    printf STDERR "akin to the index file written by fetchFastaFromNCBI.pl \n";
    printf STDERR "(but w/o circular indicator) \n\n";

    printf STDERR "\tUsage: @{[basename($0)]} [options] <gi\\trefseq file>\n";
    printf STDERR "\n";
    
    printf STDERR "\t-tx=s\ttaxonomy directory, current: $taxdir\n";
    printf STDERR "\n";
    exit;
}
$nameFile = File::Spec->catpath($volume, $taxdir, $nameFile );
$specFile = File::Spec->catpath($volume, $taxdir, $specFile );

## parse target sequences, acc as value, gi as key
my %ref;
my %seqName;
while(<>){
    my @vals= split '\t';
    chomp(@vals);
    $ref{$vals[0]} = $vals[1];
    $seqName{$vals[0]} = $vals[2];
}

### parse species names, species name as values, tax id as key
open(NAM, "<$nameFile") or die "can't open species name file at $nameFile\n";
my %name;
while(<NAM>) {
    chomp;
    my @vals = split '\t\|\t';
    chomp(@vals);
    next unless $vals[3]=~ /scientific name/;
    $name{$vals[0]} = $vals[1];
}

## parse gi2tax mapping, print on the fly
## TODO: record $found and report missing from input file <>!!
open(TAX, "<$specFile") or die "can't open taxonomy file at $specFile\n";
print "gi\tacc\tseq\tspecies\ttaxon\n";
my %found;
while(<TAX>) {
    my @vals= split '\t';
    chomp(@vals);
    my $gi = $vals[0];
    my $tax = $vals[1];
    next unless defined $ref{$gi};
    $found{$gi} = 1;
    print "$gi\t$ref{$gi}\t$seqName{$gi}\t$name{$tax}\t$tax\n";
}

foreach my $gi ( keys %ref ) {
    printf STDERR "WARNING\t$gi, $seqName{$gi} not found in taxonomy\n"
	if ( !defined($found{$gi}) );
}

## 


