
## PFAM-related data

pfampath=~/data/pfam

## download PFAM Clans
## downloaded on 20181011
cd $pfampath
wget ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/Pfam-A.clans.tsv.gz
gunzip Pfam-A.clans.tsv.gz


## dl PFAM2GO
wget http://supfam.org/SUPERFAMILY/Domain2GO/PFAM2GO.txt.gz
