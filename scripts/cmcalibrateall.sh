#!/bin/bash

## NOTE 201808 : updated for latest infernal release, but not
## tested or used; see scripts/recalibrate_r112.sh instead

# A wrapper for ompi_cmcalibrate use e.g., as
# scripts/cmcalibrateall.sh `ls -Sr subcms/*.cm` >& subcms/calibration.log
# OR:
# scripts/cmcalibrateall.sh `ls -Sr subcms/*.cm` forecast >& subcms/forecast.log
# ie. add last argument "forecast" to use forecasting
# OR: for new infernal
# scripts/cmcalibrateall.sh `ls -Sr subcms/*_v11.cm` forecast >& subcms/forecast_v11.log


## NOTE: if not used for forecasting, this will submit an Open MPI
## script (ompi_cmcalibrate) to an SGE gridengine; EDIT THIS SCRIPT
## TO YOUR REQUIREMENTS

args=("$@")
ln=${#args[@]}
#echo $@ $ln

export LD_LIBRARY_PATH=/usr/lib64/openmpi/lib


cmscanner=~/programs/gIScanner/scripts
cmcalibrate=${INFERNALDIR}/bin/cmcalibrate

#version
version=`$cmcalibrate -h |grep INFERNAL | sed 's/# INFERNAL //g;s/(.*//g'| tr -d ' '`


end="calibration.log"
if [ $version = "1.0.2" ]; then
    ompi_cmcalibrate=${cmscanner}/ompi_cmcalibrate102
    end="calibration.log"
fi
if [ $version = "1.1rc1" ]; then
    ompi_cmcalibrate=${cmscanner}/ompi_cmcalibrate
    end="v11_calibration.log"
fi
if [ $version = "1.1.2" ]; then
    ompi_cmcalibrate=${cmscanner}/ompi_cmcalibrate112
    end="v112_calibration.log"
fi
	    

t=""
if [ ${args[${ln}-1]} = "forecast" ]; then
    t="--forecast"
    end="forecast_v11.log"
    if [ $version = "1.0.2" ]; then
	t="--forecast 1"
	end="forecast.log"
    fi
    if [ $version = "1.1rc1" ]; then
	end="v11_forecast.log"
    fi
    if [ $version = "1.1.2" ]; then
	end="v112_forecast.log"
    fi
    ln=$ln-1
fi


for (( i=0;i<$ln;i++)); do
    o=${args[${i}]}
    c=${o%.*}_$end
    if [ "$t" = "" ]; then
        echo "qsub" $ompi_cmcalibrate $o ">" $c
	qsub $ompi_cmcalibrate $o > $c
	## NEW: pthreads instead of mpi
	#echo "$cmcalibrate --cpu 90 $o" | qsub -q normal.q -pe para 90 -r y -N `basename $o` -cwd 
    else
	echo $cmcalibrate $t $o ">" $c
	$cmcalibrate $t $o > $c
    fi    
done
