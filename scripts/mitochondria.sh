# mitochondria.sh
## MITOCHONDRIAL GENOMES of METAZOA

## TODO: add species name to fasta headers

## generate gff files from genbank files
for gb in `find genomes/mitochondria/ -name "*.gb"`; do
    gff=`echo $gb | sed 's/gb$/gff/'`
    echo $gb $gff
    $TATADIR/perls/genbank2gff.pl -trans -semicol $gb > $gff
done

## generate index
for seq in `ls -1 genomes/mitochondria/refseq55/metazoa/*.fas | sed 's|.*/||;s/\.fas//g'`; do
    echo -e "$seq\tmetazoa"
done > genomes/mitochondria/taxonomy.dat
for seq in `ls -1 genomes/mitochondria/refseq55/non-metazoa/*.fas | sed 's|.*/||;s/\.fas//g'`; do
    echo -e "$seq\tnon-metazoa"
done >> genomes/mitochondria/taxonomy.dat

## circularize
log=genomes/mitochondria/circularize.log
\rm $log
\rm genomes/mitochondria/allmitochondria.fasta
for seq in `find genomes/mitochondria/ -name "*.fas"`; do
    echo $seq >> $log
    scripts/circularizeSeq.R --seq $seq --ext 5000 --verb >> genomes/mitochondria/allmitochondria.fasta 2>> $log
    echo "done." >> $log
done


## generate index for infernal
echo "INDEXING FOR INFERNAL"
/home/mescalin/choener/tmp/infernal/infernal-1.1rc1/easel/miniapps/esl-sfetch --index genomes/mitochondria/allmitochondria.fasta

## subCM search
export WORKDIR=~/work/introns/phylogeny/
resdir=$WORKDIR/genomes/mitochondria/cmsearch
seq=$WORKDIR/genomes/mitochondria/allmitochondria.fasta
logdir=$resdir/log
log=$logdir/log
Z=.016
touch $log
for cm in `grep -l cmcalibrate subcms4/*cm`; do
    cmf=$(basename ${cm%.*})
    res=$resdir"/allmitochondria_"$cmf
    sed -i "s/^#\$ -N cmsearch.*/#$ -N cmsearch_${cmf}/g" scripts/ompi_cmsearch
    echo "ompi_cmsearch -Z" $Z " -o" $res.out "-A" $res.stk "--tblout" $res.tab $cm $seq >> $log
    qsub -e $logdir -o $logdir scripts/ompi_cmsearch -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq >> $log
done


## full CM search
export WORKDIR=~/work/introns/phylogeny/
resdir=$WORKDIR/genomes/mitochondria/cmfull
seq=$WORKDIR/genomes/mitochondria/allmitochondria.fasta
logdir=$resdir/log
log=$logdir/log
Z=1
for cm in `grep -l cmcalibrate rfam/RF00028_v11.cm  gissd/I*_v11.cm`; do
    cmf=$(basename ${cm%.*})
    res=$resdir"/allmitochondria_"$cmf
    sed -i "s/^#\$ -N cmsearch.*/#$ -N cmsearch_${cmf}/g" scripts/ompi_cmsearch
    echo "ompi_cmsearch -Z" $Z "-o" $res.out "-A" $res.stk "--tblout" $res.tab $cm $seq >> $log
    qsub -e $logdir -o $logdir scripts/ompi_cmsearch -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq >> $log
done
# for job in `qstat |cut -f 1 -d ' ' |grep 1877 |l`; do echo hallo $job; qalter -pe ompi 5-10 $job; done

## construct hit candidates
scripts/chainCMResults.R --dir genomes/mitochondria --cmdir=cmsearch --cmfull=cmfull --fend=_v11 --gdf=substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --out=genomes/mitochondria/results --inc --exp --store --maxr=0 --maxd=5000 --minl=3 --ovlf=TRUE --plot --fig=genomes/mitochondria/results/figures  --chs=genomes/mitochondria/results/chains --gff genomes/mitochondria/refseq55/ ## --dof

## potential introns:

## HEXACORALLIA
## NC_008071, Discosoma sp. CASIZ 168915: COX1 annotated vs. large in ND5?
## NC_008072, Discosoma sp. CASIZ 168916: COX1
## NC_015143, Lophelia pertusa (Hexacorallia)
## NC_008158, Rhodactis sp. CASIZ 171755: COX1
## NC_008159, Ricordea florida: COX1
## NC_008164, Nematostella sp. JVK-2006
## NC_008166, Porites porites: COX1
## NC_008167, Siderastrea radians: COX1 annotated vs. large in ND5?
## NC_008411, Chrysopathes formosa: large NADH5 intron
## NC_015640, Fungiacyathus stephanus (Hexacorallia): COX1
## NC_015643, Goniopora columna (Hexacorallia): COX1
## NC_015644, Porites okinawensis: COX1
## NC_018377, Stichopathes lutkeni: COX1
## NC_009797, Pocillopora damicornis: NADH5
## NC_009798, Pocillopora eydouxi
## NC_010244, Seriatopora hystrix
## NC_011160, Madracis mirabilis: NADH5
## NC_011162, Stylophora pistillata



## PLACOZOA
## NC_008151, Trichoplax adhaerens: large??
## NC_008827, Savalia savaglia: COX1
## NC_008832, Placozoan sp. BZ10101
## NC_008833, Placozoan sp. BZ49
## NC_008834
## NC_015309, Placozoan sp. 'Shirahama': COX1



## PORIFERA/Homoscleromorpha/Plakina
## NC_010217, Plakinastrella cf. onkodes DVL-2011: COX1
## NC_014852, Plakina trilopha (Porifera): COX1
## NC_014885, Plakina crypta: COX1


## MOLUSCA - unlikely!
## NC_013833, Tricula hortensis: ND5, but unlikely

# End of file
