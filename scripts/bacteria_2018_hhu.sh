#!/bin/bash

module load Infernal/1.1.2



## paths
genome=GENOME
datpath=/gpfs/project/machne/data/introns
seq=$datpath/${genome}.fa
infile=$datpath/allcms.txt
resdir=$datpath/results/subcms

Z=ZETT
ncpus=NCPU

## current job
cm=$(sed "${PBS_ARRAY_INDEX}q;d" $infile)

## output names
cmf=$(basename ${cm%.*})
res=${resdir}/${genome}_${cmf}

echo "START $cmf" # .OU file output
echo "`date +"%d.%m.%Y-%T"`"

cmsearch --cpu $ncpus -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq

qstat -f -x $PBS_JOBID 

echo "DONE WITH $PBS_JOBID ($PBS_JOBNAME) @ `hostname` at `date`"
echo "`date +"%d.%m.%Y-%T"`"
