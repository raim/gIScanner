#!/bin/bash

# A wrapper to emit consensus structure and sequence from multiple CMs 
# scripts/cmemitall.sh gissd/*.cm


cmemit=$INFERNALDIR"/bin/cmemit"

args=("$@")
ln=${#args[@]}
#echo $@ $ln

for (( i=0;i<$ln;i++)); do
    f=${args[${i}]}
    o=${f%.*}_cmemit.fasta
    echo $f $o
    $cmemit -a $f > /tmp/cmemit_tmp.stk
    # now grep consensus structure and sequence 
    rf=`grep RF /tmp/cmemit_tmp.stk  | sed 's/#=GC RF//g;s/\.//g;s/ //g' | tr '\n' ' '| tr -d ' '`
    ss=`grep SS_cons /tmp/cmemit_tmp.stk | sed 's/#=GC SS_cons//g;s/\.//g;s/ //g' | tr '\n' ' ' | tr -d ' '`
    ## convert to dot-bracket notation
    dt=`echo $ss | sed 's/_/./g;s/-/./g;s/,/./g;s/:/./g;s/</(/g;s/>/)/g;s/\[/(/g;s/\]/)/g;s/{/(/g;s/}/)/g'`
    echo -e ">RF:" $f"\n"$rf"\n>SS_dtb:" $f"\n"$dt"\n>SS_cons:" $f"\n"$ss > $o    
done
