
PF14890: Intein_splicing
PF05203: Hom_end_hint; homing endoncuclease with intein activity
PF09062: Endonuc_subdom;  intein-encoded homing endonuclease PI-PfuI
PF14527: LAGLIDADG_WhiA;



NOT inteins:
Hint (PF01079): "alignment of the Hint module in the Hedgehog proteins. It does not include any Inteins which also possess the Hint module"
