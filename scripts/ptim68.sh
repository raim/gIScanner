## CYANOPHAGE
## downloaded from 
## https://www.ncbi.nlm.nih.gov/nuccore/NC_028955.1?report=fasta
echo "GENERATING BLAST DB"
formatdb -i  genomes/P-TIM68/sequence.fasta -p F -o T
## search gII introns
#blastn -query genomes/rebecca/gII_20130204_intronsonly.fasta -db genomes/rebecca/rebecca_blastableheaders.fasta -outfmt "7 qseqid sacc pident length qstart qend sstart send  evalue bitscore" > genomes/rebecca/rebecca_vs_gII_20130204_intronsonly.dat
#blastn -query genomes/rebecca/gII_20130204_intronsonly.fasta -db genomes/rebecca/rebecca_blastableheaders.fasta   > genomes/rebecca/rebecca_vs_gII_20130204_intronsonly.out

## circularize
seq=genomes/P-TIM68/sequence.fasta
log=genomes/P-TIM68/circularize.log
scripts/circularizeSeq.R --seq $seq --ext 5000 --verb > genomes/P-TIM68/sequence_circ.fasta 

## generate index for infernal
echo "INDEXING FOR INFERNAL"
~/programs/infernal-1.1.2/easel/miniapps/esl-sfetch --index genomes/P-TIM68/sequence_circ.fasta

## subCM search
export WORKDIR=~/work/introns/phylogeny/
resdir=$WORKDIR/genomes/P-TIM68/cmsearch
seq=$WORKDIR/genomes/P-TIM68/sequence.fasta
logdir=$resdir/log
log=$logdir/log
mkdir -p $logdir
Z=1
touch $log
for cm in `grep -l cmcalibrate subcms4/*cm`; do
    cmf=$(basename ${cm%.*})
    res=$resdir"/sequence_"$cmf
    sed -i "s/^#\$ -N cmsearch.*/#$ -N cmsearch_${cmf}/g" scripts/ompi_cmsearch
    echo "ompi_cmsearch -Z" $Z " -o" $res.out "-A" $res.stk "--tblout" $res.tab $cm $seq >> $log
    qsub -e $logdir -o $logdir scripts/ompi_cmsearch -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq >> $log
done


## full CM search
export WORKDIR=~/work/introns/phylogeny/
resdir=$WORKDIR/genomes/P-TIM68/cmfull
seq=$WORKDIR/genomes/P-TIM68/sequence.fasta
logdir=$resdir/log
log=$logdir/log
Z=1
for cm in `grep -l cmcalibrate rfam/RF00028_v11.cm  gissd/I*_v11.cm`; do
    cmf=$(basename ${cm%.*})
    res=$resdir"/sequence_"$cmf
    sed -i "s/^#\$ -N cmsearch.*/#$ -N cmsearch_${cmf}/g" scripts/ompi_cmsearch
    echo "ompi_cmsearch -Z" $Z "-o" $res.out "-A" $res.stk "--tblout" $res.tab $cm $seq >> $log
    qsub -e $logdir -o $logdir scripts/ompi_cmsearch -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq >> $log
done
# for job in `qstat |cut -f 1 -d ' ' |grep 1877 |l`; do echo hallo $job; qalter -pe ompi 5-10 $job; done

## construct hit candidates
scripts/chainCMResults.R --dir genomes/P-TIM68 --cmdir=cmsearch --cmfull=cmfull --fend=_v11 --gdf=/homes/biertank/raim/work/introns/phylogeny/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --out=genomes/P-TIM68/results --inc --exp --store --maxr=0 --maxd=5000 --minl=2 --ovlf=TRUE --dof --plot --dev eps --fig=genomes/P-TIM68/results/figures  --chs=genomes/P-TIM68/results/chains

