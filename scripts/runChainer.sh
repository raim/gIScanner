##MIA's GENOME
## retrieved manually from http://www.biomol.unb.br/siteBacilo/
## and re-added original NCBI fasta headers
echo "GENERATING BLAST DB"
formatdb -i  genomes/mia/FT9.fasta -p F -o T
## search gII introns
#blastn -query genomes/rebecca/gII_20130204_intronsonly.fasta -db genomes/rebecca/rebecca_blastableheaders.fasta -outfmt "7 qseqid sacc pident length qstart qend sstart send  evalue bitscore" > genomes/rebecca/rebecca_vs_gII_20130204_intronsonly.dat
#blastn -query genomes/rebecca/gII_20130204_intronsonly.fasta -db genomes/rebecca/rebecca_blastableheaders.fasta   > genomes/rebecca/rebecca_vs_gII_20130204_intronsonly.out

## circularize
log=genomes/mia/circularize.log
\rm $log
\rm genomes/mia/FT9.fasta
for seq in `ls -1 genomes/mia/scaffold*FT9.fasta`; do
    echo $seq >> $log
    scripts/circularizeSeq.R --seq $seq --ext 5000 --verb >> genomes/mia/FT9.fasta 2>> $log
    echo "done." >> $log
done


## generate index for infernal
echo "INDEXING FOR INFERNAL"
/home/mescalin/choener/tmp/infernal/infernal-1.1rc1/easel/miniapps/esl-sfetch --index genomes/mia/FT9.fasta

## subCM search
export WORKDIR=~/work/introns/phylogeny/
resdir=$WORKDIR/genomes/mia/cmsearch
seq=$WORKDIR/genomes/mia/FT9.fasta
logdir=$resdir/log
log=$logdir/log
Z=1
touch $log
for cm in `grep -l cmcalibrate subcms4/*cm`; do
    cmf=$(basename ${cm%.*})
    res=$resdir"/FT9_"$cmf
    sed -i "s/^#\$ -N cmsearch.*/#$ -N cmsearch_${cmf}/g" scripts/ompi_cmsearch
    echo "ompi_cmsearch -Z" $Z " -o" $res.out "-A" $res.stk "--tblout" $res.tab $cm $seq >> $log
    qsub -e $logdir -o $logdir scripts/ompi_cmsearch -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq >> $log
done


## full CM search
export WORKDIR=~/work/introns/phylogeny/
resdir=$WORKDIR/genomes/mia/cmfull
seq=$WORKDIR/genomes/mia/FT9.fasta
logdir=$resdir/log
log=$logdir/log
Z=1
for cm in `grep -l cmcalibrate rfam/RF00028_v11.cm  gissd/I*_v11.cm`; do
    cmf=$(basename ${cm%.*})
    res=$resdir"/FT9_"$cmf
    sed -i "s/^#\$ -N cmsearch.*/#$ -N cmsearch_${cmf}/g" scripts/ompi_cmsearch
    echo "ompi_cmsearch -Z" $Z "-o" $res.out "-A" $res.stk "--tblout" $res.tab $cm $seq >> $log
    qsub -e $logdir -o $logdir scripts/ompi_cmsearch -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq >> $log
done
# for job in `qstat |cut -f 1 -d ' ' |grep 1877 |l`; do echo hallo $job; qalter -pe ompi 5-10 $job; done

## construct hit candidates
scripts/chainCMResults.R --dir genomes/mia --cmdir=cmsearch --cmfull=cmfull --fend=_v11 --gdf=/homes/biertank/raim/work/introns/phylogeny/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --out=genomes/mia/results --inc --exp --store --maxr=0 --maxd=5000 --minl=2 --ovlf=TRUE --dof --plot --dev eps --fig=genomes/mia/results/figures  --chs=genomes/mia/results/chains

scripts/chainCMResults.R --dir genomes/mia --cmdir=cmsearch --cmfull=cmfull --fend=_v11 --gdf=substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --out=genomes/mia/results --inc --exp --store --maxr=0 --maxd=5000 --minl=2 --ovlf=TRUE --dof --plot --dev eps --fig=genomes/mia/results/figures  --chs=genomes/mia/results/chains
