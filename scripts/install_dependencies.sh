
## easel miniapps

cd ~/programs
git clone https://github.com/EddyRivasLab/easel
cd easel
autoconf

./configure
make
make check
make install

## infernal

./configure --enable-mpi
