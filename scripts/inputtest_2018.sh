#!/bin/bash

## extract all fasta sequences from input alignments,
## and apply chainer strategy to get hit patterns

## TODO: embed original fasta in randomized sequences (dinuc preservation)
## to combine to FP/FN test

## NOTE: calculated at HHU exon and Lpzg doof in parallel

gitpath=~/programs/gIScanner
sampath=~/programs/samtools # samtool, shhu exon
eslpath=~/programs/infernal-1.1.2/easel/miniapps # easel miniapps 

infpath=/usr/local/infernal/bin # lpzg, infernal executables
datpath=/scr/doofsan/raim/data/introns #lpzg

infpath=/usr/local/bin # hhu exon, infernal executables
datpath=~/data/introns/prokaryotes_2018 # hhu exon/intron

outpath=$datpath/inputtest
mkdir $outpath


## record classification
\rm $outpath/inputtest.tab
grep "#=GS" $gitpath/parameters/rfam/RF00028_seed.stockholm.stk| while read -r seq; do
    id=`echo $seq|sed 's/#=GS //g;s/ .*//'`
    echo -e "$id\tRF00028" >> $outpath/inputtest.tab
done
	   
for file in `ls $gitpath/parameters/gissd/*.sto`; do
    group=`basename $file | sed 's/\.sto//g'`
    $eslpath/esl-reformat  fasta $file | grep ">" | sed 's/>//g'| while read -r id; do
        echo -e "$id\t$group"  >> $outpath/inputtest.tab
    done
done
   
## get fasta files from original stockholm alignments
$eslpath/esl-reformat  fasta $gitpath/parameters/rfam/RF00028_seed.stockholm.stk > $outpath/inputtest.fa

for file in `ls $gitpath/parameters/gissd/*.sto`; do
  $eslpath/esl-reformat  fasta $file >> $outpath/inputtest.fa
done

## generate infernal index
$eslpath/esl-sfetch --index $outpath/inputtest.fa

## generate genomeBrowser index
$sampath/samtools faidx $outpath/inputtest.fa
cut -f1-2 $outpath/inputtest.fa.fai > $outpath/inputtest.idx

## INFERNAL SEARCH (at HHU exon)
## subCM search
seq=$outpath/inputtest.fa
resdir=$outpath/results/subcms
logdir=$resdir/log
mkdir -p $logdir
Z=1

for cm in `grep -l cmcalibrate $gitpath/parameters/subcms/*cm`; do
    cmf=$(basename ${cm%.*})
    res=${resdir}/inputtest_${cmf}
    job=scan_$cmf
    if [ -s $res.tab ]; then
	echo "results exist: $cmf"
    else
	echo "do scan_$cmf"
	${infpath}/cmsearch --cpu 16 -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq
    fi
done

## full CM search
## NOTE: repated with anytrunc!
resdir=$outpath/results/fullcms
logdir=$resdir/log
mkdir -p $logdir
for cm in `grep -l cmcalibrate $gitpath/parameters/fullcms/*cm`; do
    cmf=$(basename ${cm%.*})
    res=${resdir}/inputtest_${cmf}
    job=scan_$cmf
    if [ -s $res.tab ]; then
	echo "results exist: $cmf"
    else
	echo "do scan_$cmf"
	${infpath}/cmsearch --cpu 16 -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq
    fi
done

## full CM search - --anytrunc
## NOTE: repated with anytrunc!
resdir=$outpath/results/fullcms_anytrunc
logdir=$resdir/log
mkdir -p $logdir
for cm in `grep -l cmcalibrate $gitpath/parameters/fullcms/*cm`; do
    cmf=$(basename ${cm%.*})
    res=${resdir}/inputtest_${cmf}
    job=scan_$cmf
    if [ -s $res.tab ]; then
	echo "results exist: $cmf"
    else
	echo "do scan_$cmf"
	${infpath}/cmsearch --anytrunc --cpu 16 -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq
    fi
done

## NOTE: merging data at exon via rsync

## CHAIN RESULTS
respath=$outpath/chainer_1500
mkdir -p $respath

## call chainer
$gitpath/scripts/chainCMResults.R --dir $outpath/results  --cmd=subcms   --cfd=fullcms_anytrunc --fend=_r112 --gdf=$gitpath/parameters/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --grf=$gitpath/parameters/gI_subgroups.dat --out=$respath --fig=$respath/figures  --chs=$respath/chains --inc --store --maxr=0 --maxd=1500 --minl=3 --ovlf --plot --exp --dof ## 
