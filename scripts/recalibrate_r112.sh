#!/bin/bash

## NOTE - THIS WAS RUN ONCE MANUALLY(!) AND RESULTS ADDED TO GIT

## RE-BUILd & CALIBRATE INFERNAL COVARIANCE MODELS
## FROM ANNOTATED ALIGNMENTS -

srcpath=~/programs/gIScanner
INFERNALDIR=/usr/local/infernal # infernal executables

## TODO: re-calibrate or download rfam!

## BUILD CMs
outdir=$srcpath/parameters/subcms
mkdir $outdir
for stk in `ls -Sr $srcpath/parameters/substk/*.stk`; do
    cm=${outdir}/`basename $stk | sed 's/\.stk/_r112.cm/'`
    log=${outdir}/`basename $stk | sed 's/\.stk/_r112.log/'`
    echo $stk $cm $log
    ${INFERNALDIR}/bin/cmbuild -F $cm $stk &> $log
done

## ERRORS?
grep -i error $srcpath/parameters/subcms/*.log
#parameters/subcms/IC3_orig_annotated_left-P1_r112.log:Error: Calculating QDBs, Z got insanely large (> 1000*clen)
#parameters/subcms/IC3_orig_annotated_P1-L2_r112.log:Error: Calculating QDBs, Z got insanely large (> 1000*clen)

outdir=$srcpath/parameters/fullcms
mkdir $outdir
for stk in `ls -Sr $srcpath/parameters/gissd/*.sto`; do
    cm=${outdir}/`basename $stk | sed 's/\.sto/_r112.cm/'`
    log=${outdir}/`basename $stk | sed 's/\.sto/_r112.log/'`
    echo $stk $cm $log
    ${INFERNALDIR}/bin/cmbuild -F $cm $stk &> $log
done

## ERRORS?
grep -i error $srcpath/parameters/fullcms/*.log

## copy latest RF00028 to fullcms!
wget http://rfam.xfam.org/family/RF00028/cm # 20180805 15:35
mv cm $srcpath/parameters/fullcms/RF00028_r112.cm
echo "downloaded on 20180805 15:35" > $srcpath/parameters/fullcms/RF00028_r112.log


## FORECAST CALIBRATION TIME
## NOTE: uses SGE parallelization, call only from cluster gate k70!
## TODO: is this used anywhere? forecasted time is stored in log file
outdir=$srcpath/parameters/subcms/forecast
logdir=$outdir
mkdir $outdir
for cm in `ls -Sr $srcpath/parameters/subcms/*_r112.cm`; do
    job=`basename $cm| sed 's/\.cm/_forecast/g;s/_annotated//g;s/_orig//g'`
    echo $cm $job    
    echo "${INFERNALDIR}/bin/cmcalibrate --forecast $cm " | qsub -q normal.q -pe para 1 -r y -N $job -o $outdir -e $logdir
done


## any Errors ?
grep -i error $srcpath/parameters/subcms/forecast/*forecast*

## view forecasted times:
## TODO: use these forecasts somehow??
grep -vh "\#" $srcpath/parameters/subcms/forecast/*forecast.o*
## sort to get longest time
grep -vh "\#" $srcpath/parameters/subcms/forecast/*forecast.o* | grep -v "\[ok\]" |sed 's/ \+/ /g' | cut -d ' ' -f 3 |sort 

## NOTE:
## * errors for empty cm: IC3_left-P1, IC3_P1-L2
## * forecasted times between 5 sec and 19 min; except
##   longer times for: IC3_P1-P2 (> 1h), IA1_P6-L7, IA1_R6-P7,
##   IA1_P7-L8, IA1_R5-P6, IA1_L6-L8 (>2h), IA1_P7-P8 (>3h) 


### FORECAST CALIBRATION TIME FOR FULL CM
outdir=$srcpath/parameters/fullcms/forecast
logdir=$outdir
mkdir $outdir
for cm in `ls -Sr $srcpath/parameters/fullcms/*.cm`; do
    job=`basename $cm| sed 's/\.cm/_forecast/g;s/_annotated//g;s/_orig//g'`
    echo $cm $job    
    echo "${INFERNALDIR}/bin/cmcalibrate --forecast $cm " | qsub -q normal.q -pe para 1 -r y -N $job -o $outdir -e $logdir
done


## any Errors ?
grep -i error $srcpath/parameters/fullcms/forecast/*forecast*

## view forecasted times:
## TODO: use these forecasts somehow??
grep -vh "\#" $srcpath/parameters/fullcms/forecast/*forecast.o*
## sort to get longest time
grep -vh "\#" $srcpath/parameters/fullcms/forecast/*forecast.o* | grep -v "\[ok\]" |sed 's/ \+/ /g' | cut -d ' ' -f 3 |sort 

## NOTE:
## * most between 9 min and 33 min, longer for IC1, Intron_gpI (>1 h),
##   IA1 (>4h), IC3 (>5h)


### CALIBRATE SUBCMS

## NOTE: uses SGE parallelization, call only from cluster gate k70!
##       forecasts above where made for 32 cpus (TODO: why??)
##       -> use --cpu 32 here

outdir=$srcpath/parameters/subcms/calibration
logdir=$outdir
mkdir $outdir
for cm in `grep -L cmcalibrate $srcpath/parameters/subcms/*cm`; do
    echo $cm
    job=`basename $cm| sed 's/\.cm/_calibrate/g;s/_annotated//g;s/_orig//g'`
    echo $cm $job
    echo "${INFERNALDIR}/bin/cmcalibrate --cpu 32 $cm" | qsub -q normal.q -pe para 32 -r y -N $job -o $outdir -e $logdir
done

## check errors
cat $outdir/*.e*
cat $outdir/*.pe*
grep -i error $outdir/*
#IC3_left-P1_r112_calibrate.e486429:Error: File exists, but appears to be empty?
#IC3_P1-L2_r112_calibrate.e486428:Error: File exists, but appears to be empty?

### CALIBRATE FULLCMs (except RFAM; should be calibrated!?)

outdir=$srcpath/parameters/fullcms/calibration # memreq for IA1 and IC3
logdir=$outdir
mkdir $outdir
for cm in `grep -L cmcalibrate $srcpath/parameters/fullcms/*cm`; do
    echo $cm
    job=`basename $cm| sed 's/\.cm/_calibrate/g;s/_annotated//g;s/_orig//g'`
    echo $cm $job
    echo "${INFERNALDIR}/bin/cmcalibrate --cpu 32 $cm" | qsub -q normal.q -pe para 32 -r y -N $job -o $outdir -e $logdir
done
grep -i error $outdir/*

## TODO: grep errors and redo missing!
## add missing: just re-run above!!


## and add calibrated cm to git, using git-lfs!

