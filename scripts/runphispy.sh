# runphispy.sh
#!/bin/sh
##
## runphispy.sh
## 
## Made by Rainer Machne
## Login   <raim@intron.tbi.univie.ac.at>
## 
## Started on  Wed Feb 27 14:44:57 2013 Rainer Machne
## Last update Wed Feb 27 14:44:57 2013 Rainer Machne
##
s2p=$1
d2s=$2
min=$3
gbd=$4
txr=$5

## get line number in taxonomy_ranks.datv of minimal rank (e.g. "order")
min=`grep -n "^\`echo $min\`$" $txr| sed 's/:.*//g'|tr -d ' '`

O=$IFS ## spaces in cut string
IFS=$(echo -en "\n\b")
for spec in `cat $s2p|grep -v "^tax"`; do
    ## get target organism
    dist=`echo $spec |cut -f 1`
    spc=`echo $spec |cut -f 2`
    tax=`echo $spec |cut -f 3`
    ## get first phispy training set organism
    torg=`echo $spec |cut -f 5|tr -d '\n'|sed 's/;.*//g'`
    ## get common rank - take all below class!
    trnk=`echo $spec |cut -f 7|tr -d '\n'|sed 's/;.*//g'`
    echo $spc $tax test organism: $torg ; related as: $trnk #$min
    ## get line number in taxonomy_ranks.dat
    trnk=`grep -n "^\`echo $trnk\`$" $txr| sed 's/:.*//g'|tr -d ' '`
    #echo TRANK IS $trnk
    sdirs=`grep \`echo $tax$\` $d2s|cut -f 1`
    if [ "$sdirs" = "" ]; then
	echo "WARNING: no dirs for $tax $spc"
	continue
    fi
    if [ "$trnk" -lt "$min" ]; then
	echo -e "DISTANCE TOO HIGH\t$tax\tline $trnk\tusing 0"
	torg=0
    fi
    for dir in $sdirs; do 
	echo -e "DIR\t$tax\t$dir"
        tests=`ls  -1 $gbd/$dir/*.gbk|wc -l`
	echo -e "CHECK\t$tax\t$tests gkb files"
	gdirs=`ls  -1 $gbd/$dir/*.gbk`
	for gbk in $gdirs; do
	    seed=`echo $gbk|sed 's/\.gbk$/_seed/g'`
            phage=`echo $seed|sed 's/_seed/_phage/g'`
	    echo $seed $phage $torg
	    if [ -f $phage/prophage.tbl ]; then
	    	echo -e "EXISTS\t$tax\t$gbk\t$torg"
	    else
	        echo -e "CALCULATE\t$tax\t$gbk\t$torg"
	    	#python $PHISPY/phiSpy.py -i $seed -o $phage -t $torg
	    fi
	done
    done
done
IFS=$O

# End of file
