#!/bin/bash

## NOTE - THIS WAS RUN ONCE MANUALLY(!) AND RESULTS ADDED TO GIT
## NOTE 2018: this was copied from the former script `doAll.sh` to
## better separate alignment generation, model calibration and model scans.

### ANALYSIS, CROSS-ANNOTATION & SPLITTING OF GROUP I INTRON ALIGNMENTS
### FROM GISSD and RFAM - HERE TO DOCUMENT THE PROCEDURE, TO BE
### ADAPTED FOR OTHER STRUCTURED RNA CLASSES (large with potential inserts)

### The outlined procedure results in 381 consistently annotated alignments
### of segments (specific named stem-loop combinations) of the original full
### alignments. These can be used to calibrate covariance models using
### infernal cmbuild and cmcalibrate. The segments can then be searched
### in genomes individually and subsequently chained into chains of segments,
### representing partial or full group I intron hits. See `doc/alignments.tex`
### for details.

### The full intermediate data (`processedData.tar.gz`) are available from
### `raim@tbi.univie.ac.at`. The annotated alignments (stockholm format,
### `*.sto` and `*.stk`) and segment ordering files (`*.dat`)
###  are available in the git `parameters/gissd`and `parameters/substk`. 


export GITDIR=~/programs/gIScanner
export SRCDIR=$GITDIR/scripts/ # sources (shell and R scripts)
export PARDIR=$GITDIR/parameters/ # parameters
export WORKDIR=~/work/introns/phylogeny/

rfam=$PARDIR/rfam
gissd=$PARDIR/gissd

cd $WORKDIR

## NOTE: additionally requires symbolic link to folders scripts and parameters 
## of the git in $WORKDIR and the commented out mkdir commands
## in $SRCDIR/download.sh!

## DOWNLOAD ALL DATA
### NOTE: STILL DONE IN doAll.sh
### ALL ORIGINAL DATA SHOULD ALSO BE PRESENT IN `processedData.tar.gz`
#$SRCDIR/download.sh

### RFAM SETUP
## generate RFAM consensus structure
## this cmemit alignment was used to construct paramaters/rf00028_consensus.fa
cmemit -a $rfam/RF00028_v11.cm > $rfam/RF00028_v11_cmemit.stk
## MANUAL STEP !
## grep RF and SS_cons from $rfam/RF00028_v11_cmemit.stk to fasta-style file
## $PARDIR/rf00028_consensus.fa (in CVS) and add lines 
## SS_anno, SS_dtb, SS_pkno, FRAGREP

## get consensus structures from original alignment
$SRCDIR/getConsensus.sh $rfam/ $rfam/RF00028_seed.stockholm.stk
## map annotation to original alignment structure - via RNAforester
$SRCDIR/mapStructureAnnotation.R $PARDIR/rf00028_consensus.fa $rfam/RF00028_seed.stockholm_annotation.fa
## add back pseudoknot
sed -i 's/\./6/g' $rfam/RF00028_seed.stockholm_annotation.fa
## annotate original aligment
$SRCDIR/annotateAlignment.R --anf=$rfam/RF00028_seed.stockholm_annotation.fa --alf=$rfam/RF00028_seed.stockholm.stk --empty="." > $rfam/RF00028_seed.stockholm_annotated.stk
## plot consensus structure
$SRCDIR/plotRNA.R --anf=$rfam/RF00028_seed.stockholm_annotation.fa --seq=SS_anno
## SPLIT ALIGNMENT
## TODO: add left/P1-P2, P7-P8/right
$SRCDIR/splitAlignment.R --alf=$rfam/RF00028_seed.stockholm_annotated.stk --out=substk --fig=eps --ct="L-P1,P1-L2,P1-P2,R1-P2,P2-L3,R2-P3,P3-L4,R3-P4,P4-L5,R4-P5,P5-L6,R5-P6,P6-L7,R6-P7,P7-L8,R7-P8,R7-R6,R6-P8,L6-L8,R2-L4,L3-L4,L3-L6,R4-L7,R5-L7,L6-R3,P7-P8,P8-R"

## search RFAM seed sequences with RFAM CM
cmsearch -A $rfam/RF00028_seed_RF00028_v11.stk --tblout $rfam/RF00028_seed_RF00028_v11.tab $rfam/RF00028_v11.cm $rfam/RF00028_seed.fasta > $rfam/RF00028_seed_RF00028_v11.dat
## convert to tab : probably not required anymore
perl $SRCDIR/stk2tab.pl $rfam/RF00028_seed_RF00028_v11.stk > $rfam/RF00028_seed_RF00028_v11.stk.tab
## align RFAM seed sequences to RFAM CM
cmalign $rfam/RF00028_v11.cm  $rfam/RF00028_seed.fasta > $rfam/RF00028_seed_RF00028_v11_ali.stk 
## annotate
$SRCDIR/annotateAlignment.R --anf=$PARDIR/rf00028_consensus.fa --alf=$rfam/RF00028_seed_RF00028_v11_ali.stk  --empty="~" > $rfam/RF00028_seed_RF00028_v11_ali_annotated.stk 
## convert to tab file : probably not required anymore
perl $SRCDIR/stk2tab.pl $rfam/RF00028_seed_RF00028_v11_ali_annotated.stk > $rfam/RF00028_seed_RF00028_v11_ali_annotated.stk.tab


# translate all frames on each chromosome
transeq -frame 6 -sequence $rfam/RF00028_seed.fasta -outseq $rfam/RF00028_seed.pep.fasta
### PFAM ANALYSIS - RFAM
## search Pfam in giisd
# LAGLIDADG_1: 
hmmsearch --domtblout pfam/RF00028_seed_PF00961.tab pfam/PF00961.hmm $rfam/RF00028_seed.pep.fasta > pfam/RF00028_seed_PF00961.dat 
# LAGLIDADG_2:
hmmsearch --domtblout pfam/RF00028_seed_PF03161.tab pfam/PF03161.hmm $rfam/RF00028_seed.pep.fasta  > pfam/RF00028_seed_PF03161.dat
# GIY-YIG:
hmmsearch --domtblout pfam/RF00028_seed_PF01541.tab pfam/PF01541.hmm $rfam/RF00028_seed.pep.fasta  > pfam/RF00028_seed_PF01541.dat
# HNH:
hmmsearch --domtblout pfam/RF00028_seed_PF01844.tab pfam/PF01844.hmm $rfam/RF00028_seed.pep.fasta  > pfam/RF00028_seed_PF01844.dat
# PI-PfuI Endonuclease subdomain:
hmmsearch --domtblout pfam/RF00028_seed_PF09062.tab pfam/PF09062.hmm $rfam/RF00028_seed.pep.fasta > pfam/RF00028_seed_PF09062.dat
# Hom_end:
hmmsearch --domtblout pfam/RF00028_seed_PF05204.tab pfam/PF05204.hmm $rfam/RF00028_seed.pep.fasta > pfam/RF00028_seed_PF05204.dat

### GISSD SETUP
## collect intron types in GISSD
$SRCDIR/processGissd.sh > $gissd/IDs.dat
cat $gissd/I*fasta > gissd.fasta
## remove protein sequence from Tfl.S1506-2 3'exon 
patch -b $gissd/gissd.fasta $PARDIR/gissd.fasta.patch
## remove duplicate intron sequence from IE3 alignment
patch -b $gissd/IE3.sto $PARDIR/gissd_IE3.sto.patch


## GENERATE GISSD SUBTYPE CMs and ALIGNMENTs from original GISSD alignments
$SRCDIR/cmbuildall.sh $gissd/*.sto
$SRCDIR/cmcalibrateall.sh $gissd/*cm > $gissd/calibration.log
mv cmcalibrate.* $gissd/log
## generate new alignments from all subtypes
$SRCDIR/cmaligngissd.sh

## gissd consensus structures and annotation
$SRCDIR/cmemitall.sh $gissd/*_v11.cm
## map manual annotation from Rfam
$SRCDIR/mapStructureAnnotation.R $PARDIR/rf00028_consensus.fa $gissd/*v11_cmemit.fasta
## MANUAL STEP !
## copy $gissd/*v11_cmemit.fasta to parameters/*consensus.fa  (in CVS) and 
## edit the mapped annotation in line SS_annm, use RNAplots and SS_guid
## as guide

## generate annotated cmalignments
for i in `ls $gissd/I*.stk|grep -v sto.stk|grep -v annotated.stk`; do
    anf=$PARDIR/$(basename ${i%.*})_consensus.fa
    o=${i%.*}_annotated.stk
    echo $anf $i $o
    ## 20131120: is this really not used ??
    #$SRCDIR/annotateAlignment.R --anf=$anf --alf=$i --empty="." > $o
done
for i in `ls $PARDIR/*consensus.fa`; do 
    echo $i
    $SRCDIR/plotRNA.R --anf=$i --seq=SS_anno
done
## MAP ANNOTATION TO ORIGINAL ALIGNMENTS
## collect consensus structures from original files
$SRCDIR/getConsensus.sh $gissd/ $gissd/*sto
## map to manually constructed annotation of cmemit-derived files
for i in `ls $PARDIR/I*consensus.fa`; do
    ## NOTE 20180504: this won't work after split of srcdir and workdir!
    j=`echo $i|sed 's/consensus.fa/annotation.fa/g;s/parameters/gissd/g'`
    echo $i $j
    $SRCDIR/mapStructureAnnotation.R $i $j
done
## add back pseudoknot!!
sed -i 's/\./6/g' $gissd/I*_annotation.fa

## generate annotated cmalignments
for i in `ls $gissd/I*.sto|grep -v annotated`; do
    anf=$gissd/$(basename ${i%.*})_annotation.fa
    o=${i%.*}_orig_annotated.stk
    echo $anf $i $o
    $SRCDIR/annotateAlignment.R --anf=$anf --alf=$i --empty="." > $o
done
for i in `ls $gissd/*annotation.fa`; do 
    echo $i
    $SRCDIR/plotRNA.R --anf=$i --seq=SS_anno
done
## SPLIT ALIGNMENTS
for i in $gissd/*orig_annotated.stk; do
    echo "cutting" $i
    $SRCDIR/splitAlignment.R --alf=$i --out=substk --fig=eps --ct="L-P1,P1-L2,P1-P2,R1-P2,P2-L3,R2-P3,P3-L4,R3-P4,P4-L5,R4-P5,P5-L6,R5-P6,P6-L7,R6-P7,P7-L8,R7-P8,R7-R6,R6-P8,L6-L8,R2-L4,L3-L4,L3-L6,R4-L7,R5-L7,L6-R3,P7-P8,P8-R"
done


## OTHER
## translate all frames on each chromosome
transeq -frame 6 -sequence $gissd/gissd.fasta -outseq $gissd/gissd_pep.fasta

### FRAGREP - GISSD
## TODO: allow 1 big insertion
## only @ leipzig
/usr/local/bin/aln2pattern -m $rfam/RF00028_seed_annotated.aln > $rfam/RF00028_seed_annotated.pattern
/usr/local/bin/fragrep -q $rfam/RF00028_seed_annotated.pattern $gissd/gissd.fasta > $rfam/gissd_RF00028_fragrep.fa

## search GISSD SUBGROUPS VS. GISSD
#cmsearch -A $gissd/gissd_IA1.stk --tblout $gissd/gissd_IA1.tab $gissd/IA1.sto.cm $gissd/gissd.fasta > $gissd/gissd_IA1.dat


### ANALYSIS OF GISSD INTRONS
## SEARCH RFAM CM IN GISSD SEQUENCES
## new infernal
cmsearch -A $rfam/gissd_RF00028_v11.stk --tblout $rfam/gissd_RF00028_v11.tab $rfam/RF00028_v11.cm $gissd/gissd.fasta > $rfam/gissd_RF00028_v11.dat
cmsearch --mid -A $rfam/gissd_RF00028_v11_mid.stk --tblout $rfam/gissd_RF00028_v11_mid.tab $rfam/RF00028_v11.cm $gissd/gissd.fasta > $rfam/gissd_RF00028_v11_mid.dat
cmsearch --max -A $rfam/gissd_RF00028_v11_max.stk --tblout $rfam/gissd_RF00028_v11_max.tab $rfam/RF00028_v11.cm $gissd/gissd.fasta > $rfam/gissd_RF00028_v11_max.dat
## align GISSD subtypes to RFAM CM
$SRCDIR/cmaligngissd2rfam.sh $rfam/RF00028_v11.cm $gissd/I*_seq.fasta $gissd/gissd.fasta
mv I*RF00028_v11.stk gissd_RF00028_v11.stk $rfam/
## convert to tab
perl $SRCDIR/stk2tab.pl $rfam/gissd_RF00028_v11_ali.stk > $rfam/gissd_RF00028_v11_ali.stk.tab

## convert stockholm alignments to tab file
## rfam seed
perl $SRCDIR/stk2tab.pl $rfam/RF00028_seed.stockholm.stk > $rfam/RF00028_seed.stockholm.stk.tab
## gissd
perl $SRCDIR/stk2tab.pl $rfam/gissd_RF00028_v11.stk > $rfam/gissd_RF00028_v11.stk.tab
perl $SRCDIR/stk2tab.pl $rfam/gissd_RF00028_v11_mid.stk > $rfam/gissd_RF00028_v11_mid.stk.tab
perl $SRCDIR/stk2tab.pl $rfam/gissd_RF00028_v11_max.stk > $rfam/gissd_RF00028_v11_max.stk.tab

## ANNOTATE ALIGNMENTS
$SRCDIR/annotateAlignment.R --anf="parameters/rf00028_consensus.fa" --alf=$rfam/gissd_RF00028_v11_ali.stk.tab --empty="~" > $rfam/gissd_RF00028_v11_ali_annotated.stk

### PFAM ANALYSIS - GISSD
## search Pfam in giisd
# 162 frames with LAGLIDADG_1: 
hmmsearch --domtblout pfam/gissd_PF00961.tab pfam/PF00961.hmm $gissd/gissd_pep.fasta > pfam/gissd_PF00961.dat 
# 66 frames with LAGLIDADG_2:
hmmsearch --domtblout pfam/gissd_PF03161.tab pfam/PF03161.hmm $gissd/gissd_pep.fasta  > pfam/gissd_PF03161.dat
# 14 frames with GIY-YIG:
hmmsearch --domtblout pfam/gissd_PF01541.tab pfam/PF01541.hmm $gissd/gissd_pep.fasta  > pfam/gissd_PF01541.dat
# 4 frames with HNH:
hmmsearch --domtblout pfam/gissd_PF01844.tab pfam/PF01844.hmm $gissd/gissd_pep.fasta  > pfam/gissd_PF01844.dat
# 2 frames with PI-PfuI Endonuclease subdomain:
hmmsearch --domtblout pfam/gissd_PF09062.tab pfam/PF09062.hmm $gissd/gissd_pep.fasta > pfam/gissd_PF09062.dat
# 4 frames with Hom_end:
hmmsearch --domtblout pfam/gissd_PF05204.tabxpfam/PF05204.hmm $gissd/gissd_pep.fasta > pfam/gissd_PF05204.dat


## plot RFAM CM -> GISSD alignments and PFAM HITS
R --args < $SRCDIR/analyzeGissd.R
## TODO 201805: why is $SRCDIR/analyzeRfam.R not used?
