#!/bin/bash

## collect all nt fasta files into one file
touch genomes/bacteria/allbacteria.fasta
for i in `find genomes/bacteria/ -name "*.fna"`
do
echo $i
cat $i >> genomes/bacteria/allbacteria.fasta
done
echo "GENERATING BLAST DB"
formatdb -i  genomes/bacteria/allbacteria.fasta -p F -o T
echo "INDEXING FOR INFERNAL"
/home/mescalin/choener/tmp/infernal/infernal-1.1rc1/easel/miniapps/esl-sfetch --index genomes/bacteria/allbacteria.fasta