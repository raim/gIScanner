
srcpath=~/programs/gIScanner
datpath=~/data/introns/prokaryotes_2018/mitochondria

## download from NCBI
wget ftp://ftp.ncbi.nlm.nih.gov/refseq/release/mitochondrion/mitochondrion.2.1.genomic.fna.gz
wget ftp://ftp.ncbi.nlm.nih.gov/refseq/release/mitochondrion/mitochondrion.2.genomic.gbff.gz
mv mitochondrion.2.1.genomic.fna.gz $datpath
mv mitochondrion.2.1.genomic.gbff.gz $datpath
gunzip $datpath/mitochondrion.2.1.genomic.fna.gz
gunzip $datpath/mitochondrion.2.genomic.gbff.gz
