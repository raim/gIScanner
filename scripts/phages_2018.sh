
### SETUP GENOMES

## NOTES: done mostly at HHU HPC (Hilbert)
## model calibration and fasta circularization and
## indexing was actually done at Lpzg and rsync'ed to HHU HPC;
## full CM model scans were also done at Lpzg;
## all results collected at working machine HHU exon

## TODOS:
## * remove circular hits for linear genomes from final results
## * untangle phage and prokaryote runtime stats in cputimes_2018.sh

## source and data paths
genbro=~/programs/genomeBrowser
srcpath=~/programs/gIScanner
datpath=~/data/introns/prokaryotes_2018
#sampath=/usr/local/bin # samtools @ exon
sampath=/usr/bin # `dirname $(which samtools)` #/samtools @ intron
eslpath=~/bin # easel miniapps - indexing fasta for infernal
infpath=/usr/local/infernal # infernal executables
export INFERNALDIR=$infpath

dattype="phages" # TODO: use everywhere?

## circularize all genomes
$srcpath/scripts/circularizeSeq.R --seq $datpath/Renamed_allPhageGenomesDB.fasta --ext 5000 > $datpath/phages.fa
## index fasta file with easel miniapps
$eslpath/esl-sfetch --index $datpath/phages.fa

## generate genomeBrowser index
#$genbro/src/fastaindex.R $datpath/phages.fa > $datpath/phages.idx
$sampath/samtools faidx $datpath/phages.fa
cut -f1-2 $datpath/phages.fa.fai > $datpath/phages.idx

### SCAN PHAGES at HHU HPC


## input/output paths/files
datpath=/gpfs/project/machne/data/introns
seq=$datpath/phages.fa
infile=$datpath/allcms.txt
resdir=$datpath/results/subcms

## list of covariance model files
grep -l cmcalibrate $datpath/subcms/*cm > $infile

## cmsearch settings
Z=1
ncpus=20

## PBS SETTINGS
mem=5gb
walltime=02:00:00

### SUB-MODELS

## scan #1
logdir=$resdir/log
mkdir -p $logdir

\cp $srcpath/scripts/bacteria_2018_hhu.sh phgscript.sh
sed -i "s/ZETT/$Z/g;s/NCPU/$ncpus/g;s/GENOME/phages/g" phgscript.sh

qsub -J 1-377 -l select=1:ncpus=${ncpus}:mem=${mem} -l walltime=$walltime -N scan_gImodels -A Coilseq -r y -e $logdir -o $logdir phgscript.sh

## scan #2
## redo failed (.out with file size 0) at higher memory!
ls -s results/subcms/*.out | sort -nr |grep "^    0 " | sed 's/.*phages_//g;s/.out//g' | grep -f - allcms.txt  > missingcms.txt

mem=10gb
ncpus=10
logdir=$resdir/log2
mkdir -p $logdir
\cp $srcpath/scripts/bacteria_2018_hhu.sh phgscript_missing.sh
sed -i "s/ZETT/$Z/g;s/NCPU/$ncpus/g;s/GENOME/phages/g;s/allcms/missingcms/g" phgscript_missing.sh
num=`wc -l phgscript_missing.sh|sed 's/ phgsc.*//'`
qsub -J 1-${num} -l select=1:ncpus=${ncpus}:mem=${mem} -l walltime=$walltime -N scan_gImodels -A Coilseq -r y -e $logdir -o $logdir phgscript_missing.sh

## scan #3
## redo failed (.out with file size 0) at higher memory!
## this time real 0-size files
find results/subcms/  -maxdepth 1 -type f -name "phages*.out" -exec wc -c {} + | sort -nr |grep "^ \+0 " | sed 's/.*phages_//g;s/.out//g' | grep -f - allcms.txt  > missingcms2.txt

mem=20gb
ncpus=20
walltime=04:00:00
logdir=$resdir/log3
mkdir -p $logdir
\cp $srcpath/scripts/bacteria_2018_hhu.sh phgscript_missing.sh
sed -i "s/ZETT/$Z/g;s/NCPU/$ncpus/g;s/GENOME/phages/g;s/allcms/missingcms2/g" phgscript_missing.sh
num=`wc -l missingcms2.txt|sed 's/ missing.*//'`
qsub -J 1-${num} -l select=1:ncpus=${ncpus}:mem=${mem} -l walltime=$walltime -N scan_gImodels -A Coilseq -r y -e $logdir -o $logdir phgscript_missing.sh

## scans #4-7 [NOTE: changes index N of missingcmsN.txt interactively/repeatedly
## redo failed (.out with file size 0) at higher memory!
## this time real 0-size files
find results/subcms/  -maxdepth 1 -type f -name "*.out" -exec wc -c {} + | sort -nr |grep "^ \+0 " | sed 's/.*phages_//g;s/.out//g' | grep -f - allcms.txt  > missingcms5.txt

mem=50gb
ncpus=10
walltime=72:00:00
logdir=$resdir/log5
mkdir -p $logdir
\cp $srcpath/scripts/bacteria_2018_hhu.sh phgscript_missing.sh
sed -i "s/ZETT/$Z/g;s/NCPU/$ncpus/g;s/GENOME/phages/g;s/allcms/missingcms5/g" phgscript_missing.sh
num=`wc -l missingcms5.txt|sed 's/ missing.*//'`
qsub -J 1-${num} -l select=1:ncpus=${ncpus}:mem=${mem} -l walltime=$walltime -N scan_gImodels -A Coilseq -r y -e $logdir -o $logdir phgscript_missing.sh

## scan #8
## two IA1 models where lost by git lfs errors
logdir=$resdir/log6
mkdir -p $logdir
echo "$datpath/subcms/IA1_orig_annotated_left-P1_r112.cm" > lastcms.txt
echo "$datpath/subcms/IA1_orig_annotated_P1-L2_r112.cm" >> lastcms.txt
\cp $srcpath/scripts/bacteria_2018_hhu.sh phgscript_last.sh
sed -i "s/ZETT/$Z/g;s/NCPU/$ncpus/g;s/GENOME/phages/g;s/allcms/lastcms/g" phgscript_last.sh
qsub -J 1-2 -l select=1:ncpus=${ncpus}:mem=${mem} -l walltime=$walltime -N scan_gImodels -A Coilseq -r y -e $logdir -o $logdir phgscript_last.sh


### fullCM search
## at LPZG, SGE CLUSTER

seq=$datpath/phages.fa
resdir=$datpath/results/fullcms
logdir=$resdir/log
mkdir -p $logdir
Z=1

for cm in `grep -l cmcalibrate $srcpath/parameters/fullcms/*cm`; do
    cmf=$(basename ${cm%.*})
    res=${resdir}/phages_${cmf}
    echo $cm $res
    if [ -s $res.tab ]; then
	echo "exists"
    else
	echo CALCULATE
	echo "${INFERNALDIR}/bin/cmsearch --cpu 32 -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq" | qsub -q normal.q -pe para 32 -r y -N $cmf -o $logdir -e $logdir
    fi
done

### COLLECT RESULTS and RUN STATISTICS
## currently also using bacteria_2018.sh results in script:
$srcpath/scripts/cputimes_2018.sh

## PFAM SEARCH HITS BY OVIDIU
## split chromosome and gene name; add new header line
datpath=~/data/introns/prokaryotes_2018
allpath=$datpath/phages
sed 's/&\_/\&\tgene_/g;1i chr\tgene_name\tpfam_name\tpfam_id\tstart\tend\tstrand\tpfam_description' $allpath/results/pfam/pfam_prediction.txt  | grep -v "^#" | cut -f 1,2,3,4,5,6,7 > $allpath/results/pfam/pfam_prediction.tsv


### CHAIN RESULTS
## at HHU exon
## NOTE: .tab results merged for phages and allcontigs
## ln -s /data/introns/prokaryotes_2018/hhu/results/subcms/phages*.tab /data/introns/prokaryotes_2018/phages/results/subcms/
## ln -s /data/introns/prokaryotes_2018/lpzg/results/fullcms/phages*.tab /data/introns/prokaryotes_2018/phages/results/fullcms/

srcpath=~/programs/gIScanner
datpath=~/data/introns/prokaryotes_2018/phages
respath=$datpath/chainer_1500_test
mkdir -p $respath

## TODO: add more data; use HMM data in chaining!
## --gff=genomes/bacteria/gff --phf=genomes/bacteria/prophages.tbl -cfd=fullcms
$srcpath/scripts/chainCMResults.R --dir $datpath/results  --cmd=subcms --cfd=fullcms --fend=_r112 --gdf=$srcpath/parameters/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --grf=$srcpath/parameters/gI_subgroups.dat --out=$respath --fig=$respath/figures  --chs=$respath/chains --inc --exp --store --maxr=0 --maxd=1500 --minl=3 --ovlf --plot --pff=$datpath/results/pfam/pfam_prediction.tsv #--dof

## RESULT NOTES:
## TITLE: Dissecting the group I structure in bacterial and phage genomes:
## symmetric structure and a HEG invasion mechanisms.
## TITLE: Prokaryotic niches of group I self-splicing introns
## FLAWS
## too long: Bacillus_phage_BCP8.2._gI1_cmchain_317.png
## two in 1: Bacillus_phage_Grass._gI2_cmchain_203.png
## two in 1: Bacillus_phage_Moonbeam._gI3_cmchain_171.png
## INSIGHTS?
## Examples
## * Enterobacteria phage T4: experimentally confirmed, all three found,
##   HEG with symmetric hits
## * Bacillus phage AR9: multiple arrays, +/- HEG, etc
## gI symmetry around pseudoknot???
## * overlapping convergent gI: Escherichia_phage_Av.05._gI5_chmchain_5.png
##   appears to be an inverted repeat around ca. 120936
## * full model hits appear to be based on two full alignments, even
##   though sub-model hits are separate, eg. central of AR9 array
## RESULT TODOS:
## gI vs pfam predictions, recovery and overlap analysis with segmenTools 

## ALL FULL MODEL HITS
srcpath=~/programs/gIScanner
datpath=~/data/introns/prokaryotes_2018/phages
respath=$datpath/chainer_dof
mkdir -p $respath

## TODO: add more data; use HMM data in chaining!
## --gff=genomes/bacteria/gff --phf=genomes/bacteria/prophages.tbl -cfd=fullcms
$srcpath/scripts/chainCMResults.R --dir $datpath/results  --cmd=subcms --cfd=fullcms --fend=_r112 --gdf=$srcpath/parameters/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --grf=$srcpath/parameters/gI_subgroups.dat --out=$respath --fig=$respath/figures  --chs=$respath/chains --inc --exp --store --maxr=0 --maxd=2500 --minl=3 --ovlf --plot --dof

## VARY?
srcpath=~/programs/gIScanner
datpath=~/data/introns/prokaryotes_2018/phages
respath=$datpath/chainer3
mkdir -p $respath

## TODO: add more data; use HMM data in chaining!
## --gff=genomes/bacteria/gff --phf=genomes/bacteria/prophages.tbl -cfd=fullcms
$srcpath/scripts/chainCMResults.R --dir $datpath/results  --cmd=subcms  --fend=_r112 --gdf=$srcpath/parameters/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --grf=$srcpath/parameters/gI_subgroups.dat --out=$respath --fig=$respath/figures  --chs=$respath/chains --store --maxr=0 --maxd=2500 --minl=3 --ovlf --plot --dof

## RESULT NOTES:
## several sub-models hit in Bacillus phage AR9 gyrase A gene, neither
## gII nor gI but spliced in cDNA


## no inc filter but increased minl
srcpath=~/programs/gIScanner
datpath=~/data/introns/prokaryotes_2018/phages
respath=$datpath/chainer3
mkdir -p $respath

$srcpath/scripts/chainCMResults.R --dir $datpath/results  --cmd=subcms  --fend=_r112 --gdf=$srcpath/parameters/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --grf=$srcpath/parameters/gI_subgroups.dat --out=$respath --fig=$respath/figures  --chs=$respath/chains --store --maxr=0 --maxd=2500 --minl=5 --ovlf --plot 


### ANALYSIS
## TODO: gI subcms vs fullcms : segmentRecovery
## TODO: gI vs pfam : segmentOverlaps, segmentAnnotate

segpath=~/programs/segmenTools
datpath=~/data/introns/prokaryotes_2018
allpath=$datpath/phages
chain=$allpath/chainer_1500

#rsync -avz raim@doof.bioinf.uni-leipzig.de:/scr/doofsan/raim/data/introns/phages.fa /home/raim/data/introns/prokaryotes_2018/phages/
chrfile=$datpath/phages.idx

## de-circularize!
## delete features that stem from chromosome circularization and
## should be present twice, and resolve those spanning chromosome start/end

## rewrite results file for use with segmenTools
echo -e "ID\tchr\tstart\tend\tstrand" > $allpath/gI_hits_circ.tab 
cut -f 1,2,7,8,9 $chain/results.tab | grep -v "^#" | grep -v "^cmchain ID" >> $allpath/gI_hits_circ.tab 

## ALL HITS, with hits spanning chromosome ends indicated by start > end
$segpath/scripts/cleanCircular.R -i $allpath/gI_hits_circ.tab -e 5000 --chrfile $datpath/phages.idx  > $allpath/gI_hits.tab 
$segpath/scripts/cleanCircular.R -i $allpath/gI_hits_circ.tab -e 5000 --chrfile $datpath/phages.idx  --split > $allpath/gI_hits_decirc.tab 

## working hits
## expand hits by +/- 200 for overlap with PFAM annotation
awk 'BEGIN {FS="\t";OFS = "\t"}; {if ($1!="ID") {$3=$3 -200; $4=$4 +200}; print}' $allpath/gI_hits_circ.tab > $allpath/gI_200_circ.tab
## working hits, with hits spanning chromosome ends split
$segpath/scripts/cleanCircular.R -i $allpath/gI_200_circ.tab -e 5000 --chrfile $datpath/phages.idx --split > $allpath/gI_200_decirc.tab 
## expand hits by +/- 500 for overlap with PFAM annotation
awk 'BEGIN {FS="\t";OFS = "\t"}; {if ($1!="ID") {$3=$3 -500; $4=$4 +500}; print}' $allpath/gI_hits_circ.tab > $allpath/gI_500_circ.tab
## working hits, with hits spanning chromosome ends split
$segpath/scripts/cleanCircular.R -i $allpath/gI_500_circ.tab -e 5000 --chrfile $datpath/phages.idx --split > $allpath/gI_500_decirc.tab 

## COMPARE TO PFAM!
## TODO: add columns to pfam predictions, use pfam.id as type column
## TODO: use segmentAnnotate, to find all
## TODO: use segmentOverlap vs. pfam categories to find enrichments
## TODO: segmentRecovery to compare full with chained hits

$segpath/scripts/segmentAnnotate.R -t $allpath/gI_500_decirc.tab -q $allpath/results/pfam/pfam_prediction.tsv --qcol pfam_id,pfam_name --details --chrfile $datpath/phages.idx --chrmap  --each.target --each.query > $allpath/${dattype}_gI2pfam_full.tab
cut -f 1,7,8 $allpath/gI_vs_pfam_full.tab  >  $allpath/${dattype}_gI2pfam.tab
## TODO add prefix and coordinates also in allcontigs, and account for
## new colnames in analysis

#$segpath/scripts/segmentRecovery.R  -i $allpath/gI_500_decirc.tab --out.path $allpath/recovery --chrfile $datpath/phages.idx --ovlth 0.8 --minj 0.8 --minf 0.2 --target  $allpath/results/pfam/pfam_prediction.tsv --ttypcol pfam_name  --fig.type png

### FISHER's EXACT TEST : RNA and PFAM OVERLAPS
## PFAM - prophages
sed 's/dattype=.*/dattype="phages"/;s/do.rna=.*/do.rna=FALSE/;s/gifff=.*/gifff=""/' $srcpath/scripts/chain2pfam.R | R --vanilla
## RNA - prophages
#sed 's/dattype=.*/dattype="phages"/;s/do.rna=.*/do.rna=TRUE/;s/gifff=.*/gifff=""/' $srcpath/scripts/chain2pfam.R | R --vanilla



## CUT CANDIDATE SEQUENCES FOR FURTHER ANALYSIS
srcpath=~/programs/gIScanner
datpath=~/data/introns/prokaryotes_2018
allpath=$datpath/phages
$srcpath/scripts/fetchSeq.R --hit $allpath/gI_hits.tab --fasta $datpath/Renamed_allPhageGenomesDB.fasta --ext 1250 --type simple --sid desc  --circular --hcircular > $allpath/${dattype}_gI_1250.fasta
