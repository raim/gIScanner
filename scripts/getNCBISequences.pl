#!/usr/bin/perl -w

## read a stockholm format alignement, retrieve NCBI sequence IDs and download, 
## tag which coordinates were used in the alignment

## to use:
# grep "#=GS" rfam/RF00028_full.stockholm.stk > rfam/rfam_ids.txt
# perl scripts/getNCBISequences.pl rfam/rfam_ids.txt 

use strict;
use Bio::Perl;

my $outpath = "/scr/hotchocolate/raim/intronSequences";
my $database="genbank";
my $format = "fasta";


my $ftype;
if ( $format eq "fasta" )
{
    $ftype = ".fa";
}else{
    $ftype = ".gb";
}
my @accessions;
my %starts;
my %ends;
while (<>){
    if ( /#=GS\s.*\s+AC\s+(.*)\/(\d+)-(\d+)/ )
    {
	#print $1."\n";
	$accessions[@accessions] = $1;
	$starts{$1} = $2;
	$ends{$1} = $3;
    }
}

open(LEN, ">rfam/rfam_lengths.txt") or die "can't write species file\n"; 
foreach my $id ( keys %starts )
{
    print $id."\t".($ends{$id}-$starts{$id})."\n";
}
close(LEN)

print STDERR "RETRIEVED ".@accessions." sequence IDs; starting download\n";

open(SPEC, ">rfam/rfam_species.txt") or die "can't write species file\n"; 

for ( my $i=0; $i<@accessions; $i++) {
   
    my $id=$accessions[$i];

    my $file = $outpath."/".$id.$ftype; 
    #

next if (-e $file);

    my $seq = get_sequence($database, $id);

    print SPEC $id."\t".$seq->species->genus." ".$seq->species->species."\t".$seq->species->ncbi_taxid."\n";

    ## edit header
    my $header = $seq->desc."; intron ".$starts{$id}."-".$ends{$id}." of ".$seq->length;
    $seq->desc($header);
     
    
    write_sequence(">".$file, $format, $seq);
  
}
close(SPEC);
print STDERR "WROTE all fasta files\n";
