#!/bin/bash
## cmaligngissd.sh
## 
## Made by Rainer Machne
## Login   <raim@intron.tbi.univie.ac.at>
## 
## Started on  Wed Oct 31 11:49:39 2012 Rainer Machne
## Last update Wed Oct 31 11:49:39 2012 Rainer Machne
##

ompi_cmalign="/scr/trieb/raim/programs/tataProject/introns/scripts/ompi_cmalign"

for i in `find gissd/ -name "I*_seq.fasta"`
do
    cm=`echo $i | sed 's/_seq.fasta/_v11.cm/'`
    o=${cm%.*}".stk"
    echo $ompi_cmalign -o $o $cm $i
    qsub $ompi_cmalign -o $o $cm $i
done



# End of file
