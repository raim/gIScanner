#!/bin/bash

export WORKDIR=~/work/introns/phylogeny/
cd $WORKDIR

# mkdir gissd
# mkdir rfam
# mkdir pfam
# mkdir genomes
# mkdir genomes/bacteria
# mkdir genomes/phages
# mkdir taxonomy
# mkdir lifestyle

cd $WORKDIR/gissd
# download intron sequences
wget http://www.rna.whu.edu.cn/gissd/download/IA1_seq.fasta.gz 
wget http://www.rna.whu.edu.cn/gissd/download/IA2_seq.fasta.gz 
wget http://www.rna.whu.edu.cn/gissd/download/IA3_seq.fasta.gz 
wget http://www.rna.whu.edu.cn/gissd/download/IB1_seq.fasta.gz 
wget http://www.rna.whu.edu.cn/gissd/download/IB2_seq.fasta.gz 
wget http://www.rna.whu.edu.cn/gissd/download/IB3_seq.fasta.gz 
wget http://www.rna.whu.edu.cn/gissd/download/IB4_seq.fasta.gz 
wget http://www.rna.whu.edu.cn/gissd/download/IC1_seq.fasta.gz 
wget http://www.rna.whu.edu.cn/gissd/download/IC2_seq.fasta.gz 
wget http://www.rna.whu.edu.cn/gissd/download/IC3_seq.fasta.gz 
wget http://www.rna.whu.edu.cn/gissd/download/ID_seq.fasta.gz  
wget http://www.rna.whu.edu.cn/gissd/download/IE1_seq.fasta.gz 
wget http://www.rna.whu.edu.cn/gissd/download/IE2_seq.fasta.gz 
wget http://www.rna.whu.edu.cn/gissd/download/IE3_seq.fasta.gz 
wget http://www.rna.whu.edu.cn/gissd/download/IE_seq.fasta.gz  

# download intron alignments
wget http://www.rna.whu.edu.cn/data/alignment/IA1.sto.gz
wget http://www.rna.whu.edu.cn/data/alignment/IA2.sto.gz
wget http://www.rna.whu.edu.cn/data/alignment/IA3.sto.gz
wget http://www.rna.whu.edu.cn/data/alignment/IB1.sto.gz
wget http://www.rna.whu.edu.cn/data/alignment/IB2.sto.gz
wget http://www.rna.whu.edu.cn/data/alignment/IB3.sto.gz
wget http://www.rna.whu.edu.cn/data/alignment/IB4.sto.gz
wget http://www.rna.whu.edu.cn/data/alignment/IC1.sto.gz
wget http://www.rna.whu.edu.cn/data/alignment/IC2.sto.gz
wget http://www.rna.whu.edu.cn/data/alignment/IC3.sto.gz
wget http://www.rna.whu.edu.cn/data/alignment/ID.sto.gz
wget http://www.rna.whu.edu.cn/data/alignment/IE.sto.gz
wget http://www.rna.whu.edu.cn/data/alignment/IE1.sto.gz
wget http://www.rna.whu.edu.cn/data/alignment/IE2.sto.gz
wget http://www.rna.whu.edu.cn/data/alignment/IE3.sto.gz
wget http://www.rna.whu.edu.cn/data/alignment/IA1.sali.gz
wget http://www.rna.whu.edu.cn/data/alignment/IA2.sali.gz
wget http://www.rna.whu.edu.cn/data/alignment/IA3.sali.gz
wget http://www.rna.whu.edu.cn/data/alignment/IB1.sali.gz
wget http://www.rna.whu.edu.cn/data/alignment/IB2.sali.gz
wget http://www.rna.whu.edu.cn/data/alignment/IB3.sali.gz
wget http://www.rna.whu.edu.cn/data/alignment/IB4.sali.gz
wget http://www.rna.whu.edu.cn/data/alignment/IC1.sali.gz
wget http://www.rna.whu.edu.cn/data/alignment/IC2.sali.gz
wget http://www.rna.whu.edu.cn/data/alignment/IC3.sali.gz
wget http://www.rna.whu.edu.cn/data/alignment/ID.sali.gz
wget http://www.rna.whu.edu.cn/data/alignment/IE.sali.gz
wget http://www.rna.whu.edu.cn/data/alignment/IE1.sali.gz
wget http://www.rna.whu.edu.cn/data/alignment/IE2.sali.gz
wget http://www.rna.whu.edu.cn/data/alignment/IE3.sali.gz
gunzip *gz
cd $WORKDIR/

## download prokaryotic genome information from NCBI
### GENOMES
## BACTERIA GENOMES
cd $WORKDIR/genomes/bacteria
## species summaries: dl 20130206!!
wget ftp://ftp.ncbi.nlm.nih.gov/bioproject/summary.txt
# fasta: dl 20120918
wget ftp://ftp.ncbi.nlm.nih.gov/genomes/Bacteria/all.fna.tar.gz
mkdir fasta
cd fasta; tar zxvf ../all.fna.tar.gz; cd ..
# genbank annotation: dl 20130206!!
wget ftp://ftp.ncbi.nlm.nih.gov/genomes/Bacteria/all.gbk.tar.gz
cd gbk; tar zxvf ../all.gbk.tar.gz;
## mv directories to 20120918 names (on 20130227)
mv Listeria_monocytogenes_SLCC2755_uid52455 Listeria_monocytogenes_serotype_1_2b_SLCC2755_uid52455
mv Listeria_monocytogenes_SLCC2372_uid174872 Listeria_monocytogenes_serotype_1_2c_SLCC2372_uid174872
mv Photorhabdus_asymbiotica_uid59243 Photorhabdus_asymbiotica_ATCC_43949_uid59243
mv Staphylococcus_aureus_ST398_uid159247 Staphylococcus_aureus_S0385_uid159247
mv Pyrobaculum_neutrophilum_V24Sta_uid58421 Thermoproteus_neutrophilus_V24Sta_uid58421
cd ..
## gff annotation: dl 20120918
wget ftp://ftp.ncbi.nlm.nih.gov/genomes/Bacteria/all.gff.tar.gz
# id list
wget ftp://ftp.ncbi.nlm.nih.gov/genomes/IDS/Bacteria.ids
cd gff; tar zxvf ../all.gff.tar.gz; cd ..
cd $WORKDIR/genomes/bacteria
## mv wrongly named directories
mv fasta/Staphylococcus_aureus_ST385_uid159247 fasta/Staphylococcus_aureus_ST398_uid159247
mv gff/Staphylococcus_aureus_ST385_uid159247 gff/Staphylococcus_aureus_ST398_uid159247
mv gbk/Staphylococcus_aureus_ST385_uid159247 gbk/Staphylococcus_aureus_ST398_uid159247
cd $WORKDIR/


## TAXONOMY
cd $WORKDIR/taxonomy
wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/gi_taxid_nucl.dmp.gz
gunzip gi_taxid_nucl.dmp.gz
## add outdated GI to gi_taxid_nucl.dmp
for line in `cat $WORKDIR/parameters/oldgi.dat`; do old=`echo $line | cut -f 1 -d';'`; new=`echo $line | cut -f 2 -d';'`; tax=`grep $new gi_taxid_nucl.dmp|cut -f 2`; echo -e "$old\t$tax"; done >> gi_taxid_nucl.dmp

wget ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz
tar zxvf taxdump.tar.gz
wget ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxcat.tar.gz
tar zxvf taxcat.tar.gz
## in Newick format via iTOL
wget http://itol.embl.de/ncbi_tree/ncbi_complete_with_taxIDs.newick.gz
## 16S rRNA complete tree from SILVA
## http://www.sciencedirect.com/science/article/pii/S0723202011000749
wget http://www.arb-silva.de/fileadmin/silva_databases/living_tree/LTP_release_111/LTPs111_SSU.tree.newick
## species mapping, contains AccNum
wget http://www.arb-silva.de/fileadmin/silva_databases/living_tree/LTP_release_111/LTPs111_SSU.csv
## GI to AccNum mapping
wget ftp://ftp.ncbi.nih.gov/genbank/livelists/GbAccList.0127.2013.gz
cd $WORKDIR

## get organism life style info
cd lifestyle
## marine organisms
wget http://www.megx.net/genomes/genomes.csv
## Freilich et al. 2010 NAR
## http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2896517/
## old lifestyle info
wget http://nar.oxfordjournals.org/content/suppl/2010/03/02/gkq118.DC1/nar-02381-s-2009-File007.txt
## cross-relations
wget http://nar.oxfordjournals.org/content/suppl/2010/03/02/gkq118.DC1/nar-02381-s-2009-File008.txt
## clustering
wget http://nar.oxfordjournals.org/content/suppl/2010/03/02/gkq118.DC1/nar-02381-s-2009-File009.txt
## environment
wget http://nar.oxfordjournals.org/content/suppl/2010/03/02/gkq118.DC1/nar-02381-s-2009-File010.txt
## EOM
wget http://nar.oxfordjournals.org/content/suppl/2010/03/02/gkq118.DC1/nar-02381-s-2009-File014.txt 
## PATRIC DB, 20130131
wget  http://www.tbi.univie.ac.at/~raim/tataProject/originalData/patric_metadata.txt
cd $WORKDIR


## PHAGE GENOMES
cd $WORKDIR/genomes/bacteria/phages
wget http://www.ebi.ac.uk/genomes/phage.txt
wget http://www.ebi.ac.uk/genomes/phage.details.txt
cd $WORKDIR


## RFAM CM and SEED ALIGNMENTS
cd $WORKDIR/rfam
wget http://rfam.sanger.ac.uk/family/RF00028/cm/1.0
wget http://rfam.sanger.ac.uk/family/RF00028/cm/1.1
wget http://www.tbi.univie.ac.at/~raim/tataProject/originalData/RF00028_seed.fasta
wget http://www.tbi.univie.ac.at/~raim/tataProject/originalData/RF00028_seed.stockholm.stk
wget http://www.tbi.univie.ac.at/~raim/tataProject/originalData/RF00028_full.stockholm.stk
cd $WORKDIR

## PFAM ENDONUCLEASES
cd $WORKDIR/pfam
wget http://pfam.sanger.ac.uk/family/PF05204/hmm
mv hmm PF05204.hmm
wget http://pfam.sanger.ac.uk/family/PF03161/hmm
mv hmm PF03161.hmm
wget http://pfam.sanger.ac.uk/family/PF00961/hmm
mv hmm PF00961.hmm
wget http://pfam.sanger.ac.uk/family/PF09062/hmm
mv hmm PF09062.hmm
wget http://pfam.sanger.ac.uk/family/PF01541/hmm
mv hmm PF01541.hmm
wget http://pfam.sanger.ac.uk/family/PF01844/hmm
mv hmm PF01844.hmm
cd $WORKDIR


## REBECCA'S GENOMES
cd genomes/rebecca
wget http://www.tbi.univie.ac.at/~raim/tataProject/originalData/gII_20130204_intronsonly.fasta
wget http://www.tbi.univie.ac.at/~raim/tataProject/originalData/gII_20130204_withexons.fasta
cd -
