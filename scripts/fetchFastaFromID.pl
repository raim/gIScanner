#!/usr/bin/perl -w

## retrieve NCBI sequence and annotation files, and species taxon ID 
## from GenBank or other IDs

## TODO: set via option which ID to use!!

use strict;
use Getopt::Long;
use File::Path qw(make_path);
#use File::Spec qw(catpath);
#use File::Find::Rule;
#use Bio::FeatureIO::gff;
use Bio::Perl;
use Bio::SeqIO;
use Bio::Tools::GFF;
#use Bio::SeqFeatureI;

my $outpath = "./";
my $database="genbank";
## to store taxonomy info and check whether file has already been downloaded
my $taxonomy="taxonomy.dat";
my $noSeq=0; ## set to 0 to ONLY write taxonomy file 

usage() unless GetOptions("o=s"  => \$outpath,
			  "db=s" => \$database,
			  "tx=s" => \$taxonomy,
			  "noseq"=> \$noSeq,
			  "h"    => \&usage);

sub usage
{    
    printf STDERR "\n";
    printf STDERR "Reads RefSeq IDs from commandline arguments, \n";
    printf STDERR "retrieves fasta and annotation files, generates \n";
    printf STDERR "a directory for each identified species where files \n";
    printf STDERR "are written to. An index file is read in, if existing, \n";
    printf STDERR "and only IDs NOT listed are downloaded and added to  \n";
    printf STDERR "this file (gi refseq species taxonid circular)!\n\n";
    printf STDERR "\tUsage: @{[basename($0)]} [options] <GenBankIDs>\n";
    printf STDERR "\n";
    
    printf STDERR "\t-o=s\toutpath, current: $outpath\n";
    printf STDERR "\t-db=s\tdatabase to which IDs refer, current: $database\n";
    printf STDERR "\t-tx=s\ttaxonomy file, current: $taxonomy\n";
    printf STDERR "\t-seq\tonly write taxonomy file, current: $noSeq\n";
    printf STDERR "\n";
    exit;
}


## rest of arguments are species IDs
my @accessions = @ARGV;

my $dbshort = "ref";
$dbshort = "gb" if $database eq "genbank";

#open(SPEC, ">rfam/rfam_species.txt") or die "can't write species file\n"; 

## generate fasta and gff directories
my $fastaDir = $outpath."/fasta"; 
my $gffDir = $outpath."/gff"; 
my $gbkDir = $outpath."/gbk"; 

make_path($fastaDir, $gffDir) if $noSeq == 0;

## open taxonomy file to check already downloaded sequences
my %downloaded;
if ( open(TAX, "<".$outpath."/".$taxonomy) ) {
    while(<TAX>){
	my @vals = split '\t';
	chomp(@vals);
	$downloaded{$vals[1]} = "yes";
	
    }
    close(TAX);
} else {
    print STDERR "no taxonomy file; creating ".$outpath."/".$taxonomy."\n";
}

## open taxonomy file to add new downloads
open(TAX, ">>".$outpath."/".$taxonomy) or die "can't open taxonomy file\n";
    
print TAX "gi\tacc\tseq\tspecies\ttaxon\tcirc\n";

for ( my $i=0; $i<@accessions; $i++) 
{
    
    my $id=$accessions[$i];
    #print STDERR "$id:\t";
    ## check whether files were already downloaded
    if ( defined $downloaded{$id} ) {
	print STDERR "$id:\talready downloaded\n";
	next;
    }
    print STDERR "$id:\tdownloading ... ";

    my $seq = get_sequence($database, $id);


    ## generate directory for species
    my $spec = $seq->species;
    my $specId = $spec->genus."_".$spec->species;
    $specId = $specId."_".$spec->sub_species
	if ( defined $spec->sub_species && $spec->sub_species ne '' );
    

    $specId =~ s/\s+/_/g;
    #print "$id IS $specId \n";

    ## generate directories
    make_path($fastaDir."/".$specId, $gffDir."/".$specId, $gbkDir."/".$specId) if $noSeq == 0;

    #next;
    #my $specdir =  $outpath."/".
    #unless ( -e $fi

    ## write genbank file  
    my $gbkFile = $gbkDir."/".$specId."/".$id.".gbk"; 
    write_sequence(">".$gbkFile, "genbank", $seq) if $noSeq == 0;

    ## write gff3 file
    if ( $noSeq == 0 ) {
	my $gffFile = $gffDir."/".$specId."/".$id.".gff"; 
	my $gffout = new Bio::Tools::GFF(-file => ">$gffFile" ,
					 -gff_version => 3);
    
	foreach my $feature ( $seq->get_SeqFeatures() ) {
	    $gffout->write_feature($feature) ;
	}
    }

    ## modify fasta header to reflect NCBI headerse
    $seq->display_id("gi|".$seq->primary_id."|".$dbshort."|".$id."|");
    my $fastaFile = $fastaDir."/".$specId."/".$id.".fasta"; 
    write_sequence(">".$fastaFile, "fasta", $seq) if $noSeq == 0;
    ##bp_genbank2gff3.pl

    my $circular = 0;
    $circular = 1 if (defined($seq->is_circular) && $seq->is_circular == 1);

    ## store taxonomy information, also used as "already downloaded check!"
    print TAX $seq->primary_id."\t".$id."\t".$seq->desc."\t".$specId."\t".$seq->species->ncbi_taxid."\t".$circular."\n";
    print STDERR "... done.\n";
}
close(TAX);

