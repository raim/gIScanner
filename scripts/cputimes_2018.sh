
## collecting statistics on cluster runs in bacteria_2018.sh

### SUBCMS: ALLCONTIGS AT LPZG
srcpath=~/programs/gIScanner
datpath=/scr/doofsan/raim/data/introns

seq=$datpath/allcontigs.fa

resdir=$datpath/results/subcms
logdir=$resdir/stat
mkdir -p $logdir

#for jobf in `ls -rt $resdir/log/*.e*`; do 
for jobf in `find $resdir/ -name "*.e*"`; do
    job=`echo $jobf|sed 's|.*/||;s/.*\.e//'`
    echo $job
    if [ -s $logdir/${job}_stats.txt ]; then
	echo STATS exist
    else
	echo get stats
       qacct -j $job &> $logdir/${job}_stats.txt
    fi
done

## FULLCMS: PHAGES AND ALLCONTIGS at lpzg
resdir=$datpath/results/fullcms
logdir=$resdir/stat
mkdir -p $logdir
for jobf in `ls -rt $resdir/log/*.e*`; do 
    job=`echo $jobf|sed 's|.*/||;s/.*\.e//'`
    echo $job
    if [ -s $logdir/${job}_stats.txt ]; then
	echo STATS exist
    else
	qacct -j $job &> $logdir/${job}_stats.txt
    fi
done


### SUBCMS: PHAGES AT HHU

srcpath=/home/machne/programs/gIScanner
datpath=/gpfs/project/machne/data/introns

seq=$datpath/phages.fa
infile=$datpath/allcms.txt

resdir=$datpath/results/subcms
logdir=$resdir/stat
mkdir -p $logdir

## doesn't work, jobs only in history for one day ;(
#for jobf in `find $resdir/ -name "*.OU"`; do
#    job=`echo $jobf|sed 's|.*/||;s/\.OU//g'`
#    echo $job
    #qstat -f -x $job > $logdir/${job}.txt
#done


datpath=~/data/introns/prokaryotes_2018
resdir=~/data/introns/prokaryotes_2018/hhu/results/subcms

## number of "!/?" hits
for res in `ls -1 $resdir/*tab`; do
    cm=`echo $res|sed "s|$resdir/||;s/.tab//;s/phages_//"`
    num=`grep -c "!" $res`
    echo $cm $num >> $datpath/sighits.txt
    num=`grep -c "?" $res`
    echo $cm $num >> $datpath/nonsighits.txt
done


## model length vs cpu time
grep CLEN $datpath/subcms/*.cm | sed 's|.*/||;s/\.cm:CLEN//;s/ \+/ /' > $datpath/allcms_clength.txt
grep CPU $resdir/phages*.out | sed 's|.*/phages_||;s/ Elapsed.*//;s/\.out\:\# CPU time:/ /;s/u / /;s/s / /' > $datpath/allcms_phages_times.txt
grep CPU $resdir/allcontigs*.out | sed 's|.*/allcontigs_||;s/ Elapsed.*//;s/\.out\:\# CPU time:/ /;s/u / /;s/s / /' > $datpath/allcms_allcontigs_times.txt


## SORT TIMES IN PHAGES
sort -k2 -n allcms_phages_times.txt

