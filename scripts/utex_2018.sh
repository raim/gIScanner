
srcpath=~/programs/gIScanner
datpath=~/data/introns/prokaryotes_2018/utex # at HHU exon
eslpath=~/programs/infernal-1.1.2/easel/miniapps # easel miniapps 
infpath=/usr/local # infernal executables
export INFERNALDIR=$infpath

cd $datpath

### SETUP GENOMES
## manually download fasta from NCBI
## circularize genomes
## TODO: restrict to those annotated as circular in gbk!!
$srcpath/scripts/circularizeSeq.R --seq $datpath/utex.fasta --ext 5000 > $datpath/elongatus_genomes_circular.fasta 
$srcpath/scripts/circularizeSeq.R --seq $datpath/utex_p1.fasta --ext 5000 >> $datpath/elongatus_genomes_circular.fasta 
$srcpath/scripts/circularizeSeq.R --seq $datpath/utex_p2.fasta --ext 5000 >> $datpath/elongatus_genomes_circular.fasta 
$srcpath/scripts/circularizeSeq.R --seq $datpath/7942.fasta --ext 5000 >> $datpath/elongatus_genomes_circular.fasta 
$srcpath/scripts/circularizeSeq.R --seq $datpath/7942_p1.fasta --ext 5000 >> $datpath/elongatus_genomes_circular.fasta 
$srcpath/scripts/circularizeSeq.R --seq $datpath/6301.fasta --ext 5000 >> $datpath/elongatus_genomes_circular.fasta 


## generate infernal index
$eslpath/esl-sfetch --index  $datpath/elongatus_genomes_circular.fasta 

## search fullcms
seq=$datpath/elongatus_genomes_circular.fasta 
resdir=$datpath/results/fullcms
logdir=$resdir/log
mkdir -p $logdir
Z=1
for cm in `grep -l cmcalibrate $srcpath/parameters/fullcms/*cm`; do
    cmf=$(basename ${cm%.*})
    res=${resdir}/allcontigs_${cmf}
    if [ -s $res.tab ]; then
	echo "$cmf exists"
    else
	echo "$cmf CALCULATE"
	${INFERNALDIR}/bin/cmsearch --cpu 7 -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq
    fi
done
## search subcms
seq=$datpath/elongatus_genomes_circular.fasta 
resdir=$datpath/results/subcms
logdir=$resdir/log
mkdir -p $logdir
Z=1
for cm in `grep -l cmcalibrate $srcpath/parameters/subcms/*cm`; do
    cmf=$(basename ${cm%.*})
    res=${resdir}/allcontigs_${cmf}
    if [ -s $res.tab ]; then
	echo "$cmf exists"
    else
	echo "$cmf CALCULATE"
	${INFERNALDIR}/bin/cmsearch --cpu 7 -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq
    fi
done

## no inc filter but increased minl
srcpath=~/programs/gIScanner
datpath=~/data/introns/prokaryotes_2018/utex
respath=$datpath/chainer
mkdir -p $respath

$srcpath/scripts/chainCMResults.R --dir $datpath/results  --cmd=subcms --cfd=fullcms --fend=_r112 --gdf=$srcpath/parameters/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --grf=$srcpath/parameters/gI_subgroups.dat --out=$respath --fig=$respath/figures  --chs=$respath/chains --store --maxr=0 --maxd=2500 --minl=2 --ovlf --plot --inc --exp --dof
