#!/bin/bash

## NOTE: THIS DOCUMENTS ALL MANUALLY EXECUTED STEPS in 2012
## sub-alignment generation, model calibration and genome scans
## are now (2018) split into `scripts/alignments_2012.sh`,
## `scripts/recalibrate_r112.sh`, and `scripts/bacteria_2018.sh` to
## re-do the latter two and allow for better adaptation to novel projects.

export GITDIR=~/programs/gIScanner
export SRCDIR=$GITDIR/scripts/ # sources (shell and R scripts)
export PARDIR=$GITDIR/parameters/ # parameters
export WORKDIR=~/work/introns/phylogeny/
cd $WORKDIR

## NOTE: additionally requires symbolic link to folders scripts and parameters 
## of the git in $WORKDIR and the commented out mkdir commands
## in $SRCDIR/download.sh!

## download all data
$SRCDIR/download.sh

### NOTE 2018 - CODE UNTIL STARTING HERE COPIED TO `alignments_2012.sh` 

### RFAM SETUP
## generate RFAM consensus structure
## this cmemit alignment was used to construct paramaters/rf00028_consensus.fa
cmemit -a rfam/RF00028_v11.cm > rfam/RF00028_v11_cmemit.stk
## MANUAL STEP !
## grep RF and SS_cons from rfam/RF00028_v11_cmemit.stk to fasta-style file
## $PARDIR/rf00028_consensus.fa (in CVS) and add lines 
## SS_anno, SS_dtb, SS_pkno, FRAGREP

## get consensus structures from original alignment
$SRCDIR/getConsensus.sh rfam/ rfam/RF00028_seed.stockholm.stk
## map annotation to original alignment structure - via RNAforester
$SRCDIR/mapStructureAnnotation.R $PARDIR/rf00028_consensus.fa rfam/RF00028_seed.stockholm_annotation.fa
## add back pseudoknot
sed -i 's/\./6/g' rfam/RF00028_seed.stockholm_annotation.fa
## annotate original aligment
$SRCDIR/annotateAlignment.R --anf=rfam/RF00028_seed.stockholm_annotation.fa --alf=rfam/RF00028_seed.stockholm.stk --empty="." > rfam/RF00028_seed.stockholm_annotated.stk
## plot consensus structure
$SRCDIR/plotRNA.R --anf=rfam/RF00028_seed.stockholm_annotation.fa --seq=SS_anno
## SPLIT ALIGNMENT
## TODO: add left/P1-P2, P7-P8/right
$SRCDIR/splitAlignment.R --alf=rfam/RF00028_seed.stockholm_annotated.stk --out=substk --fig=eps --ct="L-P1,P1-L2,P1-P2,R1-P2,P2-L3,R2-P3,P3-L4,R3-P4,P4-L5,R4-P5,P5-L6,R5-P6,P6-L7,R6-P7,P7-L8,R7-P8,R7-R6,R6-P8,L6-L8,R2-L4,L3-L4,L3-L6,R4-L7,R5-L7,L6-R3,P7-P8,P8-R"

## search RFAM seed sequences with RFAM CM
cmsearch -A rfam/RF00028_seed_RF00028_v11.stk --tblout rfam/RF00028_seed_RF00028_v11.tab rfam/RF00028_v11.cm rfam/RF00028_seed.fasta > rfam/RF00028_seed_RF00028_v11.dat
## convert to tab : probably not required anymore
perl $SRCDIR/stk2tab.pl rfam/RF00028_seed_RF00028_v11.stk > rfam/RF00028_seed_RF00028_v11.stk.tab
## align RFAM seed sequences to RFAM CM
cmalign rfam/RF00028_v11.cm  rfam/RF00028_seed.fasta > rfam/RF00028_seed_RF00028_v11_ali.stk 
## annotate
$SRCDIR/annotateAlignment.R --anf=$PARDIR/rf00028_consensus.fa --alf=rfam/RF00028_seed_RF00028_v11_ali.stk  --empty="~" > rfam/RF00028_seed_RF00028_v11_ali_annotated.stk 
## convert to tab file : probably not required anymore
perl $SRCDIR/stk2tab.pl rfam/RF00028_seed_RF00028_v11_ali_annotated.stk > rfam/RF00028_seed_RF00028_v11_ali_annotated.stk.tab


# translate all frames on each chromosome
transeq -frame 6 -sequence rfam/RF00028_seed.fasta -outseq rfam/RF00028_seed.pep.fasta
### PFAM ANALYSIS - RFAM
## search Pfam in giisd
# LAGLIDADG_1: 
hmmsearch --domtblout pfam/RF00028_seed_PF00961.tab pfam/PF00961.hmm rfam/RF00028_seed.pep.fasta > pfam/RF00028_seed_PF00961.dat 
# LAGLIDADG_2:
hmmsearch --domtblout pfam/RF00028_seed_PF03161.tab pfam/PF03161.hmm rfam/RF00028_seed.pep.fasta  > pfam/RF00028_seed_PF03161.dat
# GIY-YIG:
hmmsearch --domtblout pfam/RF00028_seed_PF01541.tab pfam/PF01541.hmm rfam/RF00028_seed.pep.fasta  > pfam/RF00028_seed_PF01541.dat
# HNH:
hmmsearch --domtblout pfam/RF00028_seed_PF01844.tab pfam/PF01844.hmm rfam/RF00028_seed.pep.fasta  > pfam/RF00028_seed_PF01844.dat
# PI-PfuI Endonuclease subdomain:
hmmsearch --domtblout pfam/RF00028_seed_PF09062.tab pfam/PF09062.hmm rfam/RF00028_seed.pep.fasta > pfam/RF00028_seed_PF09062.dat
# Hom_end:
hmmsearch --domtblout pfam/RF00028_seed_PF05204.tab pfam/PF05204.hmm rfam/RF00028_seed.pep.fasta > pfam/RF00028_seed_PF05204.dat

### GISSD SETUP
## collect intron types in GISSD
$SRCDIR/processGissd.sh > gissd/IDs.dat
cat gissd/I*fasta > gissd.fasta
## remove protein sequence from Tfl.S1506-2 3'exon 
patch -b gissd/gissd.fasta $PARDIR/gissd.fasta.patch
## remove duplicate intron sequence from IE3 alignment
patch -b gissd/IE3.sto $PARDIR/gissd_IE3.sto.patch


## GENERATE GISSD SUBTYPE CMs and ALIGNMENTs from original GISSD alignments
$SRCDIR/cmbuildall.sh gissd/*.sto
$SRCDIR/cmcalibrateall.sh gissd/*cm > gissd/calibration.log
mv cmcalibrate.* gissd/log
## generate new alignments from all subtypes
$SRCDIR/cmaligngissd.sh

## gissd consensus structures and annotation
$SRCDIR/cmemitall.sh gissd/*_v11.cm
## map manual annotation from Rfam
$SRCDIR/mapStructureAnnotation.R $PARDIR/rf00028_consensus.fa gissd/*v11_cmemit.fasta
## MANUAL STEP !
## copy gissd/*v11_cmemit.fasta to parameters/*consensus.fa  (in CVS) and 
## edit the mapped annotation in line SS_annm, use RNAplots and SS_guid
## as guide

## generate annotated cmalignments
for i in `ls gissd/I*.stk|grep -v sto.stk|grep -v annotated.stk`; do
    anf=$PARDIR/$(basename ${i%.*})_consensus.fa
    o=${i%.*}_annotated.stk
    echo $anf $i $o
    ## 20131120: is this really not used ??
    #$SRCDIR/annotateAlignment.R --anf=$anf --alf=$i --empty="." > $o
done
for i in `ls $PARDIR/*consensus.fa`; do 
    echo $i
    $SRCDIR/plotRNA.R --anf=$i --seq=SS_anno
done
## MAP ANNOTATION TO ORIGINAL ALIGNMENTS
## collect consensus structures from original files
$SRCDIR/getConsensus.sh gissd/ gissd/*sto
## map to manually constructed annotation of cmemit-derived files
for i in `ls $PARDIR/I*consensus.fa`; do
    ## NOTE 20180504: this won't work after split of srcdir and workdir!
    j=`echo $i|sed 's/consensus.fa/annotation.fa/g;s/parameters/gissd/g'`
    echo $i $j
    $SRCDIR/mapStructureAnnotation.R $i $j
done
## add back pseudoknot!!
sed -i 's/\./6/g' gissd/I*_annotation.fa

## generate annotated cmalignments
for i in `ls gissd/I*.sto|grep -v annotated`; do
    anf=gissd/$(basename ${i%.*})_annotation.fa
    o=${i%.*}_orig_annotated.stk
    echo $anf $i $o
    $SRCDIR/annotateAlignment.R --anf=$anf --alf=$i --empty="." > $o
done
for i in `ls gissd/*annotation.fa`; do 
    echo $i
    $SRCDIR/plotRNA.R --anf=$i --seq=SS_anno
done
## SPLIT ALIGNMENTS
for i in gissd/*orig_annotated.stk; do
    echo "cutting" $i
    $SRCDIR/splitAlignment.R --alf=$i --out=substk --fig=eps --ct="L-P1,P1-L2,P1-P2,R1-P2,P2-L3,R2-P3,P3-L4,R3-P4,P4-L5,R4-P5,P5-L6,R5-P6,P6-L7,R6-P7,P7-L8,R7-P8,R7-R6,R6-P8,L6-L8,R2-L4,L3-L4,L3-L6,R4-L7,R5-L7,L6-R3,P7-P8,P8-R"
done


## OTHER
## translate all frames on each chromosome
transeq -frame 6 -sequence gissd/gissd.fasta -outseq gissd/gissd_pep.fasta

### FRAGREP - GISSD
## TODO: allow 1 big insertion
## only @ leipzig
/usr/local/bin/aln2pattern -m rfam/RF00028_seed_annotated.aln > rfam/RF00028_seed_annotated.pattern
/usr/local/bin/fragrep -q rfam/RF00028_seed_annotated.pattern gissd/gissd.fasta > rfam/gissd_RF00028_fragrep.fa

## search GISSD SUBGROUPS VS. GISSD
#cmsearch -A gissd/gissd_IA1.stk --tblout gissd/gissd_IA1.tab gissd/IA1.sto.cm gissd/gissd.fasta > gissd/gissd_IA1.dat


### ANALYSIS OF GISSD INTRONS
## SEARCH RFAM CM IN GISSD SEQUENCES
## new infernal
cmsearch -A rfam/gissd_RF00028_v11.stk --tblout rfam/gissd_RF00028_v11.tab rfam/RF00028_v11.cm gissd/gissd.fasta > rfam/gissd_RF00028_v11.dat
cmsearch --mid -A rfam/gissd_RF00028_v11_mid.stk --tblout rfam/gissd_RF00028_v11_mid.tab rfam/RF00028_v11.cm gissd/gissd.fasta > rfam/gissd_RF00028_v11_mid.dat
cmsearch --max -A rfam/gissd_RF00028_v11_max.stk --tblout rfam/gissd_RF00028_v11_max.tab rfam/RF00028_v11.cm gissd/gissd.fasta > rfam/gissd_RF00028_v11_max.dat
## align GISSD subtypes to RFAM CM
$SRCDIR/cmaligngissd2rfam.sh rfam/RF00028_v11.cm gissd/I*_seq.fasta gissd/gissd.fasta
mv I*RF00028_v11.stk gissd_RF00028_v11.stk rfam/
## convert to tab
perl $SRCDIR/stk2tab.pl rfam/gissd_RF00028_v11_ali.stk > rfam/gissd_RF00028_v11_ali.stk.tab

## convert stockholm alignments to tab file
## rfam seed
perl $SRCDIR/stk2tab.pl rfam/RF00028_seed.stockholm.stk > rfam/RF00028_seed.stockholm.stk.tab
## gissd
perl $SRCDIR/stk2tab.pl rfam/gissd_RF00028_v11.stk > rfam/gissd_RF00028_v11.stk.tab
perl $SRCDIR/stk2tab.pl rfam/gissd_RF00028_v11_mid.stk > rfam/gissd_RF00028_v11_mid.stk.tab
perl $SRCDIR/stk2tab.pl rfam/gissd_RF00028_v11_max.stk > rfam/gissd_RF00028_v11_max.stk.tab

## ANNOTATE ALIGNMENTS
$SRCDIR/annotateAlignment.R --anf="parameters/rf00028_consensus.fa" --alf=rfam/gissd_RF00028_v11_ali.stk.tab --empty="~" > rfam/gissd_RF00028_v11_ali_annotated.stk

### PFAM ANALYSIS - GISSD
## search Pfam in giisd
# 162 frames with LAGLIDADG_1: 
hmmsearch --domtblout pfam/gissd_PF00961.tab pfam/PF00961.hmm gissd/gissd_pep.fasta > pfam/gissd_PF00961.dat 
# 66 frames with LAGLIDADG_2:
hmmsearch --domtblout pfam/gissd_PF03161.tab pfam/PF03161.hmm gissd/gissd_pep.fasta  > pfam/gissd_PF03161.dat
# 14 frames with GIY-YIG:
hmmsearch --domtblout pfam/gissd_PF01541.tab pfam/PF01541.hmm gissd/gissd_pep.fasta  > pfam/gissd_PF01541.dat
# 4 frames with HNH:
hmmsearch --domtblout pfam/gissd_PF01844.tab pfam/PF01844.hmm gissd/gissd_pep.fasta  > pfam/gissd_PF01844.dat
# 2 frames with PI-PfuI Endonuclease subdomain:
hmmsearch --domtblout pfam/gissd_PF09062.tab pfam/PF09062.hmm gissd/gissd_pep.fasta > pfam/gissd_PF09062.dat
# 4 frames with Hom_end:
hmmsearch --domtblout pfam/gissd_PF05204.tabxpfam/PF05204.hmm gissd/gissd_pep.fasta > pfam/gissd_PF05204.dat


## plot RFAM CM -> GISSD alignments and PFAM HITS
R --args < $SRCDIR/analyzeGissd.R
## TODO 201805: why is $SRCDIR/analyzeRfam.R not used?

#### NOTE 2018 - CODE UNTIL HERE COPIED TO `alignments_2012.sh` 

### GENERATE SUB-CMs  
## based on substk sub-STKs generated above from annotated 
## gissd and rfam alignments

## AT TBI ONLY - USE OF MPI PARALLELIZATION AND QUEUE!! 

## OLD INFERNAL
export INFERNALDIR=/scr/airline/choener/Software/infernal-1.0.2/
## build CMs
$SRCDIR/cmbuildall.sh substk/*.stk
mv substk/*cm subcms3

## forecast calibration time
$SRCDIR/cmcalibrateall.sh `ls -Sr subcms3/*.cm` forecast &> subcms3/forecast.log
## CALIBRATE SUBCMS
$SRCDIR/cmcalibrateall.sh `ls -Sr subcms3/*.cm` &> subcms3/calibration.log
mv cmcalibrate.* subcms3/log


## NEW INFERNAL
export INFERNALDIR=/scr/airline/choener/Software/infernal-1.1rc1/
## build CMs
$SRCDIR/cmbuildall.sh substk/*.stk
mv substk/*_v11.cm subcms4
## forecast calibration time
$SRCDIR/cmcalibrateall.sh `ls -Sr subcms4/*_v11.cm` forecast &> subcms4/forecast_v11.log
## CALIBRATE SUBCMS
$SRCDIR/cmcalibrateall.sh `ls -Sr subcms4/*_v11.cm` &> subcms4/calibration_v11.log
mv cmcalibrate.* subcms4/log


### SETUP TEST GENOMES
## TEST GENOMES
## set up blast DB from test genomes
export WORKDIR=~/work/introns/phylogeny/
cd $WORKDIR/genomes/gI_test
for i in `cut $PARDIR/gissdTestCases.dat -f 1`; do
    echo $i
    $SRCDIR/fetchGenome.pl -db Nucleotide -f fasta -o $i.fa -qu $i
    ## index fasta files for parallel cmsearch
    /home/mescalin/choener/tmp/infernal/infernal-1.1rc1/easel/miniapps/esl-sfetch --index $seq
done
cat *.fa > alltest.fasta
formatdb -i  alltest.fasta -p F -o T
/home/mescalin/choener/tmp/infernal/infernal-1.1rc1/easel/miniapps/esl-sfetch --index alltest.fasta


## fetch GISSD intron sequences and blast them
mkdir genomes/gI_test/blast
export WORKDIR=~/work/introns/phylogeny/
cd $WORKDIR/genomes/gI_test/
for i in `cut $PARDIR/gissdTestCases.dat -f 2`; do
    for j in `echo $i|tr ";" "\n"`; do
	echo $j
	fasta-fetch $WORKDIR/gissd/gissd.fasta $j | blastn -query - -db alltest.fasta -outfmt 7 > blast/$j.tab
    done
done
## collapse results for each genome
for i in `cut $PARDIR/gissdTestCases.dat -f 1`; do
    rm $WORKDIR/genomes/gI_test/blast/$i.tab
    grep -h $i $WORKDIR/genomes/gI_test/blast/*tab > $WORKDIR/genomes/gI_test/blast/$i.tab
done

## search PFAM 

## search sub-CMs in test genomes
export WORKDIR=~/work/introns/phylogeny/
cd  $WORKDIR/
resdir=$WORKDIR/genomes/gI_test/cmsearch
seq=$WORKDIR/genomes/gI_test/alltest.fasta
log=$WORKDIR/genomes/gI_test/qsubmit.log
logdir=$resdir/log
touch $log 
for cm in `grep -l cmcalibrate subcms4/*cm`; do
    cmf=$(basename ${cm%.*})
    res=$resdir"/alltest_"$cmf
    sed -i "s/^#\$ -N cmsearch.*/#$ -N cmsearch_${cmf}/g" $SRCDIR/ompi_cmsearch
    echo "ompi_cmsearch -o" $res.out "-A" $res.stk "--tblout" $res.tab $cm $seq >> $log
    qsub -e $logdir -o $logdir $SRCDIR/ompi_cmsearch -o $res.out -A $res.stk --tblout $res.tab $cm $seq >> $log
        #cmsearch -A $res.stk --tblout $res.tab $cm $seq > $res.out
done
mv cmsearch.* $resdir/log

## collapse results for each genome
for i in `cut $PARDIR/gissdTestCases.dat -f 1`; do
    grep -h $i $resdir/*_v11.tab > $resdir/$i"_v11.tab"
done

## search full CMs in test genomes
export WORKDIR=~/work/introns/phylogeny/
resdir=$WORKDIR/genomes/gI_test/cmfull
seq=$WORKDIR/genomes/gI_test/alltest.fasta
log=$WORKDIR/genomes/gI_test/qsubmit.log
logdir=$resdir/log
for cm in `grep -l cmcalibrate rfam/RF00028_v11.cm  gissd/I*_v11.cm`; do
    cmf=$(basename ${cm%.*})
    res=$resdir"/alltest_"$cmf
    sed -i "s/^#\$ -N cmsearch.*/#$ -N cmsearch_${cmf}/g" $SRCDIR/ompi_cmsearch
    echo "ompi_cmsearch -o" $res.out "-A" $res.stk "--tblout" $res.tab $cm $seq >> $log
    qsub -e $logdir -o $logdir $SRCDIR/ompi_cmsearch -o $res.out -A $res.stk --tblout $res.tab $cm $seq >> $log
        #cmsearch -A $res.stk --tblout $res.tab $cm $seq > $res.out
done
#mv cmsearch.* $resdir/log
for i in `cut $PARDIR/gissdTestCases.dat -f 1`; do
    grep -h $i $resdir/*_v11.tab > $resdir/$i"_v11.tab"
done


for i in `cut $PARDIR/gissdTestCases.dat -f 1`; do
    $SRCDIR/collectCMResults.R --seq $i --dir /homes/biertank/raim/work/introns/phylogeny/genomes/gI_test/ --cmdir=cmsearch --bldir=blast --gdf=/homes/biertank/raim/work/introns/phylogeny/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --fsdir="/" --cmfull=cmfull --fend=_v11 --out=testFigures --inc --store --maxr=0 --maxd=5000 --minl=3
done


## SETUP BACTERIA AND PHAGE GENOMES

## TODO: cover circular genomes! add 5 kb from start to end of each
## circular genome

## collect all nt fasta files into one file
touch genomes/bacteria/allbacteria.fasta
for i in `find genomes/bacteria/ -name "*.fna"`; do
    echo $i
    cat $i >> genomes/bacteria/allbacteria.fasta
done

## SEQUENCE<->SPECIES MAPPINGS
## as constructed by fetchFastaFromID.pl
## all analyzed sequences, grep gi and acc from fasta file
grep ">" genomes/bacteria/allbacteria.fasta | sed 's/.*gi|\([0-9.]*\)|ref|\(.*\)\.[0-9.]*|\(.*\)/\1\t\2\t\3/g' > genomes/bacteria/tmp.dat
## use gi to map to taxon id and species name! note that a similar file
## is generated by fetchFastaFromNCBI.pl (sequences retrieval via e.utils)
perl $SRCDIR/seq2spec.pl genomes/bacteria/tmp.dat -tx taxonomy > genomes/bacteria/taxonomy.dat 2> genomes/bacteria/taxonomy_missing.dat
\rm genomes/bacteria/tmp.dat
## add circular information
grep -h LOCUS `find genomes/bacteria/gbk/ -name "*.gbk"` |grep circular| sed 's/LOCUS\s*\(N._[0-9]*\).*/\1\t1/g' > genomes/bacteria/circular.dat
grep -h LOCUS `find genomes/bacteria/gbk/ -name "*.gbk"` |grep linear  | sed 's/LOCUS\s*\(N._[0-9]*\).*/\1\t0/g' >> genomes/bacteria/circular.dat

## generate species/directory mapping
## via bioproject summary.txt file: _uid{PR..123} <-> taxon mapping
## search the taxon id for each directory
\rm genomes/bacteria/dir2species.dat
for dir in `ls genomes/bacteria/fasta/ -1`; do
    spec=`echo $dir|sed 's/_uid.*$//g;s/_/ /g'`
    prjna=`echo $dir|sed 's/.*_uid\(.*\)$/\1/g;s/_/ /g'`
    taxid=`grep -P "\tPR[A-Z]+\`echo ${prjna}\`\t" genomes/bacteria/summary.txt | cut -f 2`
    echo -e "$dir\t$taxid" >> genomes/bacteria/dir2species.dat
done
\cp genomes/bacteria/dir2species.dat genomes/bacteria/dir2species.dat.bak
## next, replace wrong taxonomy IDs (in the ncbi summary.txt file) by
## taxon IDs in taxonomy.dat
## TODO: repair wrongBioProcectTxons file
for tax in `cut -d " " -f 1 $PARDIR/wrongBioProjectTaxon.dat|grep -v "^#"`; do
    echo $tax;
    seq=`grep "$tax$" genomes/bacteria/taxonomy.dat | cut -f 2|tr '\n' ' ' |sed 's/ .*//g'`;
    dir=`find genomes/bacteria/fasta/ -name "\`echo $seq\`*.fna"| sed 's/genomes\/bacteria\/fasta\///g;s/\/N.*fna//g'`;
    grep $dir genomes/bacteria/dir2species.dat;
    sed -i "s/$dir\t.*/$dir\t$tax/g" genomes/bacteria/dir2species.dat;
done

## blast DB
echo "GENERATING BLAST DB"
formatdb -i  genomes/bacteria/allbacteria.fasta -p F -o T
## ncbi index
#fasta-make-index genomes/bacteria/allbacteria.fasta
## infernal index
echo "INDEXING FOR INFERNAL"
/home/mescalin/choener/tmp/infernal/infernal-1.1rc1/easel/miniapps/esl-sfetch --index genomes/bacteria/allbacteria.fasta

## TAXONOMY TREES for all species in dataset

## TODO: update all taxonomys based on taxonomy.dat and dir2species!

## Analyzed Species
cut -f 5 genomes/bacteria/taxonomy.dat |grep -v "^taxon" | sort -n | uniq > /tmp/taxids
#perl $SRCDIR/tax2newick.pl -f /tmp/taxids -n -id > genomes/bacteria/taxonomy.phy
$SRCDIR/tax2newick.R -idf /tmp/taxids -txd taxonomy > genomes/bacteria/taxonomy.phy 2> genomes/bacteria/taxonomy.log

## 16S rRNA Phylogeny Species
## map 16S sequence Genbank ID -> GI -> taxon ID 
## Genbank to GI for 16S rRNA species
## TODO: why are AY596297,CP000924,CP001336 duplicate/triplicate?
cut -f 1 taxonomy/LTPs111_SSU.csv | perl $SRCDIR/getIDs.pl -f taxonomy/GbAccList.0127.2013 -d "," -c 0 > taxonomy/LTPs111_SSU_gb2gi.csv 2> taxonomy/LTPs111_SSU_gb2gi.log
## GI to taxonomy via taxonomy/gi_taxid_nucl.dmp
## seq. GI replacement, 15074885 (gi) not found! seq. replaced by GI 444735728
cut -d , -f 3 taxonomy/LTPs111_SSU_gb2gi.csv | sed  's/15074885/444735728/' | perl $SRCDIR/getIDs.pl -f taxonomy/gi_taxid_nucl.dmp -d "\t" -c 0  > taxonomy/LTPs111_SSU_gi2tax.csv 2> taxonomy/LTPs111_SSU_gi2tax.log
## replace outdated (merged) taxon ID
sed -i 's/\t399802/\t573179/g' taxonomy/LTPs111_SSU_gi2tax.csv

## generate tree with taxon IDs: the script generates two files;
## taxonomy/LTPs111_SSU_tree2tax.dat  maps the old species names to taxon IDs
## taxonomy/LTPs111_SSU.tree_taxonIDs.newick is the 16S rRNA tree with 
##    taxon IDs and species names (but maintaining clade names)
## NOTE 2018 - changed script, requires to edit version to 111 !!
$SRCDIR/convert16Stree.R

## Phispy Training Set Species
O=$IFS ## spaces in cut string
IFS=$(echo -en "\n\b")
echo -e "id\tname\ttaxon" > taxonomy/phispy_taxons.dat
for sp in `cut -f 2 $PARDIR/phispy_species.dat`; do
    echo -e `grep $sp $PARDIR/phispy_species.dat|tr '\n' '\t'` `grep $sp taxonomy/names.dmp|grep scientific | cut -f 1 | tr '\n' ';' |sed 's/;$//'` >> taxonomy/phispy_taxons.dat
done
IFS=$O

## taxonomy for phispy only
## TODO : only use one of phispy-taxon mapping to save time
#cut -f 3 taxonomy/phispy_taxons.dat  | grep -v "^taxon" | tr ';' '\n' | tr -d " " |sort|uniq > /tmp/phispytaxids
## only take first!
cut -f 3 taxonomy/phispy_taxons.dat  | grep -v "^taxon" | sed 's/;.*//g'| tr -d " " |sort|uniq > /tmp/phispytaxids
#perl $SRCDIR/tax2newick.pl -tax taxonomy -f /tmp/phispytaxids -n -id > taxonomy/phispy_taxons.phy 2> taxonomy/phispy_taxons.log
$SRCDIR/tax2newick.R -verb -txd taxonomy -idf /tmp/phispytaxids > taxonomy/phispy_taxons.phy 2> taxonomy/phispy_taxons.log

## taxonomy phispy X species
cut -f 5 genomes/bacteria/taxonomy.dat |grep -v "^taxon" > /tmp/phispyXspecies_rdn
## only take first!
cut -f 3 taxonomy/phispy_taxons.dat  | grep -v "^taxon" | sed 's/;.*//g'| tr -d " " >> /tmp/phispyXspecies_rdn
sort -n /tmp/phispyXspecies_rdn  | uniq > /tmp/phispyXspecies
#perl $SRCDIR/tax2newick.pl -tax taxonomy -f /tmp/phispyXspecies -n -id > taxonomy/phispyXspecies.phy 2> taxonomy/phispyXspecies.log
$SRCDIR/tax2newick.R -verb  -txd taxonomy -idf /tmp/phispyXspecies > taxonomy/phispyXspecies.phy 2> taxonomy/phispyXspecies.log

## taxonomy including 16S rRNA
## TODO:instead generate this tree inside cmpTaxa to avoid
## collapse.singletons()!!
cut -f 5 genomes/bacteria/taxonomy.dat |grep -v "^taxon" > /tmp/alltaxids_rdn
cut -f 2 taxonomy/LTPs111_SSU_gi2tax.csv >> /tmp/alltaxids_rdn
## only take first!
cut -f 3 taxonomy/phispy_taxons.dat  | grep -v "^taxon" | sed 's/;.*//g'| tr -d " " >> /tmp/alltaxids_rdnw
sort -n /tmp/alltaxids_rdn  | uniq > /tmp/alltaxids
#perl $SRCDIR/tax2newick.pl -tax taxonomy -f /tmp/alltaxids -n -id > taxonomy/all_taxons.phy 2> taxonomy/all_taxons.log
$SRCDIR/tax2newick.R -verb -txd taxonomy -idf /tmp/alltaxids > taxonomy/all_taxons.phy 2> taxonomy/all_taxons.log

##16S rRNA PHYLOGENY <-> TAXONOMY MAPPING
s2t=genomes/bacteria/phylogeny
$SRCDIR/cmpTaxa.R  -txd taxonomy  -tn acc -tf taxonomy/LTPs111_SSU_tree2tax.dat -sf genomes/bacteria/taxonomy.dat -out $s2t -dbug &> $s2t.log

s2t=genomes/bacteria/phylogeny_sub
/usr/bin/time -o=16S_sub $SRCDIR/cmpTaxa.R  -txd taxonomy  -tn acc -tf taxonomy/LTPs111_SSU_tree2tax.dat -sf genomes/bacteria/taxonomy.dat -out $s2t -dbug -sub &> $s2t.log
s2t=genomes/bacteria/phylogeny_nosub
/usr/bin/time -o=16S_nosub $SRCDIR/cmpTaxa.R  -txd taxonomy  -tn acc -tf taxonomy/LTPs111_SSU_tree2tax.dat -sf genomes/bacteria/taxonomy.dat -out $s2t -dbug &> $s2t.log


## PROPHAGE PREDICTION


## TODO : make directory <-> taxon index file! (in tataProject/parameters?)
## PHISPY
export PHISPY=/scr/hotchocolate/raim/software/phiSpyNov11_v2.2/
export WORKDIR=~/work/introns/phylogeny/
gen=$WORKDIR/genomes/bacteria/

## loop through dir2species, and for each directory/species, generate
## phispy dir for each gbk file
d2s=$gen/dir2species.dat
gbd=$WORKDIR/genomes/bacteria/gbk/
$SRCDIR/phispydirs.sh $d2s $gbd &> $gen/phispydirs.log

## get closest phispy training set species
## use only first from phispy taxa, option -sub faster in this case
cat taxonomy/phispy_taxons.dat  | sed 's/;.*//g' > /tmp/phispyTaxa.dat
s2p=$gen/species2phispy
$SRCDIR/cmpTaxa.R -txd taxonomy -tn name -tf /tmp/phispyTaxa.dat -sf genomes/bacteria/taxonomy.dat -out $s2p -dbug -sub &> $s2p.log

## loop through species2phispy, select closest species from phispy
## training set, use 0 'Generic Test Set' if distance rank > class
## TODO: in cmpTaxa.R use lowest rank instead of total tree distance!
s2p=$gen/species2phispy.dat
d2s=$gen/dir2species.dat
txr=$PARDIR/taxonomy_ranks.dat
gbd=$WORKDIR/genomes/bacteria/gbk/
$SRCDIR/runphispy.sh $s2p $d2s class $gbd $txr &> $gen/phispy_3.log

## TODO: MISSING ? run phisphydirs again!!
# Brucella_melitensis_biovar_Abortus_2308_uid62937/NC_007624_phage/
# ? same as `grep Cannot genomes/bacteria/phispy_3.log` ??
# Problem: "In the GenBank file, for a gene/RNA, locus_tag is missing. locus_tag is required for each gene/RNA."; `grep GENER $gen/phispydirs_check.log|wc -l` : 37 directories affected
$SRCDIR/phispydirs.sh $d2s $gbd &> $gen/phispydirs_check.log

## write log file to count phispy calculations
$SRCDIR/runphispy.sh $s2p $d2s class $gbd $txr &> $gen/phispy_check.log
## TODO: count phispy success stats

## COLLECT PHISPHY HITS
cat `find genomes/bacteria/gbk/ -name prophage.tbl`| sort |uniq | sed 's/^\(.*\)\t\(N.*\)_\(.*\)_\(.*\)/\1\t\2\t\3\t\4/g' > genomes/bacteria/prophages.tbl

## manual phispy call
#spc=$gen/gbk/Staphylococcus_aureus_TW20_uid159241;  gbk=$spc/NC_017331.gbk
#spc=$gen/gbk/Streptococcus_pyogenes_MGAS8232_uid57871/;gbk=$spc/NC_003485.gbk
#log=$WORKDIR/phispy.log
#seed=$spc/seed
#phage=$spc/phage
#mkdir -p $seed $phage
#spc=$gen/gbk/Staphylococcus_aureus_TW20_uid159241/;  gbk=$spc/NC_017331.gbk
#spc=$gen/gbk/Streptococcus_pyogenes_MGAS8232_uid57871/;gbk=$spc/NC_003485.gbk
#python $PHISPY/genbank_to_seed.py $gbk $seed



## subCM SEARCH
export WORKDIR=~/work/introns/phylogeny/
resdir=$WORKDIR/genomes/bacteria/allbacteria/cmsearch
seq=$WORKDIR/genomes/bacteria/allbacteria.fasta
logdir=$resdir/log
log=$logdir/log
#touch $log
Z=1
for cm in `grep -l cmcalibrate subcms4/*cm`; do
    cmf=$(basename ${cm%.*})
    res=$resdir"/allbacteria_"$cmf
    sed -i "s/^#\$ -N cmsearch.*/#$ -N cmsearch_${cmf}/g" $SRCDIR/ompi_cmsearch
    echo "ompi_cmsearch -Z" $Z " -o" $res.out "-A" $res.stk "--tblout" $res.tab $cm $seq >> $log
    qsub -e $logdir -o $logdir $SRCDIR/ompi_cmsearch -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq >> $log
        #cmsearch -A $res.stk --tblout $res.tab $cm $seq > $res.out
done

## redo failed: IA1_orig_annotated_P7-L8 and IB2_orig_annotated_L3-L4
for cm in `ls subcms4/IA1_orig_annotated_P7-L8_v11.cm subcms4/IB2_orig_annotated_L3-L4_v11.cm`; do
    cmf=$(basename ${cm%.*})
    res=$resdir"/allbacteria_"$cmf
    sed -i "s/^#\$ -N cmsearch.*/#$ -N cmsearch_${cmf}/g" $SRCDIR/ompi_cmsearch
    echo "ompi_cmsearch -Z" $Z " -o" $res.out "-A" $res.stk "--tblout" $res.tab $cm $seq >> $log
    qsub -e $logdir -o $logdir $SRCDIR/ompi_cmsearch -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq >> $log
done
## 2013 03 18: IC2, IB2, IB4, IA1_P7-L8, IB2_L3-L4: copied from old
## calculation, since it takes too long and blocks queue for phages!

## fullCM search
export WORKDIR=~/work/introns/phylogeny/
resdir=$WORKDIR/genomes/bacteria/allbacteria/cmfull
seq=$WORKDIR/genomes/bacteria/allbacteria.fasta
logdir=$resdir/log
log=$logdir/log
touch $log
Z=1
for cm in `grep -l cmcalibrate rfam/RF00028_v11.cm  gissd/I*_v11.cm`; do
    cmf=$(basename ${cm%.*})
    res=$resdir"/allbacteria_"$cmf
    sed -i "s/^#\$ -N cmsearch.*/#$ -N cmsearch_${cmf}/g" $SRCDIR/ompi_cmsearch
    echo "ompi_cmsearch -e $logdir -o $logdir  -Z" $Z "-o" $res.out "-A" $res.stk "--tblout" $res.tab $cm $seq >> $log
    qsub -e $logdir -o $logdir $SRCDIR/ompi_cmsearch -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq >> $log
done
# change # of CPUs
#qalter -u raim -pe ompi 40-50 



## collect results from bacteria!
$SRCDIR/chainCMResults.R --dir genomes/bacteria/allbacteria  --cmd=cmsearch  --cfd=cmfull --fend=_v11 --gdf=substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --gff=genomes/bacteria/gff --phf=genomes/bacteria/prophages.tbl --out=genomes/bacteria/allbacteria/results --fig=genomes/bacteria/allbacteria/results/figures  --chs=genomes/bacteria/allbacteria/results/chains --inc --exp --store --maxr=0 --maxd=2500 --minl=3 --ovlf --plot --dof

## extract sequences around candidates
## fetchSeq also defines IDs for candidates!
$SRCDIR/fetchSeq.R --hit genomes/bacteria/allbacteria/results/results.tab  --dir genomes/bacteria/fasta --ext 5000 --type cmchain --tid cmchain_ --hid cmchain.ID --sid acc --circular --idx genomes/bacteria/circular.dat:1:2  > genomes/bacteria/allbacteria/results/candidates_5000.fasta
$SRCDIR/fetchSeq.R --hit genomes/bacteria/allbacteria/results/results.tab  --dir genomes/bacteria/fasta --ext 2500 --type cmchain --tid cmchain_ --hid cmchain.ID --sid acc --circular --idx genomes/bacteria/circular.dat:1:2  > genomes/bacteria/allbacteria/results/candidates_2500.fasta
$SRCDIR/fetchSeq.R --hit genomes/bacteria/allbacteria/results/results.tab  --dir genomes/bacteria/fasta --ext 1250 --type cmchain --tid cmchain_ --hid cmchain.ID --sid acc --circular --idx genomes/bacteria/circular.dat:1:2  > genomes/bacteria/allbacteria/results/candidates_1250.fasta
$SRCDIR/fetchSeq.R --hit genomes/bacteria/allbacteria/results/results.tab  --dir genomes/bacteria/fasta --ext 500 --type cmchain --tid cmchain_ --hid cmchain.ID --sid acc --circular --idx genomes/bacteria/circular.dat:1:2  > genomes/bacteria/allbacteria/results/candidates_500.fasta
## ncbi index
#fasta-make-index genomes/bacteria/allbacteria.fasta
fasta-make-index /home/mescalin/raim/work/introns/phylogeny//genomes/bacteria/allbacteria/results/candidates_500.fasta
fasta-make-index /home/mescalin/raim/work/introns/phylogeny//genomes/bacteria/allbacteria/results/candidates_2500.fasta
fasta-make-index /home/mescalin/raim/work/introns/phylogeny//genomes/bacteria/allbacteria/results/candidates_1250.fasta
## infernal index 
/home/mescalin/choener/tmp/infernal/infernal-1.1rc1/easel/miniapps/esl-sfetch --index genomes/bacteria/allbacteria/results/candidates_2500.fasta
/home/mescalin/choener/tmp/infernal/infernal-1.1rc1/easel/miniapps/esl-sfetch --index genomes/bacteria/allbacteria/results/candidates_1250.fasta
/home/mescalin/choener/tmp/infernal/infernal-1.1rc1/easel/miniapps/esl-sfetch --index genomes/bacteria/allbacteria/results/candidates_500.fasta

## CANDIDATES full CM --max search
export WORKDIR=~/work/introns/phylogeny
fstdir=$WORKDIR/genomes/bacteria/allbacteria/results/fasta
resdir=$WORKDIR/genomes/bacteria/allbacteria/results/cmfull
seq=$WORKDIR/genomes/bacteria/allbacteria/results/candidates_1250.fasta
#seq=$WORKDIR/genomes/bacteria/allbacteria/results/candidates_500.fasta
log=$WORKDIR/genomes/bacteria/allbacteria/results/qsubmit.log
logdir=$resdir/log


mkdir -p $fstdir
Z=0.001

for cd in `cut -f 1 genomes/bacteria/allbacteria/results/results.tab|grep -v \#|sed '/^$/d'|grep cmchain_`; do
    cdf=$fstdir/$cd".fasta"
    fasta-fetch $seq $cd > $cdf
done

for cd in `cut -f 1 genomes/bacteria/allbacteria/results/results.tab|grep -v \#|sed '/^$/d'|grep cmchain_`; do
    cdf=$fstdir/$cd".fasta"
    for cm in `grep -l cmcalibrate $WORKDIR/rfam/RF00028_v11.cm  $WORKDIR/gissd/I*_v11.cm`; do
	cmn=$(basename ${cm%.*})
    	res=$resdir"/"$cd"_"$cmn
    	if [ -f $res.tab ]; then 
	    echo "$res exists"
	else
	    echo "calculate $res"
	    sed -i "s/^#\$ -N cmsearch.*/#$ -N cmsearch_${cd}_$cmn/g" $SRCDIR/ompi1_cmsearch
    	    echo "qsub -e $logdir -o $logdir $SRCDIR/ompi1_cmsearch --max -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $cdf >> $log" >> $log
    	    qsub -e $logdir -o $logdir $SRCDIR/ompi1_cmsearch --smxsize 2500 --max -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $cdf >> $log
	fi
    done
done
## try again, those that had errors
for failed in `ls -l $logdir | grep -v "0 Feb" | grep "\.e" | sed 's/.*_\(cmchain_[0-9.]*_.*_v11\).*/\1/g'`; do
    cmn=`echo $failed | sed 's/cmchain_[0-9.]*_//g'`
    res=$resdir/$failed
    cm=$WORKDIR/gissd/$cmn.cm
    cd=`echo $failed | sed 's/\(cmchain_[0-9.]*\).*/\1/g'`
    cdf=$fstdir/$cd.fasta
    sed -i "s/^#\$ -N cmsearch.*/#$ -N cmsearch_${cd}_$cmn/g" $SRCDIR/ompi1_cmsearch
    echo "retrying $failed: $res $cm $cd $cdf" >> $log
    qsub -e $logdir -o $logdir $SRCDIR/ompi1_cmsearch --smxsize 2500 --max -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $cdf >> $log
done


#Z=0.01
#for cm in `grep -l cmcalibrate rfam/RF00028_v11.cm  gissd/I*_v11.cm`; do
#    res=$resdir"/candidates_2500"$(basename ${cm%.*})
#    echo "ompi_cmsearch --max -Z" $Z "-o" $res.out "-A" $res.stk "--tblout" $res.tab $cm $seq >> $log
#    qsub $SRCDIR/ompi_cmsearch --max -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq >> $log
#        #cmsearch -A $res.stk --tblout $res.tab $cm $seq > $res.out
#done

## collect results from bacteria candidates!
#$SRCDIR/chainCMResults.R --dir genomes/bacteria/allbacteria/results --cmd=cmsearch --cfd=cmfull --fend=_v11 --gdf=/homes/biertank/raim/work/introns/phylogeny/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --out=genomes/bacteria/allbacteria/results/results --fig=genomes/bacteria/allbacteria/results/results/figures --inc --store --maxr=0 --maxd=5000 --minl=3 --ovlf --plot

### PFAM ANALYSIS - RFAM
## search Pfam in giisd
## translate candidate sequences
resdir=$WORKDIR/genomes/bacteria/allbacteria/results
transeq -frame 6 -sequence $resdir/candidates_2500.fasta -outseq $resdir/candidates_2500_pep.fasta
mkdir -p $resdir/pfam
# LAGLIDADG_1: 
hmmsearch --max --domtblout $resdir/pfam/candidates_2500_PF00961.tab pfam/PF00961.hmm $resdir/candidates_2500_pep.fasta > $resdir/pfam/candidates_2500_PF00961.out
# LAGLIDADG_2:
hmmsearch --max --domtblout $resdir/pfam/candidates_2500_PF03161.tab pfam/PF03161.hmm $resdir/candidates_2500_pep.fasta > $resdir/pfam/candidates_2500_PF03161.out
# GIY-YIG:
hmmsearch --max --domtblout $resdir/pfam/candidates_2500_PF01541.tab pfam/PF01541.hmm $resdir/candidates_2500_pep.fasta > $resdir/pfam/candidates_2500_PF01541.out
# HNH:
hmmsearch --max --domtblout $resdir/pfam/candidates_2500_PF01844.tab pfam/PF01844.hmm $resdir/candidates_2500_pep.fasta > $resdir/pfam/candidates_2500_PF01844.out
# PI-PfuI Endonuclease subdomain:
hmmsearch --max --domtblout $resdir/pfam/candidates_2500_PF09062.tab pfam/PF09062.hmm $resdir/candidates_2500_pep.fasta > $resdir/pfam/candidates_2500_PF09062.out
# Hom_end:
hmmsearch --max --domtblout $resdir/pfam/candidates_2500_PF05204.tab pfam/PF05204.hmm $resdir/candidates_2500_pep.fasta > $resdir/pfam/candidates_2500_PF05204.out

## collapse
cat $resdir/pfam/candidates_2500_*tab > $resdir/candidates_2500_pfam.tab
## map back to genome, add columns strand,nstart,nend
#$SRCDIR/mapPfam.R --res  $resdir/results.tab --typ cmchain --hid cmchain.ID --pfm $resdir/candidates_2500_pfam.tab --rng 2500 --out test.tab
$SRCDIR/mapPfam.R --pfm $resdir/candidates_2500_pfam.tab > $resdir/candidates_2500_pfam_mapped.tab

## INTEGRATE RESULTS
export WORKDIR=~/work/introns/phylogeny/
resdir=$WORKDIR/genomes/bacteria/allbacteria/results
$SRCDIR/collectSpeciesHits.R
$SRCDIR/collectChains.R --res $resdir/results.tab --hid cmchain.ID --rng 2500 --ann genomes/bacteria/gff --chs $resdir/chains --pfm $resdir/candidates_2500_pfam.tab --gdf substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --sgr `grep -l cmcalibrate rfam/RF00028_v11.cm gissd/I*_v11.cm|sed "s/gissd\/\(.*\)_v11.cm/\1/;s/rfam\/\(.*\)_v11.cm/\1/g"  |tr "\n" ";"` -out $resdir/figures/chains

## TODO:
## * extract pfam and intron hits in correct strand and start to align!
## * phylogeny, use 16S rRNA phylogeny, map to taxon ids via acc and GbAccList
## * map phages to bacteria host via ??
## * 3 PHYLOGENIES: introns, HEGs, 16S rRNA
## * PHAGES vs. BACTERIA: compare phylogeny with hosts


## PHAGE GENOMES
## download via eutils: THIS GENERATES taxonomy.dat!
## TODO: add usual NCBI header >gi|123|ref|NC123|description?
## NOTE: gff files downloaded on 20130321, genomes/bacteria/phages/dl_20130321
perl $SRCDIR/fetchFastaFromID.pl -o genomes/bacteria/phages `cut -f 1 genomes/bacteria/phages/phage.details.txt | grep -v "^#"` 2>> genomes/bacteria/phages/taxonomy.log
## circularize genomes based on indicator in taxonomy.dat (last column!)
\rm genomes/bacteria/phages/allphages.fasta
for i in `grep "0$" genomes/bacteria/phages/taxonomy.dat | cut -f 2`; do
    seq=`find genomes/bacteria/phages/fasta -name "$i*.fasta"`
    cat $seq >> genomes/bacteria/phages/allphages.fasta
done
## circular genomes
log=genomes/bacteria/phages/allphages_fasta.log
\rm $log
for i in `grep "1$" genomes/bacteria/phages/taxonomy.dat | cut -f 2`; do
    seq=`find genomes/bacteria/phages/fasta -name "$i*.fasta"`
    echo $seq >> $log
    $SRCDIR/circularizeSeq.R --seq $seq --ext 5000 --verb >> genomes/bacteria/phages/allphages.fasta 2>> $log
    echo "done." >> $log
done

## ncbi index
#fasta-make-index genomes/bacteria/phages/allphages.fasta
## infernal index
/home/mescalin/choener/tmp/infernal/infernal-1.1rc1/easel/miniapps/esl-sfetch --index genomes/bacteria/phages/allphages.fasta

## subCM search
export WORKDIR=~/work/introns/phylogeny/
resdir=$WORKDIR/genomes/bacteria/phages/cmsearch
seq=$WORKDIR/genomes/bacteria/phages/allphages.fasta
logdir=$resdir/log
log=$logdir/log
Z=0.1
touch $log
for cm in `grep -l cmcalibrate subcms4/*cm`; do
    cmf=$(basename ${cm%.*})
    res=$resdir"/allphages_"$cmf
    sed -i "s/^#\$ -N cmsearch.*/#$ -N cmsearch_${cmf}/g" $SRCDIR/ompi_cmsearch
    echo "ompi_cmsearch -Z" $Z " -o" $res.out "-A" $res.stk "--tblout" $res.tab $cm $seq >> $log
    qsub -e $logdir -o $logdir $SRCDIR/ompi_cmsearch -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq >> $log
done

## full CM search
export WORKDIR=~/work/introns/phylogeny/
resdir=$WORKDIR/genomes/bacteria/phages/cmfull
seq=$WORKDIR/genomes/bacteria/phages/allphages.fasta
logdir=$resdir/log
log=$logdir/log
Z=0.1
for cm in `grep -l cmcalibrate rfam/RF00028_v11.cm  gissd/I*_v11.cm`; do
    cmf=$(basename ${cm%.*})
    res=$resdir"/allphages_"$cmf
    sed -i "s/^#\$ -N cmsearch.*/#$ -N cmsearch_${cmf}/g" $SRCDIR/ompi_cmsearch
    echo "ompi_cmsearch -Z" $Z "-o" $res.out "-A" $res.stk "--tblout" $res.tab $cm $seq >> $log
    qsub -e $logdir -o $logdir $SRCDIR/ompi_cmsearch -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq >> $log
done

## construct hit candidates
$SRCDIR/chainCMResults.R --dir genomes/bacteria/phages  --cmd=cmsearch  --cfd=cmfull --fend=_v11 --gdf=/homes/biertank/raim/work/introns/phylogeny/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat  --gff=genomes/bacteria/phages/gff -dtyp=phg --out=genomes/bacteria/phages/results --fig=genomes/bacteria/phages/results/figures --chs=genomes/bacteria/phages/results/chains --inc --store --maxr=0 --maxd=2500 --minl=3 --exp --ovlf --plot --dof 

## extract sequences around candidates
## TODO: CIRCULAR if range<0!!
$SRCDIR/fetchSeq.R --hit genomes/bacteria/phages/results/results.tab --dir genomes/bacteria/phages/fasta --ext 2500 --type cmchain --tid cmchain_  --circular  --idx genomes/bacteria/phages/taxonomy.dat > genomes/bacteria/phages/results/candidates_phages_2500.fasta
## translate candidate sequences
transeq -frame 6 -sequence genomes/bacteria/phages/results/candidates_phages_2500.fasta -outseq genomes/bacteria/phages/results/candidates_phages_2500_pep.fasta



## retrieve 54 CYANOBACTERIA GENOMES ()
## not working: ALVM00000000 ALVQ00000000 ALWD00000000
## problem: all A.. genomes are only genome project reference files w/o seq
mkdir genomes/bacteria/cyanobacteria
perl $SRCDIR/fetchFastaFromID.pl -o genomes/bacteria/cyanobacteria/ CP003610 CP003611 CP003612 CP003613 CP003659 CP003660 CP003661 CP003662 CP003663 CP003664 CP003665 CP003600 CP003601 CP003602 CP003642 CP003643 CP003644 CP003645 CP003646 CP003647 CP003648 CP003649 CP003650 CP003630 CP003631 CP003632 CP003633 CP003634 CP003635 CP003636 CP003637 CP003638 CP003552 CP003553 CP003554 CP003614 CP003615 CP003616 CP003617 CP003618 CP003619 CP003590 CP003653 CP003654 CP003655 CP003656 CP003657 CP003658 CP003594 CP003595 CP003596 ALVP00000000 ANFQ00000000 ANFJ00000000 ALVJ00000000 CP003495 CP003591 CP003548 CP003592 CP003593 CP003549 CP003550 CP003551 CP003558 CP003559 AJWF00000000 ALVO00000000 ALVI00000000 ALVL00000000 ALVK00000000 ALWB00000000 ALWC00000000 ALWA00000000 ALVY00000000 ALVV00000000 ALVU00000000 ALVZ00000000 ALVN00000000 ALVR00000000 ALVS00000000 ALVW00000000 ANKN00000000 ANKO00000000 ALVJ00000000 CP003940 >& genomes/bacteria/cyanobacteria/taxonomy.log
