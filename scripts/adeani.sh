srcpath=~/programs/gIScanner
datpath=~/data/introns/prokaryotes_2018/Adeani # at HHU exon
eslpath=~/programs/infernal-1.1.2/easel/miniapps # easel miniapps 
infpath=/usr/local # infernal executables
export INFERNALDIR=$infpath

## switch between genomes - TODO: repeat all-in-one
seq=$datpath/160410_AdeaniGenomeAssembly_inclMatePair_oneHaplotype.fsa;result=results_adeani 
seq=$datpath/CP003804.fa;result=results_CP003804
seq=$datpath/CP000815.fa;result=results_CP000815;gbk=$datpath/CP000815.gb

if echo $seq | grep -q CP00; then
    echo "circularize sequence"
    $srcpath/scripts/circularizeSeq.R --seq $seq --ext 5000 > ${seq}_circ.fa
    seq=${seq}_circ.fa
fi

cd $datpath

### SETUP GENOMES

## annotation: ncbi genbank 2 gff
gbk=`echo $seq|sed 's/.fa/.gb/'`
gff=`echo $seq|sed 's/.fa/.gff/'`
if [ -e $gbk ]; then
    echo converting $gbk to gff
    ~/programs/genomeBrowser/src/genbank2gff.pl -trans -semicol $gbk > $gff
fi

## generate infernal index
$eslpath/esl-sfetch --index  $seq

## search fullcms
resdir=$datpath/${result}/fullcms
logdir=$resdir/log
mkdir -p $logdir
Z=1
for cm in `grep -l cmcalibrate $srcpath/parameters/fullcms/*cm`; do
    cmf=$(basename ${cm%.*})
    res=${resdir}/endos_${cmf}
    if [ -s $res.tab ]; then
	echo "$cmf exists"
    else
	echo "$cmf CALCULATE"
	${INFERNALDIR}/bin/cmsearch --cpu 7 -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq
    fi
done
## search subcms
resdir=$datpath/${result}/subcms
logdir=$resdir/log
mkdir -p $logdir
Z=1
for cm in `grep -l cmcalibrate $srcpath/parameters/subcms/*cm|tac`; do
    cmf=$(basename ${cm%.*})
    res=${resdir}/endos_${cmf}
    if [ -s $res.tab ]; then
	echo "$cmf exists"
    else
	echo "$cmf CALCULATE"
	${INFERNALDIR}/bin/cmsearch --cpu 7 -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq
    fi
done

## conventional - inc filter and minl=2
srcpath=~/programs/gIScanner
datpath=~/data/introns/prokaryotes_2018/Adeani # at HHU exon
respath=$datpath/${result}/chainer
mkdir -p $respath

$srcpath/scripts/chainCMResults.R --dir $datpath/${result}  --cmd=subcms --cfd=fullcms --fend=_r112 --gdf=$srcpath/parameters/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --grf=$srcpath/parameters/gI_subgroups.dat --out=$respath --fig=$respath/figures  --chs=$respath/chains --store --maxr=0 --maxd=2500 --minl=2 --ovlf --plot --inc --dof #--exp

## w/o inc filter 
respath=$datpath/${result}/chainer_all
mkdir -p $respath
$srcpath/scripts/chainCMResults.R --dir $datpath/${result}  --cmd=subcms --cfd=fullcms --fend=_r112 --gdf=$srcpath/parameters/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --grf=$srcpath/parameters/gI_subgroups.dat --out=$respath --fig=$respath/figures  --chs=$respath/chains --store --maxr=0 --maxd=2500 --minl=3 --ovlf --plot --dof --gff $gff #--exp

## -> NOTE COX operon with potential 3' end of gI 813500 - 813000 in CP000815
## only seen in full
## depending on settings: gyrA promoter, 3' end of HJ resolvase YqgF,
## promoter of PyrDH E1 alpha, 3' of possible ferredoxin, 

## tailor-made to find CoxIII but not much others
##  just decrease maxd to 0 to reduce to 32 candidates
respath=$datpath/${result}/chainer_full
mkdir -p $respath
$srcpath/scripts/chainCMResults.R --dir $datpath/${result}  --cmd=subcms --cfd=fullcms --fend=_r112 --gdf=$srcpath/parameters/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --grf=$srcpath/parameters/gI_subgroups.dat --out=$respath --fig=$respath/figures  --chs=$respath/chains --store --maxr=0 --maxd=0 --minl=3 --ovlf --plot --dof --gff $gff --add 5500 #--exp

## testing to remove all but number of hits filter
respath=$datpath/${result}/chainer_test
mkdir -p $respath
$srcpath/scripts/chainCMResults.R --dir $datpath/${result}  --cmd=subcms --cfd=fullcms --fend=_r112 --gdf=$srcpath/parameters/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --grf=$srcpath/parameters/gI_subgroups.dat --out=$respath --fig=$respath/figures  --chs=$respath/chains --store --maxr=10 --maxd=0 --minl=5 --plot --dof  --gff $gff #--exp

## use higher minl without ovlf to find 5' and 3' ends!? 
srcpath=~/programs/gIScanner
datpath=~/data/introns/prokaryotes_2018/Adeani # at HHU exon
respath=$datpath/${result}/chainer_partial
mkdir -p $respath
$srcpath/scripts/chainCMResults.R --dir $datpath/${result}  --cmd=subcms --cfd=fullcms --fend=_r112 --gdf=$srcpath/parameters/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --grf=$srcpath/parameters/gI_subgroups.dat --out=$respath --fig=$respath/figures  --chs=$respath/chains --store --maxr=0 --maxd=2500 --minl=5  --plot --inc --dof --gff $gff #--exp
