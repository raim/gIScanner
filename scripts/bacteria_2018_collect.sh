
### COLLECTING & CHAINING RESULTS FROM bacteria_2018.sh

datpath=~/data/introns/prokaryotes_2018

### RESULTS
## hhu hpc
rsync -avz machne@hpc.rz.uni-duesseldorf.de:/gpfs/project/machne/data/introns/results/ $datpath/hhu/results/
rsync -avz machne@hpc.rz.uni-duesseldorf.de:/gpfs/project/machne/data/introns/NOTES $datpath/hhu/
rsync -avz machne@hpc.rz.uni-duesseldorf.de:/gpfs/project/machne/data/introns/missing_hhu.txt $datpath/hhu/

## lpzg
rsync -avz raim@doof.bioinf.uni-leipzig.de:/scr/doofsan/raim/data/introns/allcontigs.idx $datpath/ # fasta index file
rsync -avz raim@doof.bioinf.uni-leipzig.de:/scr/doofsan/raim/data/introns/results/ $datpath/lpzg/results/
rsync -avz raim@doof.bioinf.uni-leipzig.de:/scr/doofsan/raim/data/introns/NOTES $datpath/lpzg/
rsync -avz raim@doof.bioinf.uni-leipzig.de:/scr/doofsan/raim/data/introns/missing_lpzg.txt $datpath/lpzg/

## merge and count missing
cat $datpath/lpzg/missing_lpzg.txt  $datpath/hhu/missing_hhu.txt | sort | uniq -c | grep " 2 " | sed 's/^ \+2 //' > $datpath/allcontigs/missing.txt
wc -l $datpath/allcontigs/missing.txt

\rm $datpath/allcontigs/missing_times.txt
for miss in `cat $datpath/allcontigs/missing.txt`; do
    grep $miss $datpath/allcms_phages_times.txt >>  $datpath/allcontigs/missing_times.txt
done

## ALL TIMES OF PHAGES
sort -k2 -n $datpath/allcms_phages_times.txt
sort -k2 -n $datpath/allcontigs/missing_times.txt

## 20180827 - DONE, only very long remaining
sort -k2 -n $datpath/allcontigs/missing_times.txt | sed 's/112 .*/112/' > $datpath/allcontigs/missing_LONG.txt
rsync -avz $datpath/allcontigs/missing_LONG.txt raim@doof.bioinf.uni-leipzig.de:/scr/doofsan/raim/data/introns/
rsync -avz $datpath/allcontigs/missing_LONG.txt machne@hpc.rz.uni-duesseldorf.de:/gpfs/project/machne/data/introns/

## remove empty results!
\rm `find $datpath/hhu/results/subcms/  -maxdepth 1 -type f -name "allcontigs*" -exec wc -c {} + | sort -nr |grep "^ \+0 " | sed 's/ \+0 //'`
\rm `find $datpath/lpzg/results/subcms/  -maxdepth 1 -type f -name "allcontigs*" -exec wc -c {} + | sort -nr |grep "^ \+0 " | sed 's/ \+0 //'`

## symbolically link data to common folder
## make fresh links
mkdir -p $datpath/allcontigs/results/subcms/
mkdir -p $datpath/allcontigs/results/fullcms/
\rm $datpath/allcontigs/results/subcms/*
\rm  $datpath/allcontigs/results/fullcms/*
## leipzig first
ln -s $datpath/lpzg/results/subcms/allcontigs*.tab $datpath/allcontigs/results/subcms/
ln -s $datpath/lpzg/results/fullcms/allcontigs*.tab $datpath/allcontigs/results/fullcms/
ln -s $datpath/hhu/results/subcms/allcontigs*.tab $datpath/allcontigs/results/subcms/

## CALL CHAINER

dattype="allcontigs" # TODO: use everywhere?

datpath=~/data/introns/prokaryotes_2018
srcpath=~/programs/gIScanner
allpath=$datpath/${dattype}
respath=$allpath/chainer_1500
mkdir -p $respath

## TODO: add more data; use HMM data in chaining!
## --gff=genomes/bacteria/gff --phf=genomes/bacteria/prophages.tbl -cfd=fullcms
$srcpath/scripts/chainCMResults.R --dir $allpath/results  --cmd=subcms --cfd=fullcms --fend=_r112 --gdf=$srcpath/parameters/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --grf=$srcpath/parameters/gI_subgroups.dat --out=$respath --fig=$respath/figures  --chs=$respath/chains --inc --store --maxr=0 --maxd=1500 --minl=3 --ovlf --plot ## --dof --exp

datpath=~/data/introns/prokaryotes_2018
srcpath=~/programs/gIScanner
allpath=$datpath/allcontigs
respath=$allpath/chainer_minl4_maxr1
mkdir -p $respath

## TODO: add more data; use HMM data in chaining!
## --gff=genomes/bacteria/gff --phf=genomes/bacteria/prophages.tbl -cfd=fullcms
$srcpath/scripts/chainCMResults.R --dir $allpath/results  --cmd=subcms --cfd=fullcms --fend=_r112 --gdf=$srcpath/parameters/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --grf=$srcpath/parameters/gI_subgroups.dat --out=$respath --fig=$respath/figures  --chs=$respath/chains --inc -store --maxr=1 --maxd=2500 --minl=4 --ovlf --plot ## --dof --exp

## including full model
## TODO: make option exp faster!

segpath=~/programs/segmenTools
datpath=~/data/introns/prokaryotes_2018
srcpath=~/programs/gIScanner
allpath=$datpath/allcontigs
respath=$allpath/chainer2
mkdir -p $respath

$srcpath/scripts/chainCMResults.R --dir $allpath/results  --cmd=subcms --cfd=fullcms --fend=_r112 --gdf=$srcpath/parameters/substk/RF00028_seed.stockholm_annotated_subcms_cuts.dat --grf=$srcpath/parameters/gI_subgroups.dat --out=$respath --fig=$respath/figures  --chs=$respath/chains --inc -store --maxr=0 --maxd=2500 --minl=3 --ovlf --plot --dof --exp

### ANALYSIS
datpath=~/data/introns/prokaryotes_2018
allpath=$datpath/allcontigs
segpath=~/programs/segmenTools
chain=$allpath/chainer_1500

## de-circularize!
## delete features that stem from chromosome circularization and
## should be present twice, and resolve those spanning chromosome start/end

## rewrite results file for use with segmenTools
echo -e "ID\tchr\tstart\tend\tstrand" > $allpath/gI_hits_circ.tab 
cut -f 1,2,7,8,9 $chain/results.tab | grep -v "^#" | grep -v "^cmchain ID" >> $allpath/gI_hits_circ.tab 

## ALL HITS, with hits spanning chromosome ends indicated by start > end
$segpath/scripts/cleanCircular.R -i $allpath/gI_hits_circ.tab -e 5000 --chrfile $datpath/allcontigs.idx  > $allpath/gI_hits.tab 

## working hits, with hits spanning chromosome ends split
$segpath/scripts/cleanCircular.R -i $allpath/gI_hits_circ.tab -e 5000 --chrfile $datpath/allcontigs.idx --split > $allpath/gI_hits_decirc.tab 

## put all on plus strand for prophage overlap
sed 's/\t-/\t+/g' $allpath/gI_hits_decirc.tab > $allpath/gI_hits_nostrand.tab

## working hits
## expand hits by +/- 200 for overlap with PFAM annotation
awk 'BEGIN {FS="\t";OFS = "\t"}; {if ($1!="ID") {$3=$3 -200; $4=$4 +200}; print}' $allpath/gI_hits_circ.tab > $allpath/gI_200_circ.tab
## working hits, with hits spanning chromosome ends split
$segpath/scripts/cleanCircular.R -i $allpath/gI_200_circ.tab -e 5000 --chrfile $datpath/allcontigs.idx --split > $allpath/gI_200_decirc.tab 
## expand hits by +/- 500 for overlap with PFAM annotation
awk 'BEGIN {FS="\t";OFS = "\t"}; {if ($1!="ID") {$3=$3 -500; $4=$4 +500}; print}' $allpath/gI_hits_circ.tab > $allpath/gI_500_circ.tab
## working hits, with hits spanning chromosome ends split
$segpath/scripts/cleanCircular.R -i $allpath/gI_500_circ.tab -e 5000 --chrfile $datpath/allcontigs.idx --split > $allpath/gI_500_decirc.tab 

### COMPARE with PHISPY PREDICTIONS by Ovidiu
datpath=~/data/introns/prokaryotes_2018
allpath=$datpath/allcontigs

## add pseudo strand info
## TODO: de-circularize: about 70 with start > end are likely
## implying not reverse strand phages, but phages crossing circular genome
## annotation borders
## handle as in expandCircularFeatures
echo -e "ID\tchr\tstart\tend\tstrand" > $allpath/results/prophage/prophage_hits.tab 
awk '{printf("%s%05d %s %s\n", "prophage_", NR, $0, "+")}'  $allpath/results/prophage/prophage_loc_info.txt | sed 's/ \+/\t/g' >> $allpath/results/prophage/prophage_hits.tab 

$segpath/scripts/cleanCircular.R -i $allpath/results/prophage/prophage_hits.tab -e 5000 --chrfile $datpath/allcontigs.idx --only.split > $allpath/results/prophage/prophage_hits_decirc.tab 

## TODO: implement chrMap in index2coor, and repair !each.target & !details
$segpath/scripts/segmentAnnotate.R -t $allpath/gI_hits_nostrand.tab -q $allpath/results/prophage/prophage_hits_decirc.tab --qcol "ID" --prefix "prophage" --details --chrfile $datpath/allcontigs.idx --chrmap --each.target --each.query > $allpath/gI_in_prophages_full.tab

cut -f 1,7,8 $allpath/gI_in_prophages_full.tab >  $allpath/gI_in_prophages.tab


### COMPARE WITH PFAM PREDICTIONS by Ovidiu
## TODO: separately for pro-phages!!

pfampath=$allpath/results/pfam

## PFAM SEARCH HITS BY OVIDIU
## (a) remove lines without coordinates, (b) remove </> in coordinates;
## (c) fuse join, and (d) rm buggy coordinates! (upstream TODO)
## NOTE: join is in left to right notation as well! using sed to fuse start/end
## TODO: what do the </> in coordinates mean??
## TODO: proper complement() and join() notation converter!!
grep "\.\." $pfampath/proka_PFAM_info.txt | sed 's/<//;s/>//'  | sed 's/join(\([0-9]*\).*\.\.\([0-9]*\))/\1..\2/' | sed 's/join(\([0-9]*\).*,\([0-9]*\))/\1..\2/' > $pfampath/proka_PFAM_info_filtered.txt

## split complement(start..end) notation, and rm remaining closing parenthesis
echo -e "chr\tgene_id\tpfam_id\tpfam_name\tstart\tend\tstrand"> $pfampath/pfam_prediction.tsv
## forward strand
cut -f 1,2,3,4,6 $pfampath/proka_PFAM_info_filtered.txt | grep -v -P "^acc\t" | grep -v "complement" | sed 's/\.\./\t/;s/$/\t+/' | sed 's/)\t+$/\t+/' | sort | uniq >> $pfampath/pfam_prediction.tsv
## reverse strand
cut -f 1,2,3,4,6 $pfampath/proka_PFAM_info_filtered.txt | grep -v -P "^acc\t" | grep    "complement" | sed 's/\.\./\t/;s/complement(//;s/)$/\t-/'| sed 's/)\t-$/\t-/' | sort | uniq >> $pfampath/pfam_prediction.tsv

## remove buggy cooridantes
cat $pfampath/pfam_prediction.tsv |grep -v BAU81397 |grep -v CAL71793 |grep -v BAH26069 >  $pfampath/pfam_prediction_fixed.tsv

## split features spanning chromosome start/end coordinates!
$segpath/scripts/cleanCircular.R -i $pfampath/pfam_prediction_fixed.tsv --ext 5000 --chrfile $datpath/allcontigs.idx --only.split --id gene_id > $pfampath/pfam_prediction_decirc.tsv 


## OVERLAP PFAM x gI
## TODO: add columns to pfam predictions, use pfam.id as type column
## TODO: use segmentAnnotate, to find all
## TODO: use segmentOverlap vs. pfam categories to find enrichments
## TODO: segmentRecovery to compare full with chained hits

$segpath/scripts/segmentAnnotate.R -t $allpath/gI_500_decirc.tab -q $pfampath/pfam_prediction_decirc.tsv --qcol pfam_id,pfam_name,gene_id --details --chrfile $datpath/allcontigs.idx --chrmap  --each.target --each.query > $allpath/${dattype}_gI2pfam_full.tab
cut -f 1,7,8 $allpath/${dattype}_gI2pfam_full.tab  >  $allpath/${dattype}_gI2pfam.tab


### RNA FAMILIES

dattype="allcontigs" # TODO: use everywhere?

datpath=~/data/introns/prokaryotes_2018
srcpath=~/programs/gIScanner
allpath=$datpath/${dattype}
rnapath=$allpath/results/rna
segpath=~/programs/segmenTools

## convert complement/join notation as above
grep "\.\." $rnapath/proka_ID_RNA_info_locus.txt | sed 's/<//;s/>//'  | sed 's/join(\([0-9]*\).*\.\.\([0-9]*\))/\1..\2/' | sed 's/join(\([0-9]*\).*,\([0-9]*\))/\1..\2/'  | sed 's/order(\([0-9]*\).*\.\.\([0-9]*\))/\1..\2/' > $rnapath/proka_RNA_info_filtered.txt

## split complement(start..end) notation, and rm remaining closing parenthesis
echo -e "chr\tgene_id\trna_type\trna_class\tstart\tend\tstrand"> $rnapath/rna_prediction.tsv
## forward strand
cut -f 1,2,3,4,5 $rnapath/proka_RNA_info_filtered.txt | grep -v "complement(" | sed 's/\.\./\t/;s/$/\t+/' | sed 's/)\t+$/\t+/' | sort | uniq >> $rnapath/rna_prediction.tsv
## reverse strand
cut -f 1,2,3,4,5 $rnapath/proka_RNA_info_filtered.txt | grep    "complement(" | sed 's/\.\./\t/;s/complement(//;s/)$/\t-/'| sed 's/)\t-$/\t-/' | sort | uniq >> $rnapath/rna_prediction.tsv

## remove buggy cooridantes and mRNA (incomplete list!)
##|grep -v "putative lipoprotein (pseudogene)" | grep -v "tRNA-specific 2-thiouridylase MnmA" 
cat $rnapath/rna_prediction.tsv | grep -v -P "\tmRNA\t" >  $rnapath/rna_prediction_fixed.tsv

## split features spanning chromosome start/end coordinates!
$segpath/scripts/cleanCircular.R -i $rnapath/rna_prediction_fixed.tsv --ext 5000 --chrfile $datpath/allcontigs.idx --only.split --id rna_id > $rnapath/rna_prediction_decirc.tsv 

## OVERLAP RNA x gI
## TODO: add columns to rna predictions, use rna.id as type column
## TODO: use segmentAnnotate, to find all
## TODO: use segmentOverlap vs. rna categories to find enrichments
## TODO: segmentRecovery to compare full with chained hits

$segpath/scripts/segmentAnnotate.R -t $allpath/gI_500_decirc.tab -q $rnapath/rna_prediction_decirc.tsv --qcol rna_type,rna_class --details --chrfile $datpath/allcontigs.idx --chrmap  --each.target --each.query > $allpath/${dattype}_gI2rna_full.tab
cut -f 1,7,8 $allpath/${dattype}_gI2rna_full.tab  >  $allpath/${dattype}_gI2rna.tab

### FISHER's EXACT TEST : RNA and PFAM OVERLAPS
## PFAM - prophages
sed 's/dattype=.*/dattype="allcontigs"/;s/do.rna=.*/do.rna=FALSE/;s/gifff=.*/gifff="_prophage"/' $srcpath/scripts/chain2pfam.R | R --vanilla
## PFAM - prokaryotes without prophages
sed 's/dattype=.*/dattype="allcontigs"/;s/do.rna=.*/do.rna=FALSE/;s/gifff=.*/gifff="_noprophage"/' $srcpath/scripts/chain2pfam.R | R --vanilla
## PFAM - ALL
sed 's/dattype=.*/dattype="allcontigs"/;s/do.rna=.*/do.rna=FALSE/;s/gifff=.*/gifff=""/' $srcpath/scripts/chain2pfam.R | R --vanilla
## RNA - prophages
sed 's/dattype=.*/dattype="allcontigs"/;s/do.rna=.*/do.rna=TRUE/;s/gifff=.*/gifff="_prophage"/' $srcpath/scripts/chain2pfam.R | R --vanilla
## RNA - prokaryotes without prophages
sed 's/dattype=.*/dattype="allcontigs"/;s/do.rna=.*/do.rna=TRUE/;s/gifff=.*/gifff="_noprophage"/' $srcpath/scripts/chain2pfam.R | R --vanilla
## RNA - ALL
sed 's/dattype=.*/dattype="allcontigs"/;s/do.rna=.*/do.rna=TRUE/;s/gifff=.*/gifff=""/' $srcpath/scripts/chain2pfam.R | R --vanilla

### TODO 20181029
## * plot chains in genomic context, using genomeBrowser
## * extend segmentAnnotate to do upstream/downstream separately

## CUT CANDIDATE SEQUENCES FOR DETAILED ANALYSIS
## TODO: map all result coordinates to these cut sequences!
datpath=/scr/doofsan/raim/data/introns
allpath=$datpath #/allcontigs # leipzig - one allcontigs folder!
segpath=~/programs/segmenTools
srcpath=~/programs/gIScanner
dattype=allcontigs

$srcpath/scripts/fetchSeq.R --hit $allpath/gI_hits.tab --dir  $datpath/seeds --fastap "contigs" --ext 1250 --type simple --sid desc  --circular --hcircular > $allpath/${dattype}_gI_1250.fasta
