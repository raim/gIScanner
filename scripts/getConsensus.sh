#!/bin/bash

## collect all consensus structures from a series of stockholm alignments
## TODO: set this path from calling script!
stk2tab=~/programs/gIScanner/scripts/stk2tab.pl

args=("$@")
path=${args[0]}
end="_annotation.fa"
ln=${#args[@]}
echo $@ $ln
for (( i=1;i<$ln;i++)); do
    f=${args[${i}]}
    o=$path/$(basename ${f%.*})$end
    echo "writing structure from" $f "to" $o
    $stk2tab $f | grep SS_cons | sed 's/\.//g;s/GC_SS_cons/>SS_cons\n/' | tr -d '\t' > $o
done
