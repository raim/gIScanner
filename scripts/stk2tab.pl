#!/usr/bin/perl
# -*-Perl-*-
# Last changed Time-stamp: <01-Oct-2012 08:30:54 raim>
# $Id: stk2tab.pl,v 1.2 2012/11/02 19:48:49 raim Exp $

use strict;

my $version = <>;
chomp($version);
$version =~ s/# STOCKHOLM //g;
print STDERR "converting Stockholm version $version\n";


my %seq;
my @id;
my $cnt=0;
while(<>){
    chomp;    
    ## remove comment from annotation line
    $_ =~ s/^#\=GC\s+/GC_/;
    ## skip all other # comment lines
    next if ( /^#/ );
    ## collect sequences: TODO - make this saver!!
    if ( /(\S+)\s+(\S+)/ ) {
	$seq{$1}.=$2;
	$id[scalar(@id)] = $1 unless (grep {$_ eq $1} @id);
    }
}
    
for ( my $i=0; $i<scalar(@id); $i++ ) {
    #print STDERR "HALLO".$i. $id[$i]. scalar(@id)."\n";
    print $id[$i]."\t".$seq{$id[$i]}."\n";
}

#use Bio::AlignIO;
# 
#my $in = Bio::AlignIO->new(-format => 'stockholm',
#			   -file   => $ARGV[1]);
#print $ARGV[1]."\n";
#
#while( my $aln = $in->next_aln ) {
#    print $aln->sequence_start_stop."hallo\n";
#}

__END__
