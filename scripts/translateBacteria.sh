#!/bin/bash

## translate bacterial genomes
for i in `find genomes/bacteria/ -name "*.fna"`
do
j=`echo $i | sed 's/\\.fna/_translated\\.fasta/g'`
echo "TRANSLATING $i"
transeq -frame 6 -sequence $i -outseq $j
done

echo "DONE TRANSLATING BACTERIA"

## collect all peptide fasta files into one file
touch genomes/bacteria/allbacteria_pep.fasta
for i in `find genomes/bacteria/ -name "*_translated.fasta"`
do
echo $i
cat $i >> genomes/bacteria/allbacteria_pep.fasta
\rm $i
done
