#!/usr/bin/perl
#

## modified from script by Donath and Wintsche

# last change Time-stamp: <28-Feb-2013 19:23:21 raim> 
#
# finding the taxonomy for a number of organisms.
#
# AUTHOR: Alexander Donath <alex[you.know.what]bioinf[dot]uni-leipzig[dot]de>
# AUTHOR: Axel Wintsche <wint[you.know.what]bioinf[dot]uni-leipzig[dot]de> (newick tree methods and list option)

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

my ($name, $h, $man, $input_file, $newick);
my $taxdir = "taxonomy";
my (@ids, @names_db);
my ($true, $longest) = (1, 0);
my $useTaxid = 0; ## use taxon id as node names

GetOptions('h|help' => \$h,
	   'tax=s' => \$taxdir,
	   'man=s' => \$man,
	   'f=s' => \$input_file,
	   'id' => \$useTaxid,
	   'n' => \$newick) or pod2usage(2);
&pod2usage(-verbose => 0) if $h;
&pod2usage(-verbose => 3) if $man;

### MAIN ###

my $names_file = $taxdir."/names.dmp";
my $nodes_file = $taxdir."/nodes.dmp";

print STDERR "using $names_file, $nodes_file\n";
#exit;

my $i = 0;

if(defined $input_file && -e "$input_file") {
    open(LIST, "<$input_file");
    foreach(<LIST>) {
	$i++;
	chomp $_;
	
	my $temp = get_from_file($_, $i);
	
	$longest = ($longest > $temp) ? $longest : $temp;
    }
    close(LIST);
}
else {
    while ($true) {
    	$i++;
    	print STDERR "$i. organism (name or taxid; @ to print and exit): ";
	$| = 1;  #force a flush after print
	$_ = <STDIN>;
    	chomp;   
	
	my $start = choose_organism($_, $i);
	if ($start > 1) {
	    my $temp = get_from_file($start, $i);
	    $longest = ($longest > $temp) ? $longest : $temp;
	} elsif ($start == -1) {
	    $true = 0;
	} else {
	    --$i;
	}
    }
}


if($newick) { newick_tree(); }

else { dump_tree($longest); }

### SUBS ###
# author: Alex Donath
# input routine to retrieve the organisms
sub choose_organism {
    my ($name, $number) = @_;
    my @names = ();
    my ($choice, $using, $error) = (0, 0, 1);
    my @grep = ();

    if ($name =~ /^\d+/) {
	@grep = `grep -P '^$name\\s' '$names_file'`;
    } elsif ($name =~ /^[a-zA-Z]+.*/) {
	@grep = `grep -i -P '$name' '$names_file'`;
    } elsif ($name =~ /\@/) {
	return -1;
    } else {
	print STDERR "ERROR: invalid name: \"$name\"\n";
	return 0;
    }

    if (!@grep) {
	print STDERR "ERROR: no such organism/tax-id: \"$name\"\n";
	return 0;
    }
    
    foreach my $line (@grep) {
	chomp($line);
	my @temp = ();
	@temp = split(/\s*\|\s*/, $line);
	push(@names, \@temp);
    }
    
    if ((@names > 1) && (@names < 100)) {
	print STDERR "Found more than one organism with specified parameters.\n";
	print STDERR "Please choose one:\n"; 
	print STDERR "-------------------------------------------------------\n";
	until ($error == 0) {
	    for ($choice = 0; $choice < @names; $choice++) {
		print STDERR ($choice.": ".join(" - ",@{$names[$choice]})."\n");
	    }
	    print STDERR "$choice: abort. new prompt.\n";
	    print STDERR "Your choice [0-$choice] (or <ENTER> for first hit): ";
	    
	    #force a flush after print
	    $| = 1;
	    $using = <STDIN>;
	    chomp $using;
	    if (!($using =~ /\d/)) {
		$using = 0;
		$error = 0;
	    } elsif ($using > $choice) {
		#print STDERR "\e[2J";
		print STDERR "ERROR: \"$using\" is no valid ID.\n";
		$error = 1;
	    } elsif ($using == $choice) {
		return 0;
	    } else {
		$error = 0;
	    }
	}
    } elsif (@names > 100) {
	print STDERR "Found more than 100 organism with specified parameters.\n";
	print STDERR "Please be more precise.\n";
	return 0;
    }
    
    print STDERR "using:\t", $names[$using]->[1], "\n";

    return $names[$using]->[0];
}

# author: Alex Donath
# retrieves tax lineage of given organism
sub get_from_file {
    my ($start, $i) = @_;
    my @temp = ();
    my ($old, $grep);
    my $longest = 0;
    $i--;

    push @ids, $start;
    $old = $start;
    while ($old != 1) {
    	$grep = `grep -m 1 -P '^$old\\s' '$nodes_file'`;
	if(!$grep) {
	    print STDERR "ERROR: tax-id \'".$old."\' not found. invalid tax-id? proceeding with next entry...\n";
	    return 0;
	}
	@temp = split /\s*\|\s*/, $grep;
	if ($temp[0] == $old) {
	    push @ids, $temp[1];
	    $old = $temp[1];
	}
    }
    
    my @names;
    while($old = pop @ids){
    	$grep = `grep -m 1 -P '^$old\\s.+scientific name' '$names_file'`;
    	@temp = split /\s*\|\s*/, $grep;
	my $name = $temp[1];
	$name = $old if $useTaxid; ### use taxid instead!
    	push @names, $name; 
    	$longest = ($longest > length($name)) ? $longest : length($name);
    }
    #my @names = @ids;
	
	#push @names, $start;
    push @names_db,[ @names ];
    
    return $longest;
}

# author: Alex Donath
# prints a representation of taxonomy
sub dump_tree {
    my ($size) = @_;
    my @t;
    
    @names_db = sort array_cmp @names_db;


    push @t, [map { shift @$_ } @names_db] 	while grep {scalar @$_} @names_db;
    
	for (my $i = 0; $i <= $#t ; $i++) {
	
	for (my $j = 0 ; $j <= $#{$t[$i]}; $j++) {
	
	    if (defined($t[$i][$j]) && ($j == 0)) {
			printf STDOUT ("%*s", -($size+1), $t[$i][$j]);
	    } 
		elsif (!defined($t[$i][$j]) && $j == 0) {
			printf STDOUT ("%*s", -($size+1), "-");
	    } 
		elsif (defined($t[$i][$j]) && defined($t[$i][$j-1]) && ($t[$i][$j] eq $t[$i][$j-1])) {
			printf STDOUT ("| %*s", -($size+1), "=");
	    } 
		elsif (defined($t[$i][$j])) {
			printf STDOUT ("| %*s", -($size+1), $t[$i][$j]);
	    } 
		else {
			printf STDOUT ("| %*s", -($size+1), "-");
	    }
	}
	print STDOUT "\n";
    }
    
    return 1;
}

# author: axel
sub newick_tree {

    @names_db = sort array_cmp @names_db;
    
    if( $#names_db == -1 ) { return; }
    else {
	print rec_newick_tree(0, $#names_db).";";
    }
}


# author: axel
# 
# this sub creates a tree in newick format. to represent
# multifurcations it have to add a branch length of 1 to each node.
# because the comma, colon and brackets are reserved for tree (to
# seperate nodes) they have to replaced in the species names as
# followed:
# "," -> ""
# ":" -> ""
# "(" -> ""
# ")" -> ""

sub rec_newick_tree {
	
    my ($start, $end) = @_;
    my $tree;
    my $node_name;
    
    # get leaf of this non furcating branch
    if($start == $end) { 
	
	my $last_element = $#{ $names_db[$start] };
	( $last_element > -1 )? ( $node_name = $names_db[$start][$last_element] ) : ( $node_name = "" );
	$node_name =~ s/[\(\),:]//g;
	return $node_name;
    }
    
    $tree = "(";
    #my $parent = $names_db[$start][1];
    for (my $i = $start; $i < $end ; $i++) {
	if(!defined $names_db[$i][1]) {
	    #this is the case if the choosen organism is in the same 
	    #phylogenetic branch at different levels (e.g. species &
	    #subspecies)in this case print name right here
	    $node_name = $names_db[$i][0];
	    $node_name =~ s/[\(\),:]//g;
	    $tree .= $node_name.":1,";
	    $start = $i+1;
	    
	}
	elsif($names_db[$i][0] ne $names_db[$i+1][0]) {
	    
	    $node_name = shift @{$names_db[$i]};
	    $node_name =~ s/[\(\),:]//g;
	    
	   my $subtree = rec_newick_tree($start, $i);
	    ( $subtree ne "" )? ( $tree .= $subtree.":1," ) : ( $tree .= $node_name.":1," ); 
	    $start = $i+1;
	}
	else {
	    shift @{$names_db[$i]};
	}
    }
    
    if( !defined $names_db[$end][0] ) { return "HALLO"; } # should never come true if species are uniquely sorted
    
    $node_name = shift @{$names_db[$end]};
    $node_name =~ s/[\(\),:]//g;
    
    if($start == $_[0]) {
	my $subtree = rec_newick_tree($start, $end);
	( $subtree ne "" )? ( $tree = $subtree ) : ( $tree .= $node_name ); 
    }
    else {
	if( $tree eq "(" ) {
	    $tree = "";
	    my $subtree = rec_newick_tree($start, $end);
	    ( $subtree ne "" )? ( $tree .= $subtree ) : ( $tree .= $node_name );
	}
	else {
	    my $subtree = rec_newick_tree($start, $end);
	    ( $subtree ne "" )? ( $tree .= $subtree.":1)" ) : ( $tree .= $node_name.":1)" );
	}
	
    }
    return $tree;
    
}
# author: xtof
# modified by: axel
# compares two arrays and sorts them
sub array_cmp {
  my $cmp = 0;

  # iterate over length of shorter array
  for (0..(($#$b < $#$a) ? $#$b : $#$a)) {
    $cmp = $a->[$_] cmp $b->[$_];
    last if $cmp != 0;
  }

 # shorter array first if arrays are equal
  my $tmp = 0;
  if( $#$b <  $#$a ) { $tmp = 1; }
  elsif( $#$b >  $#$a ) { $tmp = -1; }
  
  return $cmp || $tmp;
}


__END__

=head1 NAME

C<tax2newick> - Fetches the lineage of a number of organisms from
    the NCBI taxonomy database. 

=head1 SYNOPSIS

C<tax2newick [options]>

B<Options:>

   -h|--help   See short information
   -man        man page
   -f <file>   list of unique ncbi ids, each id in a seperate line
   -tax <dir>  taxonomy directory with names.dmp and nodes.dmp
   -id         use taxonomy IDs as node names
   -n	       prints a taxonomic tree in newick format

=head1 OPTIONS AND ARGUMENTS

   -h|--help   See short information         
   -man        man page
   -f <file>   list of unique ncbi ids, each id in a seperate line
   -tax <dir>  taxonomy directory with names.dmp and nodes.dmp
   -id         use taxonomy IDs as node names
   -n	       prints a taxonomic tree in newick format

=head1 DESCRIPTION

 The lineage of a number of organisms from the NCBI taxonomy database
 is fetched. Organisms can be defined by (parts of a) name or taxonomy
 ID. If more than one organism is found for the given input, the user
 will be prompted to choose one of the hits. If more than 100 hits are
 found to a given input, the user will be prompted to be more precice.
 The script continues to ask for another organism, until a @ is
 encountered. To speed up this procedure one can use the -f option.
 The taxonomy of the organisms is printed on exit.


 The taxonomy is printed column by column and ordered according to
 common taxonomic groups. Each line is delimited by "\s+\n". The
 single records are delimited by "\s+|\s". The number of spaces
 depends on the longest name. Columns containing "=" indicate that the
 organism shares the taxonomic group on that level with his neighbor
 on the left. To prevent spaces-only columns those are printed as "-".

 If the -n option is given a newick tree is printed instead of a
 column representation. The NCBI taxonomy is used for this tree.

=head2 WARNING: Do not use the taxonomic informations as a source
    for phylogeny!  

(see Disclaimer)

=head1 EXAMPLES

=head3 [Example_1]

B<Input:>
 $ tax
 1. organism (name or taxid; @ to print and exit): millepora

B<Prompt:>
 0: 45264 - Acropora millepora -  - scientific name
 1: 290610 - Symbiodinium sp. ex Acropora millepora -  - scientific name
 2: 351731 - Montipora millepora -  - scientific name
 3: 432346 - Wanella milleporae -  - scientific name
 4: abort. new prompt.

B<Choice:>
 Your choice [0-4] (or <ENTER> for first hit): 0

B<Output> (on exit)B<:>
 root                
 cellular organisms  
 Eukaryota           
 Fungi/Metazoa group 
 Metazoa             
 Eumetazoa           
 Cnidaria            
 Anthozoa            
 Hexacorallia        
 Scleractinia        
 Astrocoeniina       
 Acroporidae         
 Acropora            
 Acropora millepora

=head3 [Example_2]

 A taxonomy search for Metazoa (tax id 33208), Firmicutes (1239), and
 Viridiplantae (33090) results in:

 root                | =                   | =                  
 cellular organisms  | =                   | =                  
 Bacteria            | Eukaryota           | =                  
 Firmicutes          | Fungi/Metazoa group | Viridiplantae       
 -                   | Metazoa             | -           

 Firmicutes, Metazoa, and Viridiplantae are all cellular organisms.
 Metazoa and Viridiplantae also share their superkingdom (Eukaryota).
 Therefore the corresponding columns contain a "=". Since we were only
 interested in Firmicutes and Viridiplantae and not one of their
 (sub-)groups the remaining columns are filled with "-".

=head1 DISCLAIMER

 The authors are not responsible for any loss of data, wrong research
 results, or anything else which may be caused by using this tool.

 The NCBI taxonomy database is not an authoritative source for
 nomenclature or classification - please consult the relevant
 scientific literature for the most reliable information.

=head1 FEEDBACK

=head2 Reporting Bugs:

 This script does not, as far as known, obey Sturgeons law which says:
 90% of everything is crud.

 But in case you’ve found any bugs, please report them to the author.

=head1 SEE ALSO

The B<wo> script of Manja.

=head1 ACKNOWLEDGEMENT

 Thanks to xtof for the rapid array_cmp method implementation.

=head1 AUTHOR

 Alexander Donath <alex[you.know.what]bioinf[dot]uni-leipzig[dot]de>,
 Axel Wintsche <wint[you.know.what]bioinf[dot]uni-leipzig[dot]de>

=head1 VERSION

v0.3.1 (LE/Germany, 06/09)

=cut

