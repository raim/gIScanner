#!/bin/bash

## This scripts downloads the NCBI Taxonomy and the SILVA 16S Tree,
## and uses them to map a phylogentic tree to NCBI taxon IDs,
## and then scan a set of input taxononmy IDs for species
## present in the 16S tree.

## TODO - 201908 : replace by genomeBrowser/data/taxonomy

## TODO - SPLIT AND GENERALIZE:
##       * use current NCBI taxonomy and SILVA 16S versions
##         as input, to allow easy updating of data set!
##       * use this as an input to cmpTaxa.R as used at the bottom of
##         this script, with a list of new taxon IDs as input


## TO USE, adapt the paths srcpath and datpath:
srcpath=~/programs/gIScanner/scripts
datpath=~/data/introns/prokaryotes_2018/taxonomy # at hhu exon & intron

# NOTE: symbolic link to external drive at /data2/introns/prokaryotes_2018/
mkdir -p $datpath


### NOTE: start general taxonomy update script here
###       (end below before calling cmpTaxa.R)

## NOTE: downloaded all data on 20180912


## DOWNLOAD
## NOTE adapt scripts to read archive files instead of unpacking?
cd $datpath
## NCBI: full taxonomy
wget ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz
tar zxvf taxdump.tar.gz
## NCBI: taxid refseq ID mapping
wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/gi_taxid_nucl.dmp.gz
gunzip gi_taxid_nucl.dmp.gz
## NBCI: genbank to nucleotide accession (gi) mapping 
wget ftp://ftp.ncbi.nih.gov/genbank/livelists/GbAccList.0909.2018.gz
gunzip GbAccList.0909.2018.gz
## SILVA: species AccNum mapping
wget https://www.arb-silva.de/fileadmin/silva_databases/living_tree/LTP_release_132/LTPs132_SSU.csv
## SILVA: 16S rRNA tree
wget https://www.arb-silva.de/fileadmin/silva_databases/living_tree/LTP_release_132/LTPs132_SSU_tree.newick


## map accession numbers from SILVA 16S Tree to taxon ids
## 1) find accession number in GbAccList from NCBI and get GI identifier
cut -f 1 $datpath/LTPs132_SSU.csv  | perl $srcpath/getIDs.pl -f $datpath/GbAccList.0909.2018 -d "," -c 0 > $datpath/LTPs132_SSU_gb2gi.csv 2> $datpath/LTPs132_SSU_gb2gi.log
## 2) find GI in gi_taxid_nucl.dmp
cut -d , -f 3 $datpath/LTPs132_SSU_gb2gi.csv | perl $srcpath/getIDs.pl -f $datpath/gi_taxid_nucl.dmp -d "\t" -c 0  > $datpath/LTPs132_SSU_gi2tax.csv 2> $datpath/LTPs132_SSU_gi2tax.log

## NOTE: see LTPs132_SSU_gb2gi.log
## for species with multiple 16S rRNA sequences !


## generate tree with taxon IDs: the script generates two files;
## taxonomy/LTPs132_SSU_tree2tax.dat  maps the old species names to taxon IDs
## taxonomy/LTPs132_SSU.tree_taxonIDs.newick is the 16S rRNA tree with 
##    taxon IDs and species names (but maintaining clade names)
## 1) replace spaces in tree file
sed s'/ \+/XYZYX/g' $datpath/LTPs132_SSU_tree.newick > $datpath/LTPs132_SSU_tree_nospaces.newick ## NOTE: XYZYX is replaced with spaces in the convert16Stree.R

## replace path in convert16Stree.R
cp  -a $srcpath/convert16Stree.R  $datpath/tmp.R
sed -i "s|tpath\=.*|tpath\='$datpath'|" $datpath/tmp.R
$datpath/tmp.R # TODO: use command-line option for version

### NOTE: end general taxonomy update script here
###       start taxon ID input here

### MAIN INPUT: list of target taxon IDs
## as sent by Ovidiu - TODO: convert to argument?
cp -a ~/Downloads/prokaAcc_taxID.txt $datpath/

## ALL TAXIDs in allcontigs.fa
## replace obsolete taxon IDs!
## TODO: user merged.dmp to find and replace merged entries
sed 's/#prokaID taxID/acc taxon/' $datpath/prokaAcc_taxID.txt | sed 's/ /\t/g' | sed 's/\t657310$/\t1351/;s/\t1166016$/\t1905730/;s/\t595593$/\t656366/;s/\t319938$/\t288004/;s/\t940260$/\t481146/;s/\t697046$/\t645/;s/\t469595$/\t1639133/;s/\t758602$/\t1073996/;s/\t913102$/\t59277/;s/\t585494$/\t573/;s/\t1411903$/\t1267768/;'  > $datpath/target_taxonIDs.txt

##16S rRNA PHYLOGENY <-> TAXONOMY MAPPING
## NOTE: once calculated, for development, we can use the taxonomy tree
## to save time
$srcpath/cmpTaxa.R  -txd $datpath  -tn acc -tf $datpath/LTPs132_SSU_tree2tax.dat -sf $datpath/target_taxonIDs.txt -out $datpath/species2silva -xf $datpath/species2silva_tree.RData -dbug &> $datpath/species2silva.log

#$srcpath/cmpTaxa.R  -txd $datpath  -tn taxon -tf $datpath/LTPs132_SSU_tree2tax.dat -sf /data/topoisomerases/cyanoids.txt -out $datpath/species2silva -xf $datpath/species2silva_tree.RData -dbug 
