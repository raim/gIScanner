#!/bin/bash


module load Infernal/1.1.2
module load intelmpi/5.0.2.044

## paths
genome=allcontigs
datpath=/gpfs/project/machne/data/introns
seq=$datpath/${genome}.fa
infile=$datpath/allcms.txt
resdir=$datpath/results/subcms

## replace with sed
Z=ZETT

## current job
cm=$(sed "${PBS_ARRAY_INDEX}q;d" $infile)

## output names
cmf=$(basename ${cm%.*})
res=${resdir}/${genome}_${cmf}


MPICPUS=`cat $PBS_NODEFILE | wc -l`


if [ -s $res.tab ]; then
    echo "results exist: $cmf" > $res.log 
else
    echo "scan $cmf" # model name in .OU file output
    echo "START $cmf" > $res.log 
    echo "CPUS: $MPICPUS" >> $res.log
    echo "`date +"%d.%m.%Y-%T"`" >> $res.log
    mpirun --rsh=ssh -n $MPICPUS cmsearch --mpi -Z $Z -o $res.out -A $res.stk --tblout $res.tab $cm $seq
    ## getstats
    qstat -f -x $PBS_JOBID >> $res.log
fi

echo "DONE WITH $PBS_JOBID ($PBS_JOBNAME) @ `hostname`" >> $res.log
echo "`date +"%d.%m.%Y-%T"`" >> $res.log
