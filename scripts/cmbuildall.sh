#!/bin/bash

# A wrapper to build multiple CMs from a list of stk alignments
# scripts/cmbuildall.sh gissd/*.stk


cmbuild=${INFERNALDIR}/bin/cmbuild

#version
version=`$cmbuild -h |grep INFERNAL | sed 's/# INFERNAL //g;s/(.*//g'| tr -d ' '`


args=("$@")
ln=${#args[@]}
#echo $@ $ln

for (( i=0;i<$ln;i++)); do
    f=${args[${i}]}
    o=${f%.*}.cm
    l=${f%.*}_build.log
    if [ $version = "1.1rc1" ]; then
	o=${f%.*}_v11.cm
	l=${f%.*}_v11_build.log
    fi
    if [ $version = "1.1.2" ]; then
	o=${f%.*}_v112.cm
	l=${f%.*}_v112_build.log
    fi
    cmd="$cmbuild -F $f $o &> $l"
    echo $cmd
    $cmbuild -F $o $f &> $l
done
